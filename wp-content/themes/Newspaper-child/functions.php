<?php
/*  ----------------------------------------------------------------------------
    Newspaper 6 Child theme - Please do not use this child theme with older versions of Newspaper Theme

    What can be overwritten via the child theme:
     - everything from /parts folder
     - all the loops (loop.php loop-single-1.php) etc

     - the rest of the theme has to be modified via the theme api:
       http://forum.tagdiv.com/the-theme-api/

 */




/*  ----------------------------------------------------------------------------
    hook used to modify/add/delete items via the theme API
 */
add_action('td_global_after', 'child_theme_td_global_after');
function child_theme_td_global_after() {
    /*
        You can use the api inside this function:

        See how the api is used in Newspaper/includes/td_config.php    the API starts from "static function td_global_after() {"
        Api Documentation: http://forum.tagdiv.com/the-child-theme-method/
    */
}




/*  ----------------------------------------------------------------------------
    add the parent style + style.css from this folder
 */
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 1000);
function theme_enqueue_styles() {
    wp_enqueue_style('td-theme', get_template_directory_uri() . '/style.css', '', '6.1c', 'all' );
    wp_enqueue_style('td-theme-child', get_stylesheet_directory_uri() . '/style.css', array('td-theme'), '6.1c', 'all' );

}