<?php
function iva_album_popular_songs($atts, $content = null,$code) {
	extract(shortcode_atts(array(
		'columns'		=> '4',
		'songs_id' => ''
		), $atts));
		
		global $wpdb,$default_date,$post;
		$output ='';
		?>
		<div class="custompost_entry">				
			<div class="custompost_details">
				<div class="col_three_fourth end">
					<?php
					$audiolist = $songs_id;
					
					$audio_list=array();
					$audio_list=explode(',',$audiolist);
					$count=count($audio_list);
					$audio_id=array();
					
					/*** Album Details for MusicPlayer
					 * permalink, title, release date, audio playlist
					 */
								
					
				
					if($audiolist !='') {
						
						if($count > 0) { 
						
							$i = 1;
							
							
							$fcount=1;
								
								
							foreach($audio_list as $songs_id) {
								$found= false;
								$album_meta_args =  array(
								   'post_type' 	=> 'albums',
								   'post_status'=>'publish',
								   'meta_query'	=> array(
								   'relation'  	=> 'OR',
										array(
										 'key'     => 'audio_upload',
										 'value'   => $songs_id,
										 'compare' => 'LIKE'
										),
										array(
										 'key'     => 'audio_mp3',
										 'value'   => $songs_id,
										 'compare' => 'LIKE'
										),
										array(
											'key'     => 'audio_soundcloud_id',
											'value'   => $songs_id,
											'compare' => 'LIKE'
										),
										array(
											'key'     => 'medialibrary',
											'value'   => $songs_id,
											'compare' => 'LIKE'
										),
									),
								);

								$album_meta_query = new WP_Query( $album_meta_args );
								$start= 1;

									
								if ( $album_meta_query->have_posts() ) : 
							
									while ( $album_meta_query->have_posts() ) : $album_meta_query->the_post();
									
										
										$iva_page_id = get_post($songs_id);
										
										
										$audio_posttype_option		= get_post_meta( $post->ID, 'audio_posttype_option', true );
										
										
										$permalink = get_permalink(  $post->ID);
										$post_date = get_post_meta( $post->ID,'audio_release_date',true);
										if($post_date !='') { 
											if(is_numeric($post_date)){
												 $post_date= date_i18n($default_date,$post_date);
											}	
										}
										$imagesrc = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );
										$music_image = aq_resize( $imagesrc[0], '80', '80', true );
									
										$playlisttitle              = get_the_title( get_the_id() );
										if ( $audio_posttype_option  == 'player' ) {
										
											
											$lyrics_option	= get_option('atp_lyrics_option') ?  get_option('atp_lyrics_option') : 'lightbox';
											if($lyrics_option ==="lightbox") { $lyrics_box ="prettyPhoto";}else{ $lyrics_box ="lyrics"; } 
											
											$audio_upload	= get_post_meta( $post->ID, 'audio_upload', true );
											$audio_upload 	= explode(',',$audio_upload);$count=count($audio_upload);
											
											
											if(in_array($songs_id,$audio_upload)){ 
												$player_title = get_the_title( get_the_id());
												$attachment 		= get_post( $songs_id );
												
												
													
												if(count($attachment) > 0) {
													$found = true;
													$starts_pl = rand(5,100);
														
													
													$pageid  = $attachment->post_parent;
													$attachment_title = $attachment->post_title;
													$attachment_caption=$attachment->post_excerpt;
													$output .='<tr><td>';
													$output .= $fcount;
													$output .= '</td><td>';
													if( has_post_thumbnail($post->ID)){ 
														$output .= '<a href="'.get_permalink().'" >'.atp_resize( $post->ID, '', '50', '50', '', '' ).'</a>';
													}
													$output .= '</td><td>';
															
													
													$output .='<a class="fap-single-track no-ajaxy"  data-meta="#player-meta'.$starts_pl.'"  href="'.$attachment->guid.'"  rel="'.$music_image.'"  title="'.$attachment_title.'">';
													$output .='<i class="fa fa-play"></i> </a>';
													$output .= '</td><td>';
													$output .= $attachment_title;
													$output .= '</td><td>';
													$output .= '<h3>'.get_the_title($post->ID).'</h3>';	
													$output .= '</td></tr>';
													
												}
												$output .= '<div id="player-meta'.$starts_pl.'" class="single-player-meta" >';
												$output .= '<a href="'.$permalink.'">'.$player_title.'</a>';
												$output .= '<div>'.get_the_term_list( $post->ID, 'label', '', ',', '' ).' | '.$post_date.'</div></div>'; 	
											}
										
										}else if ( $audio_posttype_option  == 'externalmp3' ) {
										
											$attachment = get_post( $songs_id );
											$audiolist=get_post_meta($post->ID,'audio_mp3',true) ? get_post_meta($post->ID,'audio_mp3',true) :'';
											$count=count($audiolist);
											$imagesrc = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );
											$music_image = aq_resize( $imagesrc[0], '80', '80', true );
											if($audiolist !='') {
												if($count > 0) {
													$i = 1;
													
													foreach($audiolist as $mp3_list) {
														
														$mp3_id=$mp3_list['mp3id'];
														if($mp3_id == $songs_id){
														$starts_ex = rand(101,200);
															$found = true;
															$attachment_title = $mp3_list['mp3title'];
															$output .= '<tr><td>';
															$output .= $fcount;
															$output .='</td><td>';
															if( has_post_thumbnail($post->ID)){ 
																$output .= '<a href="'.get_permalink().'" >'.atp_resize( $post->ID, '', '50', '50', '', '' ).'</a>';
															}
															$output .= '</td><td>';
															$output .= '<a class="fap-single-track no-ajaxy"  data-meta="#player-meta'.$starts_ex.'"  href="'.esc_attr($mp3_list['mp3url']).'"  rel="'.$music_image.'"  title="'.$attachment_title.'">';
															$output .= '<i class="fa fa-play"></i></a>';
															$output .= '</td><td>';
															$output .= $attachment_title;
															$output .= '</td><td>';
															$output .= '<h3>'.get_the_title($post->ID).'</h3>';
															$output .= '</td></tr>';
															$output .= '<div id="player-meta'.$starts_ex.'" class="single-player-meta" >';
															$output .= '<a href="'.$permalink.'">'.get_the_title($post->ID).'</a>';
															$output .= '<div>'.get_the_term_list( $post->ID, 'label', '', ',', '' ).' | '.$post_date.'</div></div>'; 	
														}
													}
												}
												
											}
										}else if ( $audio_posttype_option  == 'soundcloud' ) {
											$audio_soundcloud_id = get_post_meta($post->ID,'audio_soundcloud_id',true);
											
											if($audio_soundcloud_id == $songs_id ){
												$starts_sc = rand(201,300);
												$found=true;
												$attachment 		= get_post( $songs_id );
											
												$audio_soundcloud_title = get_post_meta($post->ID,'audio_soundcloud_title',true);
												$attachment_title = $audio_soundcloud_title ? $audio_soundcloud_title :'Title Label';
												$audio_soundcloud_url = get_post_meta($post->ID,'audio_soundcloud_url',true);
												$output .= '<tr><td>';
												$output .= $fcount;
												$output .= '</td><td>';
												if( has_post_thumbnail($post->ID)){ 
													$output .= '<a href="'.get_permalink().'" >'.atp_resize( $post->ID, '', '50', '50', '', '' ).'</a>';	
												}
												$output .= '</td><td>';
											
												$output .='<a title="'.$attachment_title.'" href="'.$audio_soundcloud_url.'" data-meta="#player-meta'.$starts_sc.'"   class="fap-single-track no-ajaxy"  rel="'.$music_image.'"><i class="fa fa-play"></i></a>';
											
												$output .= '</td><td>';
												$output .= $attachment_title;
												$output .= '</td>';
												$output .= '<td>';
												$output .= '<h3>'.get_the_title($post->ID).'</h3>';	
												$output .='</td>';
												$output .='</tr>';

												$output .= '<div id="player-meta'.$starts_sc.'" class="single-player-meta" >';
												$output .= '<a href="'.$permalink.'">'.get_the_title($post->ID).'</a>';
												$output .= '<div>'.get_the_term_list( $post->ID, 'label', '', ',', '' ).' | '.$post_date.'</div></div>'; 
											}
											
										}else if ( $audio_posttype_option  == 'medialibrary' ) {
											$media_upload	= get_post_meta( $post->ID, 'audio_medialibrary', false );
											$count=count($media_upload);
											foreach ($media_upload as  $media_upload){
												if($media_upload == $songs_id ){
													$attachment 		= get_post( $songs_id );
													$audio_title_label 	= $attachment->post_title;
													if(count($attachment) > 0) {
														$starts = rand(301,400);
														$found = true;
														$pageid  = $attachment->post_parent;
														$attachment_title = $attachment->post_title;
														$attachment_caption=$attachment->post_excerpt;
														$output .= '<tr><td>';
														$output .= $fcount;
														$output .= '</td><td>';
														if( has_post_thumbnail($post->ID)){ 
															
															$output .= '<a href="'.get_permalink().'" >'.atp_resize( $post->ID, '', '50', '50', '', '' ).'</a>';
														}
														$output .= '</td><td>';
														$output .= '<a class="fap-single-track no-ajaxy"  data-meta="#player-meta'.$starts.'"  href="'.$attachment->guid.'"  rel="'.$music_image.'"  title="'.$attachment_title.'">';
														$output .= '<i class="fa fa-play"></i></a>';
														$output .= '</td><td>';
														$output .= $attachment_title;
														$output .= '</td><td>';
														$output .= '<h3>'.get_the_title($post->ID).'</h3>';	
														$output .= '</td></tr>';
														$output .= '<div id="player-meta'.$starts.'" class="single-player-meta" >';
														$output .= '<a href="'.$permalink.'">'.get_the_title($post->ID).'</a>';
														$output .= '<div>'.get_the_term_list( $post->ID, 'label', '', ',', '' ).' | '.$post_date.'</div></div>'; 
														
													}
													
												}
												
												
											}
										}
										
										if( $found == false) {
											$iva_error[] =  $songs_id; 
										}
										$start++;
									endwhile; 
									
									wp_reset_query();
									else :
										
										$arr_songs_id[] = $songs_id;
										$iva_songs_ids = implode(",", $arr_songs_id);
								endif;
								
								
								if( $found ) {  $fcount++; }
							
							}
							
						}
					}else{
						echo '<div class="messagebox error">'.__( '<strong>Please Enter Ids</strong>','THEME_ADMIN').'<span class="close">x</span></div>';
					} 
					$iva_error_ids = @implode(",",$iva_error);
					if(!empty($iva_error_ids)){
						echo '<div class="messagebox error">'.__($iva_error_ids .' Not found in selected type','THEME_ADMIN').'<span class="close">x</span></div>';
										
					}	
			
					if($output){
						echo '<table width="100%">';
						echo '<tr>';
						echo '<th>S.No</th>';
						echo '<th>Image</th>';
						echo '<th>Play</th>';
						echo '<th>Song Title</th>';
						echo '<th>Post Title</th>';
						echo '</tr>';
						echo $output;
						echo '</table>';
					}
					if(!empty($iva_songs_ids)){
						echo '<div class="messagebox error">'.__( '<strong>'.$iva_songs_ids.'</strong> not found','THEME_ADMIN').'<span class="close">x</span></div>';
					}
								

					?>	
			</div><!-- .col_three_fourth -->
		</div><!-- .custompost_details -->
	</div><!-- .custompost_entry --><?php
	
}
add_shortcode('popular_songs','iva_album_popular_songs');  // End Shortcode
?>