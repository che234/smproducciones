<?php
function iva_djmix($atts, $content = null,$code) {
	
	extract(shortcode_atts(array(
			'cat'			=> '',
			'limit'			=> '-1',
			'columns'		=> '',
			'postid'		=> '',
			'order'			=> 'ASC',
			'orderby'		=> 'date',	
			'pagination'	=> ''
		), $atts));
		global $default_date;
		$column_index = 0;
		if( $columns == '4' ) { $class = 'col_fourth'; }
		if( $columns == '3' ) { $class = 'col_third'; }
		if( $columns == '' ) { $columns ='4'; $class = 'col_fourth'; }
		
		//Album Image Sizes
		$width='470'; $height = '470';
		$out = $djmix_genre = $djtitle ='';

		if ( get_query_var('paged') ) { 
			$paged = get_query_var('paged'); 
		} elseif ( get_query_var('page') ) { 
			$paged = get_query_var('page');
		}else {
			$paged = 1;
		}
		$orderby = $orderby ? $orderby : get_option('atp_djmix_orderby');
		$order   = $order ? $order : get_option('atp_djmix_order');

		if($orderby == 'djmix_release_date'){
			$meta_key ='djmix_release_date';
		}else{
			$meta_key = '';
		}
		$query = array(
			'post_type'	=> 'djmix', 
			'showposts'	=> $limit, 
			'tax_query' => array(
							'relation' => 'OR',
						),
			'meta_key'  => $meta_key,			
			'paged'		=> $paged,
			'orderby'	=> $orderby,
			'order'		=> $order
		);

		if($cat !=''){
			$cats = explode(',',$cat);
			$tax_artist	=	array(
			'taxonomy'  => 'djmix_cat',
			'field' 	=> 'slug',
			'terms' 	=> $cats
			);
			array_push( $query['tax_query'],$tax_artist);
		}

		$postid_array = array();
		$postid_array = explode(',',$postid);
		if($postid!=''){
			$query = array(
					'post_type'	=> 'djmix', 
					'post__in'	=> $postid_array
			);
		}

		$start=rand(10,111);
		query_posts($query); //get the results
		global $post;
		if ( have_posts()) : while (  have_posts()) :  the_post(); 

		$djmix_release_date = get_post_meta( $post->ID, 'djmix_release_date', true );
	
		if($djmix_release_date !='') { 
	     if(is_numeric($djmix_release_date)){
	      $djmix_release_date= date_i18n($default_date,$djmix_release_date);
	     } 
	    }
		$djmix_genre			= get_post_meta( $post->ID, 'djmix_genre', true );
		$djmix_buy_mix			= get_post_meta( $post->ID, 'djmix_buy_mix', true );
		$djmix_buy_url			= get_post_meta( $post->ID, 'djmix_buy_url', true );
		$djmix_download_url 	= get_post_meta( $post->ID, 'djmix_download_url', true );
		$djmix_download_text 	= get_post_meta( $post->ID, 'djmix_download_text', true ) ? get_post_meta( $post->ID, 'djmix_download_text', true ) : 'Download';
		$audio_posttype_option	= get_post_meta( $post->ID, 'audio_posttype_option', true )? get_post_meta( $post->ID, 'audio_posttype_option', true ) :'player';
		$djmix_upload_mix		= get_post_meta( $post->ID, 'djmix_upload_mix', true );
		$imagesrc				= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
		$image					= aq_resize($imagesrc[0], '80', '80', true ); 
		$djtitle				= get_the_title($post->ID);
		$img_alt_title 			= get_the_title();
		$post_date			= get_post_meta(get_the_id(),'djmix_release_date',true);
				
				if($post_date !='') { 
					if(is_numeric($post_date)){
						$post_date= date_i18n($default_date,$post_date);
					}	
				}
		$column_index++;
		$last = ( $column_index == $columns && $columns != 1 ) ? 'end ' : '';
		/*--------------------------------*/
		$out .= '<div class="album-list djmix-slist '.$class.' '. $last.'">'; 
		$out .= '<div class="custompost_entry">';
		$out .="<span class='fap-my-playlist album-playlist' data-playlist='unique-playlist-id'>";
		
		if($audio_posttype_option == 'player'){ 
			$djmix_list = get_post_meta($post->ID,'djmix_upload_mix',true) ? get_post_meta($post->ID,'djmix_upload_mix',true) :'';

			if( has_post_thumbnail($post->ID)){ 
				$out .='<div class="custompost_thumb port_img">';
				$out .='<figure>'. atp_resize( $post->ID, '', '500','500', '', $img_alt_title ).'</figure>';
				$out .='<div class="hover_type">';
				$attachment = get_post( $djmix_list );
				$title = $attachment->post_title;
				$playlisttitles = '<div id="single-player-meta'.$start.'">'.$djtitle.'</div>';
				$out .='<div class="play mediamx"><a class="hoverdjmix fap-single-track no-ajaxy" data-meta="#single-player-meta'.$start.'" class="no-ajaxy" href="'.$attachment->guid.'" title="'.$title.'" rel="'.$image.'"><div class="hoverdjmix"></div></a></div>';
				$out .='</div>';
				$out .='<span class="imgoverlay"></span>'; 
				$out .='</div>';
			} 
			$out .='<div class="single-player-meta" id="single-player-meta'.$start.'">'.$djtitle.'</div>'; 
			
		}elseif($audio_posttype_option == 'medialibrary'){
   
		   $djmix_list = get_post_meta($post->ID,'djmix_medialibrary',true) ? get_post_meta($post->ID,'djmix_medialibrary',true) :'';
		
		   if( has_post_thumbnail($post->ID)){ 
		    $out .='<div class="custompost_thumb port_img">';
		    $out .='<figure>'. atp_resize( $post->ID, '', '500','500', '', $img_alt_title ).'</figure>';
		    $out .='<div class="hover_type">';
		    $attachment = get_post( $djmix_list );
		    $title = $attachment->post_title;
		    $playlisttitles = '<div id="single-player-meta'.$start.'">'.$djtitle.'</div>';
		    $out .='<div class="play mediamx"><a class="hoverdjmix fap-single-track no-ajaxy" data-meta="#single-player-meta'.$start.'" class="no-ajaxy" href="'.$attachment->guid.'" title="'.$title.'" rel="'.$image.'"><div class="hoverdjmix"></div></a></div>';
		    $out .='</div>';
		    $out .='<span class="imgoverlay"></span>'; 
		    $out .='</div>';
		   } 
		   $out .='<div class="single-player-meta" id="single-player-meta'.$start.'">'.$djtitle.'</div>'; 
		  }elseif($audio_posttype_option == 'externalmp3'){
			if( has_post_thumbnail($post->ID)){ 
				$out .='<div class="custompost_thumb port_img">';
				$out .='<figure>'. atp_resize( $post->ID, '', '500','500', '', $img_alt_title ).'</figure>';
				$out .='<div class="hover_type">';
		
					$djmix_externalmp3url = get_post_meta($post->ID,'djmix_externalmp3',true); 
					$djmix_externalmp3urltitle = get_post_meta($post->ID,'djmix_externalmp3title',true); 
					$out .='<div class="play mediamx"><a class="hoverdjmix fap-single-track no-ajaxy "  data-meta="#single-player-meta'.$start.'" class="no-ajaxy" href="'.$djmix_externalmp3url.'" rel="'.$image.'" title="'.$djmix_externalmp3urltitle.'"></a><div class="hoverdmix"></div></a></div>';

				$out .='</div>';
				$out .='<span class="imgoverlay"></span>'; 
				$out .='</div>';
			} 
			$out .='<span class="single-player-meta" id="single-player-meta'.$start.'"><div><p style="font-size: 13px;">'.$djmix_genre.'</p></div></span>'; 
		}else{ 
			if( has_post_thumbnail($post->ID)){ 
				$out .='<div class="custompost_thumb port_img">';
				$out .='<figure>'. atp_resize( $post->ID, '', '500','500', '', $img_alt_title ).'</figure>';
				$out .='<div class="hover_type">';
				$audio_soundcloud_url = get_post_meta($post->ID,'audio_soundcloud_url',true);
				$out .='<div><a data-meta="#single-player-meta'.$start.'" class="hoverdjmix fap-single-track no-ajaxy" href="'.$audio_soundcloud_url.'" class="fap-single-track no-ajaxy"><div class="hoverdjmix"></div></a></div>';
				$out .='</div>';
				$out .='<span class="imgoverlay"></span>'; 
				$out .='</div>';
			
			}			
			$out .='<div class="single-player-meta" id="single-player-meta'.$start.'">'.$djtitle.'</div>'; 
		}
		$out .="</span>";		
		$out .='<div class="album-desc">';
		$out .='<h2 class="entry-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h2>';
		if ( $djmix_genre != '' ) { 
			$out .='<span>'.$djmix_genre.'</span>';
		} 
		$out .= '</div>';
		$out .= '</div>';
		$out .= '</div>';
	
		if( $column_index == $columns ){
			$column_index = 0;
			$cleardiv=false;
			$out.='<div class="clear"></div>';
		}
		$start++;
		endwhile;
		// Last Row  Divesion clear 
			if( ($column_index != 0 ) && ($column_index != $columns) ){

			$out.='<div class="clear"></div>';
		
		}
		//
		if(function_exists('atp_pagination')){
			if ( $pagination == 'true' ) {
				$out .=atp_pagination();
			} 	
		}
		wp_reset_query();
		endif; 
	return $out;
}
add_shortcode('musicdjmix','iva_djmix'); //add shortcode
//  List Djmix style
function iva_listdjmix($atts, $content = null,$code) {
	
	extract(shortcode_atts(array(
			'cat'			=> '',
			'limit'			=> '-1',
			'postid'		=> '',
			'order'			=> 'ASC',
			'orderby'		=> 'date',
			'pagination'	=> ''
		), $atts));
		global $default_date;
	
		//Album Image Sizes
		$width='470'; $height = '470';
		$out = $djmix_genre = $djtitle ='';

		if ( get_query_var('paged') ) { 
			$paged = get_query_var('paged'); 
		} elseif ( get_query_var('page') ) { 
			$paged = get_query_var('page');
		}else {
			$paged = 1;
		}

		$orderby = $orderby ? $orderby : get_option('atp_djmix_orderby');
		$order   = $order ? $order : get_option('atp_djmix_order');

		if($orderby == 'djmix_release_date'){
			$meta_key ='djmix_release_date';
		}else{
			$meta_key = '';
		}
		$query = array(
			'post_type'	=> 'djmix', 
			'showposts'	=> $limit, 
			'tax_query' => array(
							'relation' => 'OR',
						),
			'meta_key'  => $meta_key,			
			'paged'		=> $paged,
			'orderby'	=> $orderby,
			'order'		=> $order
		);

		if($cat !=''){
			$cats = explode(',',$cat);
			$tax_artist	=	array(
			'taxonomy'  => 'djmix_cat',
			'field' 	=> 'slug',
			'terms' 	=> $cats
			);
			array_push( $query['tax_query'],$tax_artist);
		}

		$postid_array = array();
		$postid_array = explode(',',$postid);
		if($postid!=''){
			$query = array(
					'post_type'	=> 'djmix', 
					'post__in'	=> $postid_array
			);
		}

		$start=rand(10,111);
		query_posts($query); //get the results
		global $post;
		if ( have_posts()) : while (  have_posts()) :  the_post(); 

		$djmix_release_date = get_post_meta( $post->ID, 'djmix_release_date', true );
	
		if($djmix_release_date !='') { 
	     if(is_numeric($djmix_release_date)){
	      $djmix_release_date= date_i18n($default_date,$djmix_release_date);
	     } 
	    }
		$djmix_genre			= get_post_meta( $post->ID, 'djmix_genre', true );
		$djmix_buy_mix			= get_post_meta( $post->ID, 'djmix_buy_mix', true );
		$djmix_buy_url			= get_post_meta( $post->ID, 'djmix_buy_url', true );
		$djmix_download_url 	= get_post_meta( $post->ID, 'djmix_download_url', true );
		$djmix_download_text 	= get_post_meta( $post->ID, 'djmix_download_text', true ) ? get_post_meta( $post->ID, 'djmix_download_text', true ) : 'Download';
		$audio_posttype_option	= get_post_meta( $post->ID, 'audio_posttype_option', true )? get_post_meta( $post->ID, 'audio_posttype_option', true ) :'player';
		$atp_djmix_like_enable       = get_option( 'atp_djmix_like_btn' ) ? get_option( 'atp_djmix_like_btn' ) : 'true' ;
		$atp_djmix_count_enable      = get_option( 'atp_djmix_count_btn' ) ? get_option( 'atp_djmix_count_btn' ) : 'true' ;
		$djmix_upload_mix		= get_post_meta( $post->ID, 'djmix_upload_mix', true );
		$imagesrc				= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
		$image					= aq_resize($imagesrc[0], '80', '80', true ); 
		$djtitle				= get_the_title($post->ID);
		$img_alt_title 			= get_the_title();
		$post_date			= get_post_meta(get_the_id(),'djmix_release_date',true);
				
				if($post_date !='') { 
					if(is_numeric($post_date)){
						$post_date= date_i18n($default_date,$post_date);
					}	
				}
		/*--------------------------------*/
		$out .= '<div class="djmix-list clearfix">'; 
			if( has_post_thumbnail($post->ID)){ 
			
				$out .='<div class="djmix_thumb">';
				$out .='<figure>'. atp_resize( $post->ID, '', '60','60', '', $img_alt_title ).'</figure>';
				$out .='</div>';
			}
		$out .= '<div class="djmix-content">';
	
		if($audio_posttype_option == 'player'){ 
				$trackcount   = '0';
				$djmix_list   = get_post_meta($post->ID,'djmix_upload_mix',true) ? get_post_meta($post->ID,'djmix_upload_mix',true) :'';	
				$attachment   = get_post( $djmix_list );
				$title		  = $attachment->post_title;
				$attachmentid = $attachment->ID;
				if ( isset( $attachment->trackcount ) ) {
					$trackcount = $attachment->trackcount;
				}

				$trackCountlist = isset($trackcount) ?  $trackcount : '0';

				$out .= '<div class="single-player-meta" id="single-player-meta'.$start.'">'.$djtitle.'</div>';
				$out .= '<h2 class="entry-title djmix-title"><a class="hoverdjmix fap-single-track no-ajaxy"  data-id="'. $attachmentid.'"  data-meta="#single-player-meta'.$start.'" class="fap-single-track no-ajaxy" href="'.$attachment->guid.'" title="'.$title.'" rel="'.$image.'"><i class="ivaplay"></i></a>';
				$out .='<a href="'. get_permalink().'">'.get_the_title().'</a>';
				$out .='</h2>';
		
			
		}elseif($audio_posttype_option == 'externalmp3'){
			$attachmentid  				= '0'.$post->ID;
			$trackCountlist 			= get_post_meta($attachmentid,'trackcount',true) ? get_post_meta($attachmentid,'trackcount',true) : '0';
			$djmix_externalmp3url 		= get_post_meta($post->ID,'djmix_externalmp3',true); 
			$djmix_externalmp3urltitle 	= get_post_meta($post->ID,'djmix_externalmp3title',true); 
			
			$out .='<h2 class="entry-title djmix-title"><a class="hoverdjmix fap-single-track no-ajaxy "  data-id="'.$attachmentid.'"  data-meta="#single-player-meta'.$start.'" class="no-ajaxy" href="'.$djmix_externalmp3url.'" rel="'.$image.'" title="'.$djmix_externalmp3urltitle.'"><i class="ivaplay"></i></a>';
			$out .='<a href="'. get_permalink().'">'.get_the_title().'</a>';
			$out .='</h2>';
		}elseif( $audio_posttype_option == 'medialibrary' ){
			$djmixmediaid = get_post_meta($post->ID,'djmix_medialibrary',false) ? get_post_meta($post->ID,'djmix_medialibrary',false) :'';
			 if( !empty( $djmixmediaid ) ) {
					foreach( $djmixmediaid as $mediaupload_list) {
						$attachment = get_post( $mediaupload_list );
						$trackCountlist ='0';
						$attachmentid  = $attachment->ID;
						if (  isset( $attachment->trackcount ) ) {
							$trackCountlist= $attachment->trackcount;
						}
					}
				}
				$out .='<h2 class="entry-title djmix-title"><a class="hoverdjmix fap-single-track no-ajaxy "  data-id="'.$attachmentid.'"  data-meta="#single-player-meta'.$start.'" class="no-ajaxy" href="'.$attachment->guid.'" rel="'.$image.'" title="'.$attachment->post_title.'"><i class="ivaplay"></i></a>';
				$out .='<a href="'. get_permalink().'">'.get_the_title().'</a>';
				$out .='</h2>';
		}else{ 
				$audio_soundcloud_url = get_post_meta($post->ID,'audio_soundcloud_url',true);
				$attachmentid  = $post->ID;
				$trackCountlist = get_post_meta($attachmentid,'trackcount',true) ? get_post_meta($attachmentid,'trackcount',true) : '0';	

				$out .='<h2 class="entry-title djmix-title"><a data-meta="#single-player-meta'.$start.'"  data-id="'.$attachmentid.'"   class="hoverdjmix fap-single-track no-ajaxy" href="'.$audio_soundcloud_url.'" class="fap-single-track no-ajaxy"><i class="ivaplay"></i></a>';	
				$out .='<a href="'. get_permalink().'">'.get_the_title().'</a>';
				$out .='</h2>';
		}
		$out .= '<div class="djmix-postmeta">';
		if ( $djmix_genre != '' ) { 
			$out .='<span>'.$djmix_genre.'</span>';
		} 
		if (  $post_date != '' ) { $out .='<span>'.$post_date.'</span>'; }
		
		$out .= '</div>';
		
		$out .= '<div class="djmix-buttons">';
		// djmix somg count enable/disable since 3.2.5
		if( $atp_djmix_count_enable != 'on' ){	
			$out .='<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachmentid.'">'.$trackCountlist.'</span></span>';
		}
		// djmix  song like  enable/disale since 3.2.5
		if( $atp_djmix_like_enable != 'on' ){
			$out .= iva_track_like( $attachmentid );
		}
		if ( $djmix_buy_mix != '' ) { 
			$out .='<div class="djbtn"><a href="'.$djmix_buy_url.'" target="_blank"><button type="button" class="btn mini abestos"><span>'.$djmix_buy_mix.'</span></button></a></div>'; 
		}
		
		if ( $djmix_download_url != '' ) {
		
			$download_count = get_post_meta( $attachmentid,'download_count',true);
			
			$out .='<div class="djbtn"><button type="button" class="btn mini abestos music_download" data-id ="'.$attachmentid.'" data-download="'.iva_get_download( $djmix_download_url ).'"><span>'.$djmix_download_text.'</span></button></span><span class="iva_dcount">'.(int)$download_count.'</span></div>';
		}
		$out .='</div>'; //djmix  buttons

		$out .='</div>'; // djmix-content
		$out .= get_the_content();
		$out .= '</div>'; // djmix content
		
	
		$start++;
		endwhile;

		//	
		if(function_exists('atp_pagination')){
			if ( $pagination == 'true' ) {
				$out .=atp_pagination();
			} 	
		}

		wp_reset_query();
		endif; 
	return $out;
}
add_shortcode('djmixlist','iva_listdjmix'); //add shortcode
?>