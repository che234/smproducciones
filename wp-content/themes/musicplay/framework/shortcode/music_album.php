<?php
function iva_album($atts, $content = null,$code) {
	extract(shortcode_atts(array(
			'genres'		=> '',
			'label'			=>'',
			'id'			=> '',
			'postid'		=> '',
			'limit'			=> '-1',
	 		'filter'		=> 'no',
			'ordering'		=> '',
			'order'			=> 'ASC',
			'orderby'		=> 'ID',
			'columns'		=> '1',
			'pagination'	=> ''
		), $atts));
		
		$column_index = 0;

		if( $columns == '4' ) { $class = 'col_fourth'; }
		if( $columns == '3' ) { $class = 'col_third'; }
		if( $columns == '5' ) { $class = 'col_fifth'; }
		if( $columns == '6' ) { $class = 'col_sixth'; }
		if( $columns == '1' ) { $class = 'col_one'; }

		if ( get_query_var('paged') ) { 
			$paged = get_query_var('paged'); 
		} elseif ( get_query_var('page') ) { 
			$paged = get_query_var('page');
		}else {
			$paged = 1;
		}

		$albums_orderby = get_option('atp_album_orderby');
		$albums_order = get_option('atp_album_order');

		$orderby = ($ordering == 'yes') ? $orderby : $albums_orderby;
		$order   = ($ordering == 'yes') ? $order   : $albums_order ;
		

		$add_to_playlist_txt        = get_option('atp_add_to_playlist_txt') ? get_option('atp_add_to_playlist_txt') :  __('Add songs to playlist', 'musicplay');
		$atp_song_like_enable       = get_option( 'atp_song_like_btn' ) ? get_option( 'atp_song_like_btn' ) : 'true' ;
		$atp_song_count_enable      = get_option( 'atp_song_count_btn' ) ? get_option( 'atp_song_count_btn' ) : 'true' ;

		//Album Image Sizes
		$width='470'; $height = '470';
		$out = '';
		if($orderby == 'meta_value_num'){
			$meta_key = 'audio_release_date';
		}else{
			$meta_key = "";
		}

		
		

		if( $filter == 'yes') {
			$class = '';
			$out .= '<ul id="options" class="clearfix splitter"><li>';
			$out .= '<ul id="filters" class="option-set clearfix" data-option-key="filter">';
			$out .= '<li><a class="all selected" href="#show-all" data-option-value="*">All</a></li>';
			
			if( $genres ){
				$iva_port_cat = @explode(',', $genres);
				for( $i = 0; $i < count( $iva_port_cat ); $i++ ){
					$genresterms[] = get_term_by('slug', $iva_port_cat[$i], 'genres');
				}
				if(!empty( $genresterms )){
					foreach( $genresterms as $term ) {
						$out .= '<li>&#9002; <a class="'.$term->slug.'" data-option-value=".' . $term->slug . '" href="#filter">' . $term->name . '</a></li>';
					}
				}
			}
			if( $label ){
				$iva_port_cat = @explode(',', $label);
				for( $i = 0; $i < count( $iva_port_cat ); $i++ ){
					$labelterms[] = get_term_by('slug', $iva_port_cat[$i], 'label');
				}
				
				if(!empty( $labelterms )){
					foreach( $labelterms as $term ) {
						$out .= '<li>&#9002; <a class="'.$term->slug.'" data-option-value=".' . $term->slug . '" href="#filter">' . $term->name . '</a></li>';
					}
				}
			}
			if( empty( $genres )  && empty( $label )){
				$terms = get_terms( array( 'genres'),'' );
				 if ( !empty( $terms ) && !is_wp_error( $terms ) ){
					foreach ( $terms as $term ) {
					  $out .='<li>&#9002; <a class="'.$term->slug.'" data-option-value=".' . $term->slug . '" href="#filter" >' . $term->name . '</a></li>'; 
					} 
				 }
			}			
			$out .= '</ul></li></ul>';
			$out .="<div class='sortable sort_column".$columns."'><div  id='list' class='image-grid'>";
		}


		if($genres !='null' ||  $label !=''){
			$query = array(
				'post_type'			=> 'albums', 
				'showposts'			=> $limit, 
				'tax_query' => array(
						'relation' => 'OR',
				),
				'meta_key'  => $meta_key, // date order by
				'paged'		=> $paged,
				'orderby'	=> $orderby,
				'order'		=> $order
			);
		
			if($genres !=''){
				$term_genres = explode(',',$genres);
				$tax_genres	=	array(
						'taxonomy' => 'genres',
						'field' => 'slug',
						'terms' => $term_genres
				);
				array_push( $query['tax_query'],$tax_genres );
			}
			if($label !=''){
				$term_label = explode(',',$label);
				$tax_label	=	array(
						'taxonomy' => 'label',
						'field' => 'slug',
						'terms' => $term_label
					);
				array_push( $query['tax_query'],$tax_label );
			}
	
		}

		if($postid!=''){
		$postid_array = array();
		$postid_array = explode(',',$postid);
			$query = array(
					'post_type'	=> 'albums', 
					'post__in'	=> $postid_array
			);
		}

		if($id!=''){
		$postid_array = array();
		$postid_array = explode(',',$id);
			$query = array(
					'post_type'	=> 'albums', 
					'post__in'	=> $postid_array
			);
		}
	query_posts($query); //get the results
	global $post,$default_date;
	if(have_posts()) : while(have_posts()) : the_post();
		$audio_genre_music	= get_post_meta( $post->ID, 'audio_genre_music', true );
		$audio_catalog_id	= get_post_meta( $post->ID, 'audio_catalog_id', true );
		$audio_id_display	= get_post_meta( $post->ID, 'audio_upload', true );
		$audio_label		= get_the_term_list( $post->ID, 'label', '', ', ', '' );
		$audio_labels = strip_tags( $audio_label );
		$audio_genres		= get_the_term_list( $post->ID, 'genres', '', ', ', '' );
		$audio_genres = strip_tags( $audio_genres );
		$img_alt_title 		= get_the_title();
		$audio_button_disable 		= get_post_meta( $post->ID, 'audio_button_disable', true );
		$audio_buttons   = get_post_meta( $post->ID, 'audio_buttons', true );

		$genres 		= get_the_terms( get_the_ID(), 'genres');
		$genres_slug = array();
						
		if ( is_array( $genres ) ) {
			foreach( $genres as $term ) {
			
					$genres_slug[] = $term->slug;
				}
		}	
		$audiolabel 		= get_the_terms( get_the_ID(), 'label');				
		$label_slug = array();
			if ( is_array( $audiolabel ) ) {
				foreach( $audiolabel as $term ) {
					$label_slug[] = $term->slug;
				}
			}	

			
		$audio_release_date	= get_post_meta( $post->ID, 'audio_release_date', true );
		if($audio_release_date !='') { 
		 if(is_numeric($audio_release_date)){
		  $audio_release_date= date_i18n($default_date,$audio_release_date);
		 } 
		}
		$audio_artist=array();
			$audio_artist_name =get_post_meta($post->ID,'audio_artist_name',true);
			if ( is_array( $audio_artist_name ) && count( $audio_artist_name ) > 0 ) {
			foreach( $audio_artist_name as $audio_artist_id){
				$audio_artist = array();
				$permalink = get_permalink(  $audio_artist_id);
				$playlisttitle = get_the_title(  $audio_artist_id);
				$audio_artist[]= '<a href="'.$permalink.'">'.$playlisttitle.'</a>';
			}
			$audio_artist_name = implode( ', ', $audio_artist );
			}
		$column_index++;
		$last = ( $column_index == $columns && $columns != 1 ) ? 'end ' : '';

		$permalink = get_permalink( get_the_id() );
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', true );
		
		$lyrics_option	= get_option( 'atp_lyrics_option' ) ?  get_option( 'atp_lyrics_option' ) : 'lightbox' ;
		
		if ( $lyrics_option === 'lightbox' ) {
			$lyrics_box = 'prettyPhoto[inline]';
		}else{
			$lyrics_box = 'lyrics';
		}
			
		$playlisttitles ='<div id="single-player-meta">
						<a href="'.$permalink.'">'.get_the_title($post->ID).'</a>
						<div>'.$audio_label.' | '.$audio_release_date.'</div></div>';

		//if($id!='' && $label== 'null' && $genres== 'null' && $postid =='null' ){
		if($id!=''){
			$audio_posttype_option		= get_post_meta( $post->ID, 'audio_posttype_option', true ) ?  get_post_meta( $post->ID, 'audio_posttype_option', true ) :'player';
			$imagesrc 	= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );
			$image_dis 	= aq_resize($imagesrc[0], '80', '80', true );
			$out.= '<div class="album_widgetmeta">';
			$out .=atp_resize( $post->ID, '', 90, 90, 'album-thumb',$img_alt_title );
				$out .='<div class="album-widget-info">';
						$out .='<strong><a href="' .$permalink. '">'.get_the_title().'</a></strong>';
						$out .='<div class="album-widgetmeta">';
						$out .='<span>'.$audio_release_date.'</span>';
						$out .='<span>'.$audio_artist_name.'</span>';
						$out .='<span>'.get_the_term_list( $post->ID, 'genres', '', ',' , '' ).'</span>';
				$out.='</div>';
				$out .='</div>';
			$out .='</div>';
			// Add tracks to playlist
			if(get_option('atp_playlist_enable') == 'on' ) { 
				$out .='<span class="add-to-playlist-btn"><i class="fa fa-bars fa-fw"></i>&nbsp;<a href="#" class="fap-add-playlist alldata-playlist" data-enqueue="no" data-playlist="unique-playlist-id">'.$add_to_playlist_txt.'</a></span>';
			 }

			$out .="<ul class='fap-my-playlist album-playlist' data-playlist='unique-playlist-id'>";
			$audio_display=array();
			$audio_display=explode(',',$audio_id_display);			
			
			if($audio_posttype_option =='player'){
				$trackcount =0;
				$count = count($audio_display);
				if($count > 1) {
					$i=1;
					foreach($audio_display as $attachment_id) { 
						$attachment = get_post( $attachment_id );
						if( count( $attachment ) > 0) {
							$attachment_title = $attachment->post_title;
							if(  isset( $attachment->trackcount ) ){
								$trackcount = $attachment->trackcount;
							}
							$lyricsdesc	 = $attachment->post_content;
							$buy		 = $attachment->buy;
							$download	 = $attachment->download;
							$download_player_count = get_post_meta( $attachment->ID,'download_count',true);
							
							$out .="<li><a class='fap-single-track no-ajaxy' data-id=".$attachment->ID." data-meta='#single-player-meta' href=".$attachment->guid."  title='".$attachment_title."'  rel='".$imagesrc[0]."'><i class='ivaplay'></i></a>".$attachment_title;
							
							$out .="<div class='mp3options'>";
							// song count enable/disable since 3.2.5
							if( $atp_song_count_enable != 'on') {
								$out .='<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachment->ID.'">'.$trackcount.'</span></span>';
							}
							// song like enable/disable since 3.2.5
							if( $atp_song_like_enable != 'on' ){	
								$out .= iva_track_like($attachment->ID);
							}
							if($buy !='') { 
								$out .='<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
							}
							if($download != '' && $download != '#' ) { 
								$out .='<span class="mp3o download music_download" data-id="'.$attachment->ID.'" data-download="'. iva_get_download($download).'"><i class="fa fa-download fa-lg"></i><span class="iva_dcount">'.(int)$download_player_count.'</span></span>';
							}
							if($lyricsdesc !=''){ 
								$out .='<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $attachment_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
							}
							$out .='</div>';
							if( $lyricsdesc !='' ){
								echo '<div id="'.preg_replace('/\s+/', '-', $attachment_title) .'" class="lyricdesc">';
								echo '<p><strong>'.$attachment_title.'</strong></p>';
								$content = apply_filters('the_content', $attachment->post_content); 
								echo $content;
								echo '</div>';
							}
							$out .='</li>'; 
						}
					}
				} 
			
			}elseif($audio_posttype_option =='externalmp3'){		
				$audiolist  = get_post_meta($post->ID,'audio_mp3',true) ? get_post_meta($post->ID,'audio_mp3',true) :'';
				$count		= count($audiolist);
				if( $count > 0 ) {
					$i=1;
					foreach( $audiolist as $mp3_list ) { 
						$mp3_title  = $mp3_list['mp3title'];
						$mp3id  	= $mp3_list['mp3id'];
						$trackcount = get_post_meta($mp3id,'trackcount',true) ? get_post_meta($mp3id,'trackcount',true) : '0';	
						$download_mp3_count = get_post_meta( $mp3id,'download_count',true)?get_post_meta( $mp3id,'download_count',true):'0';						
						$out .="<li><a class='fap-single-track no-ajaxy' data-id=".$mp3id." data-meta='#single-player-meta' href='".esc_attr($mp3_list['mp3url'])."' title='".$mp3_title."'  rel='".$image."'><i class='ivaplay'></i></a>$mp3_title"; 
						
						$out .="<div class='mp3options'>";
						// song count enable/disable since 3.2.5
						if( $atp_song_count_enable != 'on' ){		
							$out .='<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $mp3id.'">'.$trackcount.'</span></span>';
						}
						// song like enable/disable since 3.2.5
						if( $atp_song_like_enable != 'on') {
							$out .=iva_track_like($mp3id);
						}
						if($buy !='') { 
							$out .='<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
						}
						if($download !='') { 
							$out .= '<span class="mp3o download music_download" data-id="'.$mp3id.'" data-download="'. iva_get_download( $download ).'"><i class="fa fa-download fa-lg"></i><span class="iva_dcount">'.(int)$download_mp3_count.'</span></span>';
						}
						if($lyricsdesc !=''){ 
							$out .='<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $mp3_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
						}
						$out .='</div>';
						if( $lyricsdesc !='' ){
							echo '<div id="'.preg_replace('/\s+/', '-', $mp3_title) .'" class="lyricdesc">';
							echo '<p><strong>'.$mp3_title.'</strong></p>';
							$content = apply_filters('the_content', $mp3_list['lyrics']); 
							echo $content;
							echo '</div>';
						}
						$out .="</li>";
					}
				}
			}elseif( $audio_posttype_option =='soundcloud' ){
			
				$audio_soundcloud_title = get_post_meta($post->ID,'audio_soundcloud_title',true);
				$audio_soundcloud_url 	= get_post_meta($post->ID,'audio_soundcloud_url',true);
				$audio_soundcloud_id 	= get_post_meta($post->ID,'audio_soundcloud_id',true);
				$soundtrackcount 		= isset( $trackcount )  ?  $trackcount :'0';
				
				$out .="<li><a class='fap-single-track no-ajaxy' data-id=".$audio_soundcloud_id." data-meta='#single-player-meta' href='".$audio_soundcloud_url."' title='".$audio_soundcloud_title."'  rel='".$image."'><i class='ivaplay></i></a>".$audio_soundcloud_title;
				
				$out .='<div class="mp3options">';
				// song count enable/disable since 3.2.5
				if( $atp_song_count_enable != 'on' ){
					$out .='<span class="mp3o iva_playCount ivaplay-'. $audio_soundcloud_id.'"><i class="fa fa-play fa-fw"></i>'.$soundtrackcount.'</span>';
				}
				// song like enable/disable since 3.2.5
				if( $atp_song_like_enable != 'on') {
					$out .= iva_track_like($audio_soundcloud_id);
				}
				$out .='</div>';
				$out .="</li>";
			} 
			$out .="</ul>";	
			echo $playlisttitles;
			$out .="<div class='divider thin'></div>";
		 
		 } elseif ( $postid !='')  {
			$out .= '<div class="'.$class.' '.$last.' item element '.implode('  ', $genres_slug).' '.implode('  ', $label_slug).'">'; 
			$out .= '<div class="album-list">'; 
			$out .= '<div class="custompost_entry">';
			if(has_post_thumbnail()){
				$out .='<div class="custompost_thumb port_img">';
				$out .='<figure>';
				$out .= atp_resize( $post->ID, '',$width, $height, '', $img_alt_title );
				$out .='</figure>'; 
				$out .='<div class="hover_type">';
				$out .='<a class="hoveraudio"  href="' .$permalink. '" title="' . get_the_title() . '"></a>';
				$out .='</div>'; 
				$out .='<span class="imgoverlay"></span>'; 
				$out .='</div>';
			}
			$out .='<div class="album-desc">';
			$out .='<h2 class="entry-title"><a href="' .$permalink. '">'.get_the_title().'</a></h2>';
			if ( $audio_label != '' ) {
				$out .='<span>'.$audio_labels.'</span>';
			}
			if ( $audio_genres != '' ) {
				$out .='<span>'.$audio_genres.'</span>';
			}
			$out .= '</div>';
			$out .= '</div>';
			$out .= '</div>';
			$out .= '</div>';

			if( $column_index == $columns ){
			    $column_index = 0;
			    $out.='<div class="clear"></div>';
			}
			
		}elseif($genres !='null' || $label !='null' || $postid =='')  {
			
			$out .= '<div class="'.$class.' '.$last.' item element '.implode('  ', $genres_slug).' '.implode('  ', $label_slug).'">'; 
			$out .= '<div class="custompost_entry album-list">';
			if(has_post_thumbnail()){
				$out .='<div class="custompost_thumb port_img">';
				if ( get_option('atp_hovericon') ) {
					$out .= '<figure><a href="' .$permalink. '" title="' . get_the_title() . '">';
					$out .= atp_resize( $post->ID, '',$width, $height, '', $img_alt_title ); 
					$out .= '</a></figure>';
				}else{
					$out .='<figure>'. atp_resize( $post->ID, '',$width, $height, '', $img_alt_title ) .'</figure>'; 
					$out .='<div class="hover_type"><a class="hoveraudio"  href="' .$permalink. '" title="' . get_the_title() . '"></a></div>'; 
					$out .='<span class="imgoverlay"></span>'; 
				}
				$out .='</div>';
			}
			$out .='<div class="album-desc">';
			$out .='<h2 class="entry-title"><a href="' .$permalink. '">'.get_the_title().'</a></h2>';
			if ( $audiolabel != '' && 	!empty( $label)  ) {
				$out .='<span>'.$audio_labels.'</span>';
			}
			elseif	( $audio_genres != '' && !empty( $genres ) ) {
				$out .='<span>'.$audio_genres.'</span>';
			}
			$out .= '</div>';
			$out .= '</div>';
			$out .= '</div>';
			
			if( $column_index == $columns ){
				$column_index = 0;
				$out.='<div class="clear"></div>';
			}
		}
	endwhile;
	if( $filter == 'yes' ) {  $out .='</div></div>';  }

	if(function_exists('atp_pagination')){
		if($pagination == 'true') {   
			$out .= atp_pagination();  	
		}
	}
	wp_reset_query();
	endif; 
	return $out;
}
add_shortcode('album','iva_album');  // End Shortcode
?>