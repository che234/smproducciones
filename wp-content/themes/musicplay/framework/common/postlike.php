<?php
/***
 * P O S T  L I K E 
 *--------------------------------------------------------
 * iva_get_post_like - like on post
 * @param - int postid - Post ID 
 * @return - int  likecount -  like count
 * 
 */
function iva_get_post_like($postid)
{
	$likecount = get_post_meta($postid,'post_like',true);
	$likecounttxt = get_option('atp_likecounttxt') ? get_option('atp_likecounttxt') : 'Likes';
	$out  = '<span class="iva_like_post" id="iva_like_post'.$postid.'">';
	$out .= '<span class="like">';
	$out .= '<a href="#" data-calss="like" data-action="like" class="PostLike"  data-id='.$postid.'><i class="fa fa-thumbs-o-up fa-lg fa-fw"></i>&nbsp;<span class="likes_count'.$postid.'">'.$likecount.'</span>&nbsp;'.$likecounttxt.'</a>';
	$out .= '</span>';
	$out .= '</span>';
	return $out;
}
function iva_track_like($trackid)
{	$out='';	
	$track_likecount = get_post_meta($trackid,'track_like',true);
	$out .= '<span class="mp3o track_like"><i class="fa fa-thumbs-o-up fa-lg fa-fw"></i><span data-action="like" data-id='.$trackid.' class="likes_count-'.$trackid.'">&nbsp;'.$track_likecount.'</span></span>';
	return $out;
}

/**
	 * Get the actual ip address
	 * @param no-param
	 * @return string
*/
	function GetIpAddress() {
		if (getenv('HTTP_CLIENT_IP')) {
			$ip = getenv('HTTP_CLIENT_IP');
		} elseif (getenv('HTTP_X_FORWARDED_FOR')) {
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		} elseif (getenv('HTTP_X_FORWARDED')) {
			$ip = getenv('HTTP_X_FORWARDED');
		} elseif (getenv('HTTP_FORWARDED_FOR')) {
			$ip = getenv('HTTP_FORWARDED_FOR');
		} elseif (getenv('HTTP_FORWARDED')) {
			$ip = getenv('HTTP_FORWARDED');
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		return $ip;
	}
/**
	 * Get the actual like count
	 * @param no-param
	 * @return int
*/
function iva_post_like()
	{
	
		$can_vote=false;
		$postid=$_POST['post_id'];
		$postcount=get_post_meta( $postid, 'post_like', true );
		$task=$_POST['task'];
		$ip=GetIpAddress();
		$has_already_voted = Alreadyliked( $postid, $ip );
		if ( !$has_already_voted ) {
						$can_vote = true;
		}
		
		if( $can_vote ){
			
			if ( $task == "like" ) {
			
				$like_count = get_post_meta( $postid, 'post_like', true );
				update_post_meta( $postid, 'post_like', $like_count+1 );
				$postcount = get_post_meta( $postid, 'post_like', true );
				
			}
			
			
		}
		
		echo  $postcount;
		exit();
	}

	/**
	 * Get the like or liked 
	 *	@param - int post_id - Post ID
	 * @param - int ip -  ip address
	 * @return boolean
	*/
	function Alreadyliked($post_id, $ip)
		{
				$return =0;		
				$get_ip=get_post_meta($post_id, 'like_ip', true);
				if( $ip == $get_ip)
				{
				$return= 1;
				}else{
				update_post_meta($post_id,'like_ip',$ip);
				}
			return $return; 
		}

	add_action('wp_ajax_iva_like_post_process_vote', 'iva_post_like');
	add_action('wp_ajax_nopriv_iva_like_post_process_vote', 'iva_post_like');

function iva_track_liked()
	{

		$can_vote=false;
		$track_id=$_POST['track_id'];
		$postcount=get_post_meta( $track_id, 'track_like', true );
		$task=$_POST['task'];
		$ip=GetIpAddress();
		$has_already_voted = Alreadyliked( $track_id, $ip );
		if ( !$has_already_voted ) {
						$can_vote = true;
		}
		
		if( $can_vote ){
			
			if ( $task == "like" ) {
			
				$like_count = get_post_meta( $track_id, 'track_like', true ) ? get_post_meta( $track_id, 'track_like', true ) : '0';
				update_post_meta( $track_id, 'track_like', $like_count+1 );
				$postcount = get_post_meta( $track_id, 'track_like', true );
				
			}
			
			
		}
		
		echo  $postcount;
		exit();
	}
	add_action('wp_ajax_iva_track_like', 'iva_track_liked');
	add_action('wp_ajax_nopriv_iva_track_like', 'iva_track_liked');
?>