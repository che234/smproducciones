<?php
/**
 * testimonials meta box
 */

 $prefix = '';
 $this->meta_box[] = array(
	'id'		=> 'Testimonial-meta-box',
	'title'		=> 'Testimonial Options',
	'page'		=> array('testimonialtype'),
	'context'	=> 'normal',
	'priority'	=> 'core',
	'fields'	=> array(

				/**
				 * client picture
				 */
				array(
					'name'	=> __('Client Picture','ATP_ADMIN_SITE'),
					'desc'	=> 'Select the Image type/mode you wish to use either Gravatar or Custom Image.',
					'id'	=> $prefix . 'testimonial_image_option',
					'std'	=> 'gravatar',
					'type'	=> 'select',
					'options'=> array(
									"gravatar"	=> "Gravatar Logo",
									"customimage"	=>'Upload Picture')
				),
			
				/**
				 * upload photo
				 */
				array(
					'name'	=> __('Upload Photo','ATP_ADMIN_SITE'),
					'desc'	=> 'Upload Image from media library or from your desktop.',
					'id'	=> 'testimonial_photo',
					'class'	=> 'testimonialoption customimage',
					'std'	=> '',
					'type'	=> 'upload',
				),
				/**
				 * email id
				 */
				array(
					'name'	=> __('Email ID','ATP_ADMIN_SITE'),
					'desc'	=> 'If you are using gravtar logo enter the email id here,email id will not be displayed on the frontend.',
					'id'	=> 'testimonial_email',
					'std'	=> '',
					'title'	=> 'Name',
					'type'	=> 'text',
				),
				/**
				 * company name
				 */
				array(
					'name'	=> __('Company Name','ATP_ADMIN_SITE'),
					'desc'	=> 'Enter company Name/individual.',
					'id'	=> 'company_name',
					'std'	=> '',
					'title'	=> 'Company Name',
					'type'	=> 'text',
				),
				/**
				 * website url
				 */
				array(
					'name'	=> __('Website URL','ATP_ADMIN_SITE'),
					'desc'	=> 'Type the URL you wish to display. excluding any protocols (ex:http://) .',
					'id'	=> 'website_url',
					'std'	=> '',
					'title'	=> 'Website URL',
					'type'	=> 'text',
				),
				/**
				 * website name
				 */
				array(
					'name'	=> __('Website Name','ATP_ADMIN_SITE'),
					'desc'	=> 'Enter the website name you wish to display for the website url if left empty complete website url will be displayed.',
					'id'	=> 'website_name',
					'std'	=> '',
					'title'	=> 'Website Name',
					'type'	=> 'text',
				),

				// CUSTOM SEARCH ENABLE/DISABLE 
				//--------------------------------------------------------
				array(
						'name'		=> __('Custom Search Bar','ATP_ADMIN_SITE'),
						'desc'		=> 'Check this if you wish to disable the custom music search bar just below the header in inner page.',
						'id'		=> $prefix.'search_page_visible',
						'std' 		=> 'off',
						'type'		=> 'checkbox',
				),


			)
		);
	?>