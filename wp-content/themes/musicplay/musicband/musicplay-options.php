<?php

/**
 * Music Settings Options
 * Appends to musicplay theme options
 * Tabs : music settings
 *		  player settings
 **/

add_filter( 'custompost_themeoptions','atp_custompost_options' );
function atp_custompost_options( $iva_options )
{
	global $iva_options, $atp_options, $url, $shortname, $atp_theme;
	
	
	//------------------------------------------------------------------------------------------------
	$iva_options[] = array( 'name' => 'Music Settings', 'icon' => $url.'nav-icon.png', 'type' => 'heading' );
	//------------------------------------------------------------------------------------------------

		$iva_options[] = array( 'name'	=> 'Image Overlay Effect',
								'desc'	=> 'Check this if you wish to disable the Image Overlay Effect on albums and photos',
								'id'  	=> $shortname.'_img_over_effect',
								'std' 	=> '',
								'type' 	=> 'checkbox');

		$iva_options[] = array(
					'name'			=> 'Hover Icon Disable - Albums, Artists, Gallery and Video',
					'desc'			=> 'Check this if you wish to display hover icons in page template and shortcodes. This will make your albums single clickable.',
					'id'			=> $shortname.'_hovericon',
					'class'			=> '',
					'std'			=> '',
					'type'			=> 'checkbox',
					'inputsize'		=> '',
		);


		//---------------------------------------------------------------------------------------------------
		$iva_options[] = array( 'name'	=> 'Custom Search Bar Properties', 'desc' => 'Disable or enable the search option shown below the header area. .', 'type' => 'subsection');
		//---------------------------------------------------------------------------------------------------
	

		$iva_options[] = array(
				'name'			=> 'Custom Search Bar',
				'desc'			=> 'Check this if you wish to Disable the Custom Search Bar which displays below header.',
				'id'			=> $shortname.'_custom_search',
				'class'			=> '',
				'std'			=> 'disable',
				'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
				'type'			=> 'radio',
		);
	
		$iva_options[] = array(
					'name'			=> 'Search Pagination',
					'desc'			=> 'Check this if you wish to display pagination in search results page',
					'id'			=> $shortname.'_search_pagination',
					'std'			=> '',
					'type'			=> 'checkbox',
			);
	
		$iva_options[] = array(
					'name'			=> 'Genres and labels Buttons',
					'desc'			=> 'Check this if you wish to Disable Genres and labels which displays on custom search bar below header.',
					'id'			=> $shortname.'_genres_labels_display',
					'class'			=> '',
					'std'			=> 'enable',
					'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
					'type'			=> 'checkbox',
		);

		$iva_options[] = array(
					'name'			=> 'Albums Limit on Results Page',
					'desc'			=> 'Type the limits for Albums you wish to limit on the search results page  (e.g: 3)',
					'id'			=> $shortname.'_search_album_limits',
					'class'			=> 'search_limits',
					'std'			=> '',
					'type'			=> 'text',
					'inputsize'		=> '',
		);
		
		$iva_options[] = array(
					'name'			=> 'Artists Limit on Results Page',
					'desc'			=> 'Type the limits for Artists you wish to limit on the search results page  (e.g: 3)',
					'id'			=> $shortname.'_search_artist_limits',
					'class'			=> 'search_limits',
					'std'			=> '',
					'type'			=> 'text',
					'inputsize'		=> '',
		);
		
		$iva_options[] = array(
					'name'			=> 'Djmix Limit on Results Page',
					'desc'			=> 'Type the limits for Artists you wish to limit on the search results page  (e.g: 3)',
					'id'			=> $shortname.'_search_djmix_limits',
					'class'			=> 'search_limits',
					'std'			=> '',
					'type'			=> 'text',
					'inputsize'		=> '',
		);
		

		$iva_options[] = array(
					'name'			=> 'Tracks Limit on Results Page',
					'desc'			=> 'Type the limits for Tracks you wish to limit on the search results page  (e.g: 3)',
					'id'			=> $shortname.'_search_track_limits',
					'class'			=> 'search_limits',
					'std'			=> '',
					'type'			=> 'text',
					'inputsize'		=> '',
		);

		$iva_options[] = array(
				'name'			=> 'Genres Button Color',
				'desc'			=> 'Select the color variation for the button labels',
				'id'			=> $shortname.'_search_genres',
				'std'			=> '',
				'type'			=> 'select',
				'class'			=> 'select300',
				'inputsize'		=> '',
				'options'		=> array( 
										 '' 			=> 'Choose one...',
										'greensea'	 	=> 'Green Sea',
										'nephritis' 	=> 'Nephritis',
										'belizehole' 	=> 'Belize Hole',
										'wisteria' 		=> 'Wisteria',
										'midnightblue' 	=> 'Midnight Blue',
										'orange'		=> 'Orange',
										'pumpkin' 		=> 'Pumpkin',
										'pomegranate' 	=> 'Pomegranate',
										'silver' 		=> 'Silver',
										'abestos' 		=> 'Abestos',
										'black' 		=> 'Black',
										'white' 		=> 'White'
									),
		);

		$iva_options[] = array(
				'name'			=> 'Labels Button Color',
				'desc'			=> 'Select the color variation for the button labels',
				'id'			=> $shortname.'_search_labels',
				'std'			=> '',
				'type'			=> 'select',
				'class'			=> 'select300',
				'inputsize'		=> '',
				'options'		=> array( 
										 '' 			=> 'Choose one...',
										'greensea'	 	=> 'Green Sea',
										'nephritis' 	=> 'Nephritis',
										'belizehole' 	=> 'Belize Hole',
										'wisteria' 		=> 'Wisteria',
										'midnightblue' 	=> 'Midnight Blue',
										'orange'		=> 'Orange',
										'pumpkin' 		=> 'Pumpkin',
										'pomegranate' 	=> 'Pomegranate',
										'silver' 		=> 'Silver',
										'abestos' 		=> 'Abestos',
										'black' 		=> 'Black',
										'white' 		=> 'White'
									),
		);

		$iva_options[] = array(
				'name'			=> 'Search Button Color',
				'desc'			=> 'Select the color variation for the  Submit button',
				'id'			=> $shortname.'_search_button',
				'std'			=> '',
				'type'			=> 'select',
				'class'			=> 'select300',
				'inputsize'		=> '',
				'options'		=> array( 
										 '' 			=> 'Choose one...',
										'greensea'	 	=> 'Green Sea',
										'nephritis' 	=> 'Nephritis',
										'belizehole' 	=> 'Belize Hole',
										'wisteria' 		=> 'Wisteria',
										'midnightblue' 	=> 'Midnight Blue',
										'orange'		=> 'Orange',
										'pumpkin' 		=> 'Pumpkin',
										'pomegranate' 	=> 'Pomegranate',
										'silver' 		=> 'Silver',
										'abestos' 		=> 'Abestos',
										'black' 		=> 'Black',
										'white' 		=> 'White'
									),
		);

		$iva_options[] = array(	'name'	=> 'Genres/Labels Link',
								'desc'	=> 'Select the link color you wish to use for the links displays in the genres and labels in custom search bar.',
								'id'	=> $shortname.'_quickaccess_link',
								'std'	=> '', 
								'type'	=> 'color');

	
		$iva_options[] = array(	'name'	=> 'Genres/Labels Link Hover',
								'desc'	=> 'Select the link hover color you wish to use for the links displays in the genres and labels in custom search bar.',
								'id'	=> $shortname.'_quickaccess_linkhover',
								'std'	=> '', 
								'type'	=> 'color');

		$iva_options[] = array(	'name'	=> 'Custom Search Bar Color',
								'desc'	=> 'Select the background color for the custom search for which displays below header.',
								'id'	=> $shortname.'_cus_search_bgcolor',
								'std'	=> '', 
								'type'	=> 'color');


		//---------------------------------------------------------------------------------------------------
		$iva_options[] = array( 'name'	=> 'Albums Properties', 'desc' => 'Select  Pagination, Limit, Orderby and Order settings.', 'type' => 'subsection', 'class'=>'album-section');
		//---------------------------------------------------------------------------------------------------
		

		$iva_options[] = array(	'name' => 'Add To Playlist',
								'desc' => 'Check this if you wish to enable the Add To Playlist in albums single page ( Default: Disabled ).',
								'id'   => $shortname.'_playlist_enable',
								'std'  => '',
								'type' => 'checkbox',
		);

		$iva_options[] = array(	'name'	=> 'Post Likes',
								'desc'	=> 'Check this if you wish to disable the Post Likes in Albums Single page',
								'id'	=> $shortname.'_like_btn',
								'std'	=> '', 
								'type'	=> 'checkbox');

		$iva_options[] = array(	'name'	=> 'More Albums From This Label',
								'desc'	=> 'Check this if you wish to disable <mark>More Albums From This Label</mark> in Albums Single page',
								'id'	=> $shortname.'_more_albums_label',
								'std'	=> '', 
								'type'	=> 'checkbox');
		$iva_options[] = array(	'name'	=> 'More Albums From This Genres',
								'desc'	=> 'Check this if you wish to disable <mark>More Albums From This Genres</mark> in Albums Single page',
								'id'	=> $shortname.'_more_albums_genres',
								'std'	=> '', 
								'type'	=> 'checkbox');


		/**
		 * Albums
		 * ------------------------
		 */
		
		$iva_options[] = array(
					'name'			=> 'Album Pagination',
					'desc'			=> 'Check this if you wish to display pagination in Albums page template',
					'id'			=> $shortname.'_audio_pagination',
					'class'			=> 'audio_pagination',
					'std'			=> '',
					'type'			=> 'checkbox',
					'inputsize'		=> '',
		);
	
		$iva_options[] = array(
					'name'			=> 'Albums Limit',
					'desc'			=> 'Type the limits for Albums you wish to limit on the page  (e.g: 5)',
					'id'			=> $shortname.'_audio_limits',
					'class'			=> 'audio_limits',
					'std'			=> '',
					'type'			=> 'text',
					'inputsize'		=> '',
		);
		$iva_options[] = array(
				'name'			=> 'Album Orderby',
				'desc'			=> 'Select the orderby  which you want to use  Id ,title,date or menu order in Albums page template',
				'id'			=> $shortname.'_album_orderby',
				'class'			=> 'album_orderby',
				'std'			=> '',
				'type'			=> 'select',
				'class'			=> 'select300',
				'inputsize'		=> '',
				'options'		=> array( 
										'ID' 					=> 'ID',
										'title'					=> 'Title',
										'meta_value_num' 		=> 'Audio Release Date',
										'rand'					=> 'Random',
										'menu_order'			=> 'Menu Order'
									),
		);
		$iva_options[] = array(
				'name'			=> 'Album Order',
				'desc'			=> 'Select the order which you wish to display in Albums Page Template',
				'id'			=> $shortname.'_album_order',
				'class'			=> 'album_order',
				'std'			=> 'ASC',
				'type'			=> 'radio',
				'inputsize'		=> '',
				'options'		=> array( 
										'ASC' 			=> 'Ascending',
										'DSC'			=> 'Descending'
									),

		);

		$iva_options[] = array(
				'name'			=> 'Lyrics Lightbox/Toggle',
				'desc'			=> 'Check this if you wish to Lightbox/Toggle ( Default: Lightbox ).',
				'id'			=> $shortname.'_lyrics_option',
				'class'			=> '',
				'std'			=> 'lightbox',
				'options'		=> array('lightbox' => 'Lightbox','toggle' => 'Toggle'),
				'type'			=> 'radio',
		);

		$iva_options[] = array(
				'name'			=> 'Comments',
				'desc'			=> 'Check this if you wish to Enable/Disable comments on albums single page ( default: disable ).',
				'id'			=> $shortname.'_album_comments',
				'class'			=> '',
				'std'			=> 'disable',
				'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
				'type'			=> 'radio',
		);

		$iva_options[] = array(
				'name'			=> 'Album Featured Image',
				'desc'			=> 'Check this if you wish to disable the featured image lightbox option in album single page ( Default: Enable ).',
				'id'			=> $shortname.'_album_lightbox',
				'class'			=> '',
				'std'			=> 'enable',
				'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
				'type'			=> 'radio',
		);

		$iva_options[] = array(	'name'	=> 'Songs Like Option',
								'desc'	=> 'Check this if you wish to disable the songs Like option on albums single page',
								'id'	=> $shortname.'_song_like_btn',
								'std'	=> '', 
								'type'	=> 'checkbox');

		$iva_options[] = array(	'name'	=> 'Download All',
								'desc'	=> 'Check this if you wish to disable the Download All Button on albums single page',
								'id'	=> $shortname.'_download_all_btn',
								'std'	=> '', 
								'type'	=> 'checkbox');

		//---------------------------------------------------------------------------------------------------
		$iva_options[] = array( 'name'	=> 'Artist Properties', 'desc' => 'Pagination, Limit, Orderby and Order settings ', 'type' => 'subsection');
		//---------------------------------------------------------------------------------------------------
				
		/**
		 * Artist
		 * ------------------------
		 */
		$iva_options[] = array(
					'name'			=> 'Artists Pagination',
					'desc'			=> 'Check this if you wish to display pagination in Artists page template',
					'id'			=> $shortname.'_artist_pagination',
					'class'			=> 'artist_pagination',
					'std'			=> '',
					'type'			=> 'checkbox',
					'inputsize'		=> '',
		);
	
		$iva_options[] = array(
					'name'			=> 'Artists Limit',
					'desc'			=> 'Type the limits for Artits you wish to limit on the page  (e.g: 5)',
					'id'			=> $shortname.'_artist_limits',
					'class'			=> 'artist_limits',
					'std'			=> '',
					'type'			=> 'text',
					'inputsize'		=> '',
					
		);
		$iva_options[] = array(
					'name'			=> 'Artist Orderby',
					'desc'			=> 'Select the orderby  which you want to use  Id ,title,date or menu order in Artist page template',
					'id'			=> $shortname.'_artist_orderby',
					'class'			=> 'artist_orderby',
					'std'			=> '',
					'class'			=> 'select300',
					'type'			=> 'select',
					'inputsize'		=> '',
					'options'		=> array( 
											'ID' 			=> 'ID',
											'title'			=> 'Title',
											'date' 			=> 'Date',
											'menu_order'	=> 'Menu Order'
										),
		);
		$iva_options[] = array(
					'name'			=> 'Artist Order',
					'desc'			=> 'Select the order which you wish to display in Artists Page Template',
					'id'			=> $shortname.'_artist_order',
					'class'			=> 'artist_order',
					'std'			=> 'ASC',
					'type'			=> 'radio',
					'inputsize'		=> '',
					'options'		=> array( 
											'ASC' 			=> 'Ascending',
											'DSC'			=> 'Descending'
										),
		);
	
	
		$iva_options[] = array(
					'name'			=> 'Comments',
					'desc'			=> 'Check this if you wish to Enable/Disable comments on artist single page ( default: disable ).',
					'id'			=> $shortname.'_artist_comments',
					'class'			=> '',
					'std'			=> 'disable',
					'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
					'type'			=> 'radio',
		);
	
	
	
		$iva_options[] = array(
				'name'			=> 'Artist Featured Image',
				'desc'			=> 'Check this if you wish to disable the featured image lightbox option in artist single page ( Default: Enable ).',
				'id'			=> $shortname.'_artist_lightbox',
				'class'			=> '',
				'std'			=> 'enable',
				'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
				'type'			=> 'radio',
		);

		$iva_options[] = array(	'name'	=> 'More Artist From',
								'desc'	=> 'Check this if you wish to disable <mark>More Artist From</mark> section in artist single page',
								'id'	=> $shortname.'_artist_related',
								'std'	=> '', 
								'type'	=> 'checkbox');

		$iva_options[] = array(	'name'	=> 'More Albums From',
								'desc'	=> 'Check this if you wish to disable <mark>More Albums from</mark> section in artist single page',
								'id'	=> $shortname.'_artist_albums',
								'std'	=> '', 
								'type'	=> 'checkbox');

	
	//---------------------------------------------------------------------------------------------------
	$iva_options[] = array( 'name'	=> 'Djmix Properties', 'desc' => 'Pagination, Limit, Orderby and Order settings ', 'type' => 'subsection');
	//---------------------------------------------------------------------------------------------------
	
	/**
	 * DjMix
	 * ------------------------
	 */

	$iva_options[] = array(
				'name'			=> 'DJMix Pagination',
				'desc'			=> 'Check this if you wish to display pagination in DJMix page template',
				'id'			=> $shortname.'_djmix_pagination',
				'class'			=> 'djmix_pagination',
				'std'			=> '',
				'type'			=> 'checkbox',
				'inputsize'		=> '',
	);
	$iva_options[] = array(
				'name'			=> 'DJMix Limit',
				'desc'			=> 'Type the limits for DJMix you wish to limit on the page  (e.g: 5)',
				'id'			=> $shortname.'_djmix_limits',
				'class'			=> 'djmix_limits',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
				
	);
	$iva_options[] = array(
				'name'			=> 'Djmix Orderby',
				'desc'			=> 'Select the orderby  which you want to use  Id ,title,date or menu order in Djmix page template',
				'id'			=> $shortname.'_djmix_orderby',
				'class'			=> 'djmix_orderby',
				'std'			=> '',
				'class'			=> 'select300',
				'type'			=> 'select',
				'inputsize'		=> '',
				'options'		=> array( 
										'ID' 				=> 'ID',
										'title'				=> 'Title',
										'djmix_release_date'=> 'Djmix Release Date',
										'date' 				=> 'Date',
										'menu_order'		=> 'Menu Order'
									),
	);

	$iva_options[] = array(
				'name'			=> 'Djmix Order',
				'desc'			=> 'Select the order which you wish to display in DJMix Page Template',
				'id'			=> $shortname.'_djmix_order',
				'class'			=> '',
				'std'			=> 'ASC',
				'type'			=> 'radio',
				'inputsize'		=> '',
				'options'		=> array( 
										'ASC' 			=> 'Ascending',
										'DSC'			=> 'Descending'
									),
	);


	$iva_options[] = array(	'name'	=> 'Djmix Songs Like Option',
							'desc'	=> 'Check this if you wish to disable the post likes in djmix single page',
							'id'	=> $shortname.'_djmix_like_btn',
							'std'	=> '', 
							'type'	=> 'checkbox');

	$iva_options[] = array(	'name'	=> 'Djmix Songs Played Counter',
							'desc'	=> 'Check this if you wish to disable the song played counter on djmix single page',
							'id'	=> $shortname.'_djmix_count_btn',
							'std'	=> '', 
							'type'	=> 'checkbox');

	//---------------------------------------------------------------------------------------------------
	$iva_options[] = array( 'name'	=> 'Events Properties', 'desc' => 'Pagination, Limit, Countdown Settings ', 'type' => 'subsection');
	//---------------------------------------------------------------------------------------------------
	
	/**
	 * Events
	 * ------------------------
	 */

	$iva_options[] = array(
			'name'		=> 'Time Format',
			'desc'		=> __('Select time format which appears at the admin events meta and front page','ATP_ADMIN_SITE'),
			'id'		=> $shortname.'_timeformat',
			'std'		=> '',
			'type'		=> 'select',
			'class'		=> 'select300',
			'inputsize'	=> '',
			'options' 	=> array( '12' => '12 Hours', '24' => '24 Hours' )
	);

	$iva_options[] = array(
			'name'		=> 'Events Countdown',
			'desc'		=> __('Check this if you wish to disable the events countdown for the events.','ATP_ADMIN_SITE'),
			'id'		=> $shortname.'_events_cd',
			'std'		=> '',
			'type'		=> 'checkbox',
			'inputsize'	=> '',
	);

	$iva_options[] = array(
				'name'			=> 'Events Pagination',
				'desc'			=> 'Check this if you wish to display pagination in Events Page Template(s)',
				'id'			=> $shortname.'_event_pagination',
				'class'			=> 'event_pagination',
				'std'			=> '',
				'type'			=> 'checkbox',
				'inputsize'		=> '',
	);

	$iva_options[] = array(
				'name'			=> 'Events Limit',
				'desc'			=> 'Type the limits for Events Page Template(s) you wish to display.',
				'id'			=> $shortname.'_event_limits',
				'class'			=> '',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
				
	);

	$iva_options[] = array(
				'name'			=> 'Comments',
				'desc'			=> 'Check this if you wish to Enable/Disable comments on events single page ( default: disable ).',
				'id'			=> $shortname.'_events_comments',
				'class'			=> '',
				'std'			=> 'disable',
				'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
				'type'			=> 'radio',
	);
	
	//---------------------------------------------------------------------------------------------------
	$iva_options[] = array( 'name'	=> 'Gallery Properties', 'desc' => 'Pagination, Limit, Orderby and Order settings ', 'type' => 'subsection');
	//---------------------------------------------------------------------------------------------------
	
	/**
	 * Gallery
	 * ------------------------
	 */

	$iva_options[] = array(
				'name'			=> 'Gallery Pagination',
				'desc'			=> 'Check this if you wish to display pagination in Gallery page template',
				'id'			=> $shortname.'_gallery_pagination',
				'class'			=> 'gallery_pagination',
				'std'			=> '',
				'type'			=> 'checkbox',
				'inputsize'		=> '',
	);
	$iva_options[] = array(
				'name'			=> 'Galleries Limit',
				'desc'			=> 'Type the limits for Gallery you wish to limit on the page  (e.g: 5)',
				'id'			=> $shortname.'_gallery_limits',
				'class'			=> 'gallery_limits',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
	);
	$iva_options[] = array(
				'name'			=> 'Gallery Orderby',
				'desc'			=> 'Select the orderby  which you want to use  Id ,title,date or menu order in Gallery page template',
				'id'			=> $shortname.'_gallery_orderby',
				'class'			=> 'gallery_orderby',
				'std'			=> '',
				'type'			=> 'select',
				'class'			=> 'select300',
				'inputsize'		=> '',
				'options'		=> array( 
										'ID' 			=> 'ID',
										'title'			=> 'Title',
										'date' 			=> 'Date',
										'menu_order'	=> 'Menu Order'
									),
	);
	$iva_options[] = array(
				'name'			=> 'Gallery Order',
				'desc'			=> 'Select the order which you wish to display in Gallery Page Template',
				'id'			=> $shortname.'_gallery_order',
				'class'			=> 'gallery_order',
				'std'			=> 'ASC',
				'type'			=> 'radio',
				'inputsize'		=> '',
				'options'		=> array( 
										'ASC' 			=> 'Ascending',
										'DSC'			=> 'Descending'
									),
	);
	$iva_options[] = array(
				'name'			=> 'Comments',
				'desc'			=> 'Check this if you wish to Enable/Disable comments on gallery single page ( default: disable ).',
				'id'			=> $shortname.'_gallery_comments',
				'class'			=> '',
				'std'			=> 'disable',
				'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
				'type'			=> 'radio',
	);
	$iva_options[] = array(
				'name'			=> 'Single page galleries items',
				'desc'			=> 'Type the limit for galleries you wish to display on the gallery single page.',
				'id'			=> $shortname.'_single_gallery_limits',
				'class'			=> 'gallery_limits',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
	);
	$iva_options[] = array(
				'name'			=> 'Gallery Pagination',
				'desc'			=> 'Check this if you wish to display pagination in gallery single page',
				'id'			=> $shortname.'_single_gallery_pagination',
				'class'			=> 'gallery_pagination',
				'std'			=> '',
				'type'			=> 'checkbox',
				'inputsize'		=> '',
	);

	$iva_options[] = array(
				'name'			=> 'Gallery Image Caption',
				'desc'			=> 'Check this if you wish to display caption on gallery single page',
				'id'			=> $shortname.'_single_gallery_imagecaption',
				'class'			=> 'gallery_pagination',
				'std'			=> '',
				'type'			=> 'checkbox',
				'inputsize'		=> '',
	);
	//---------------------------------------------------------------------------------------------------
		$iva_options[] = array( 'name'	=> 'Select Video Properties', 'desc' => '<h3>Video Properties</h3> Select  Pagination,Limit,Orderby and Order settings ', 'type' => 'subsection');
	//---------------------------------------------------------------------------------------------------


	/**
	 * Video
	 * ------------------------
	 */

	$iva_options[] = array(
				'name'			=> 'Video Pagination',
				'desc'			=> 'Check this if you wish to display pagination in Video page template',
				'id'			=> $shortname.'_video_pagination',
				'class'			=> 'video_pagination',
				'std'			=> '',
				'type'			=> 'checkbox',
				'inputsize'		=> '',
	);

	$iva_options[] = array(
				'name'			=> 'Video Limits',
				'desc'			=> 'Type the limits for Videos you wish to limit on the page',
				'id'			=> $shortname.'_video_limits',
				'class'			=> 'video_limits',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
				
	);
	$iva_options[] = array(
				'name'			=> 'Video Orderby',
				'desc'			=> 'Select the orderby  which you want to use  Id ,title,date or menu order in Video page template',
				'id'			=> $shortname.'_video_orderby',
				'class'			=> 'video_orderby',
				'std'			=> '',
				'type'			=> 'select',
				'class'			=> 'select300',
				'inputsize'		=> '',
				'options'		=> array( 
										'ID' 			=> 'ID',
										'title'			=> 'Title',
										'date' 			=> 'Date',
										'menu_order'	=> 'Menu Order'
									),
	);
	$iva_options[] = array(
				'name'			=> 'Video Order',
				'desc'			=> 'Select the order which you wish to display in Video Page Template',
				'id'			=> $shortname.'_video_order',
				'class'			=> 'video_order',
				'std'			=> 'ASC',
				'type'			=> 'radio',
				'inputsize'		=> '',
				'options'		=> array( 
										'ASC' 			=> 'Ascending',
										'DSC'			=> 'Descending'
									),
	);
	$iva_options[] = array(
				'name'			=> 'Comments',
				'desc'			=> 'Check this if you wish to Enable/Disable comments on video single page ( default: disable ).',
				'id'			=> $shortname.'_video_comments',
				'class'			=> '',
				'std'			=> 'disable',
				'options'		=> array('enable' => 'Enable','disable' => 'Disable'),
				'type'			=> 'radio',
	);
	//Music Player
	//------------------------------------------------------------------------
	$iva_options[] = array( 'name' => 'Player Options', 'icon' => $url.'music-icon.png', 'type' => 'heading' );
	//------------------------------------------------------------------------------------------------

	

	$iva_options[] = array(
				'name'			=> 'Player',
				'desc'			=> 'Check this if you wish to enable the Audio Player in the footer ( Default: Disabled ).',
				'id'			=> $shortname.'_audio_enable',
				'class'			=> 'audio_visible',
				'std'			=> '',
				'type'			=> 'checkbox',
	);




	$iva_options[] = array(
				'name'			=> 'Player Toggle',
				'desc'			=> 'Check this if you wish the enable the toggle option for the Audio Player in bottom',
				'id'			=> $shortname.'_audio_visible',
				'class'			=> 'audio_visible',
				'std'			=> '',
				'type'			=> 'checkbox',
				
	);

	$iva_options[] = array(
				'name'			=> 'Autoplay',
				'desc'			=> 'Check this if you wish to Play the audio automatically after the page is loaded.',
				'id'			=> $shortname.'_audio_autoplay',
				'class'			=> 'audio_autoplay',
				'std'			=> '',
				'type'			=> 'checkbox'
	);

	$iva_options[] = array(
				'name'			=> 'Volume',
				'desc'			=> 'Select Audio Player Volume ( Default: 50% ).',
				'id'			=> $shortname.'_playlist_volume',
				'std'			=> '0.5',
				'class'			=> 'select300',
				'options'		=> array(	'0.1'=>'10%',
											'0.2'=>'20%',
											'0.3'=>'30%',
											'0.4'=>'40%',
											'0.5'=>'50%',
											'0.6'=>'60%',
											'0.7'=>'70%',
											'0.8'=>'80%',
											'0.9'=>'90%',
											'1'=>'100%'
									),
				'type'			=> 'select',
		);
	$iva_options[] = array(
				'name'			=> 'Play Next',
				'desc'			=> 'Check this if you wish to play next song and loop in the Audio Player.',
				'id'			=> $shortname.'_audio_player_next',
				'class'			=> 'audio_player_next',
				'std'			=> '',
				'type'			=> 'checkbox'
	);
	
		$iva_options[] = array(
				'name'			=> 'Player Pop-Up',
				'desc'			=> 'Check this if you wish to Enable a pop-up player button the Audio Player.',
				'id'			=> $shortname.'_popup_player_button',
				'std'			=> '',
				'type'			=> 'checkbox'
	);

	$iva_options[] = array( 'name'	=> 'Player Radio/Audio',
						'desc'	=> 'Select how you wish to populate the sticky player in the footer with Radio, Albums or DJMix. If selected as radio the radio settings will be there in the bottom and if Albums Selected the Album Page ID should be added and same for the DJMixes as well',
						'id'	=> $shortname.'_audio_player',
						'std'	=> '',
						'class'	=> 'select300',
						'options' => array( 
										'album'   => 'Album',
										'djmix'   => 'Djmix',
										'radio'	=> 'Radio'  ),
						'type'	=> 'select');

	$iva_options[] = array(
				'name'			=> 'Album ID',
				'desc'			=> 'Type the album ID(s) comma separated if more than 1 Album ID to hold the audios to play in Audio Player',
				'id'			=> $shortname.'_audio_page_id',
				'class'			=> 'audio_player album',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
	);
	$iva_options[] = array(
				'name'			=> 'Djmix ID',
				'desc'			=> 'Type the DJMix ID(s) comma separated if more than 1 DJmix ID to hold the audios to play in Audio Player',
				'id'			=> $shortname.'_djmix_page_id',
				'class'			=> 'audio_player djmix',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
	);
	
	$iva_options[] = array(
				'name'			=> 'Radio Streaming IP:Port',
				'desc'			=> 'Enter your streaming URL IP:Port<br>Player works with Icecast and ShoutCast servers that serve MP3 or M4A (AAC) audio. On ShoutCast servers, you may need to add ;stream/1 to the URL to access the audio live-stream. For example, <mark>http://domain.com/;stream/1</mark>',
				'id'			=> $shortname.'_radiostream_id',
				'class'			=> 'audio_player radio',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '400',
	);
	$iva_options[] = array(
				'name'			=> 'Radio Player',
				'desc'			=> 'Check this if you wish to enable the Radio Player in the footer.',
				'id'			=> $shortname.'_radio_enable',
				'class'			=> 'audio_player radio',
				'std'			=> '',
				'type'			=> 'checkbox',
	);

	$iva_options[] = array(
				'name'			=> 'Radio AutoPlay',
				'desc'			=> 'Check this if you wish to play the radio automatically after the page is loaded.',
				'id'			=> $shortname.'_radio_autoplay',
				'class'			=> 'audio_player radio',
				'std'			=> '',
				'type'			=> 'checkbox'
	);
	$iva_options[] = array(
				'name'			=> 'Radio Player',
				'desc'			=> 'Type the title for the Radio Player',
				'id'			=> $shortname.'_radio_title',
				'class'			=> 'audio_player radio',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
	);
	$iva_options[] = array(
				'name'			=> 'Radio Player Sub Title',
				'desc'			=> 'Type the sub title for the Radio Player which will be displayed below the title.',
				'id'			=> $shortname.'_radio_desc',
				'class'			=> 'audio_player radio',
				'std'			=> '',
				'type'			=> 'text',
				'inputsize'		=> '',
	);

	$iva_options[] = array(	'name'	=> 'Player Background',
						'desc'	=> 'Background color for the Sticky Player',
						'id'	=> $shortname.'_playerBG',
						'std'	=> '', 
						'type'	=> 'color');

	$iva_options[] = array(	'name'	=> 'Player Border',
						'desc'	=> 'Border color for the Sticky Player',
						'id'	=> $shortname.'_playerBr',
						'std'	=> '', 
						'type'	=> 'color');

	$iva_options[] = array(	'name'	=> 'Player Buttons',
						'desc'	=> 'Buttons color for the Sticky Player',
						'id'	=> $shortname.'_playerBtnBG',
						'std'	=> '', 
						'type'	=> 'color');

	$iva_options[] = array(	'name'	=> 'Player Title',
						'desc'	=> 'Title color for the Sticky Player',
						'id'	=> $shortname.'_playerTitle',
						'std'	=> '', 
						'type'	=> 'color');
	
	$iva_options[] = array(	'name'	=> 'Player Volume Bar',
						'desc'	=> 'Volume Bar color for the Sticky Player',
						'id'	=> $shortname.'_playerVolBG',
						'std'	=> '', 
						'type'	=> 'color');

	$iva_options[] = array(	'name'	=> 'Player Meta Colors',
						'desc'	=> 'Player Meta Data, Switcher x mark, Player Timing text, color for the Sticky Player',
						'id'	=> $shortname.'_playerMeta',
						'std'	=> '', 
						'type'	=> 'color');

	
	//Shareing Options
	//------------------------------------------------------------------------
	$iva_options[] = array( 'name' => 'Sharing', 'icon' => $url.'link-icon.png', 'type' => 'heading' );
	//------------------------------------------------------------------------------------------------
	$iva_options[] = array(
				'name'			=> 'Google+',
				'desc'			=> 'Check this to enable Google+ Icon for Post Sharing',
				'id'			=> $shortname.'_google_enable',
				'std'			=> '',
				'type'			=> 'checkbox',
				
	);
	$iva_options[] = array(
				'name'			=> 'LinkedIn',
				'desc'			=> 'Check this to enable LinkedIn Icon for Post Sharing',
				'id'			=> $shortname.'_linkedIn_enable',
				'std'			=> '',
				'type'			=> 'checkbox',
				
	);
	$iva_options[] = array(
				'name'			=> 'Digg',
				'desc'			=> 'Check this to enable Digg Icon for Post Sharing',
				'id'			=> $shortname.'_digg_enable',
				'std'			=> '',
				'type'			=> 'checkbox',
				
	);
	$iva_options[] = array(
				'name'			=> 'StumbleUpon',
				'desc'			=> 'Check this to enable StumbleUpon Icon for Post Sharing',
				'id'			=> $shortname.'_stumbleupon_enable',
				'std'			=> '',
				'type'			=> 'checkbox',
				
	);
	$iva_options[] = array(
				'name'			=> 'Pinterest',
				'desc'			=> 'Check this to enable Pinterest Icon for Post Sharing',
				'id'			=> $shortname.'_pinterest_enable',
				'class'			=> 'pinterest_sharing',
				'std'			=> '',
				'type'			=> 'checkbox',
				
	);
	$iva_options[] = array(
				'name'			=> 'Twitter',
				'desc'			=> 'Check this to enable Twitter Icon for Post Sharing',
				'id'			=> $shortname.'_twitter_enable',
				'std'			=> '',
				'type'			=> 'checkbox',
				
	);
	$iva_options[] = array(
				'name'			=> 'Facebook',
				'desc'			=> 'Check this to enable Facebook Icon for Post Sharing',
				'id'			=> $shortname.'_facebook_enable',
				'std'			=> '',
				'type'			=> 'checkbox',
				
	);


	return $iva_options;
}
