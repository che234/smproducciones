<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php

	global $wpdb,$iva_custom_search_select,$keyword,$default_date;
			
	$track_posts_perpage 		=  get_option('atp_search_track_limits') ? get_option('atp_search_track_limits'):'4';
	$search_pagination_option 	=  get_option('atp_search_pagination') ? get_option('atp_search_pagination') :''; 
	
	$iva_albunm_postids = $attachment_posts = $album_ids = $all_ids = array();
	$tracks_pagination = $dj_tracks_pagination = '';
	$error = true; 
	$keyword = $_GET['iva_search_input'] ? $_GET['iva_search_input'] :'';
	$lyrics_option	= get_option( 'atp_lyrics_option' ) ?  get_option( 'atp_lyrics_option' ) : 'lightbox' ;
	$atp_song_like_enable       = get_option( 'atp_song_like_btn' ) ? get_option( 'atp_song_like_btn' ) : 'true' ;
	$atp_song_count_enable      = get_option( 'atp_song_count_btn' ) ? get_option( 'atp_song_count_btn' ) : 'true' ;
			
			if ( $lyrics_option === 'lightbox' ) {
				$lyrics_box = 'prettyPhoto[inline]';
			}else{
				$lyrics_box = 'lyrics';
			}				
	if( $keyword ) {
		$results = '';
		
		// Pagination
		if ( get_query_var('paged') ) {
			$paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
			$paged = get_query_var('page');
		} else {
			$paged = 1;  
		}
		
		if( $iva_custom_search_select == 'all'){				
			// 
			$sql = "SELECT * FROM $wpdb->posts WHERE post_title LIKE '%$keyword%'
					OR post_content LIKE '%$keyword%'
					AND ( post_type='attachement' OR post_mime_type='audio/mpeg') 
					AND ( post_status = 'inherit' OR post_status = 'publish')";
						
			$results = $wpdb->get_results( $sql );
			
					
			if( count( $results ) > 0  ) {
			
				$iva_track_ids 	 = $attachment_posts = $all_ids = array();
				
				foreach( $results as $result ) {
					$track_ids[] = $result->ID;
				}
					
				// Track ids from media
				$track_ids = array_unique( $track_ids );
				
				// Fetching track ids from posttype 'albums'
				$track_args = array( 'post_type'=> array( 'djmix','albums'),'showposts'=>'-1' );
		
				$iva_track_query = new WP_Query( $track_args );
				
				if ( $iva_track_query->have_posts() ) : 
					while ( $iva_track_query->have_posts() ) : $iva_track_query->the_post();
					
					$iva_post_type = get_post_type();

					$audio_posttype_option	= get_post_meta( $post->ID,'audio_posttype_option',true );
					

					if( $audio_posttype_option == 'player' ){
						$audiolist  	  = get_post_meta( $post->ID, 'audio_upload', true ) ? get_post_meta( $post->ID, 'audio_upload', true ) : '';
						$djmix_upload_id  = get_post_meta( $post->ID, 'djmix_upload_mix', false );
						
						
						// Album
						$audio_list = explode( ',', $audiolist );
						foreach( $track_ids as $trackid ){
							if( in_array( $trackid, $audio_list ) ) {
								$iva_track_ids[] = $trackid;
							}
						}
						
						// Djmix
						if($djmix_upload_id!=''){
							foreach( $track_ids as $trackid ){
								if( in_array( $trackid, $djmix_upload_id ) ) {
									$iva_track_ids[] = $trackid;
								}
							}
						}
					}
					
					if( $audio_posttype_option === 'medialibrary' ){
						$audiolist  = get_post_meta( $post->ID, 'audio_medialibrary', false ) ? get_post_meta( $post->ID, 'audio_medialibrary', false ) : '';
						if(is_array($audiolist)){
							foreach( $track_ids as $trackid ){
								if( in_array( $trackid, $audiolist ) ) {
									$iva_track_ids[] = $trackid;
								}
							}
						}
					}
			
					endwhile; 
		
					wp_reset_query();
					else :
				endif;
					
				// Track ids from posttype 'albums'
				$iva_track_ids = array_unique( $iva_track_ids );
						
						
				foreach( $iva_track_ids as $attachment_id ) {
					$attachment 			= get_post( $attachment_id );
					$attachment_title 		= $attachment->post_title;
					$attachment_posts[] 	= $attachment->post_parent;
				}
							
				// Fetching Post ids those having attachements
				$attachment_posts = array_unique( $attachment_posts );
			
			}
			global $track_ids;
			
		
			// Album Meta Query
			$alltracks_meta_args =  array(
				'post_type' => array( 'albums','djmix'),
				'posts_per_page' => '-1',
				'meta_query'=> array(
				'relation'  => 'OR',
					array(
						'key'     => 'audio_soundcloud_title',
						'value'   => $keyword,
						'compare' => 'LIKE'
					),
					array(
						'key'     => 'audio_mp3',
						'value'   => $keyword,
						'compare' => 'LIKE'
					),
					array(
						'key'     => 'audio_medialibrary',
						'value'   => $track_ids,
						'compare' => 'IN'
					),
					array(
						'key'     => 'djmix_externalmp3title',
						'value'   => $keyword,
						'compare' => 'LIKE'
					),
				),
			);

			$tracks_output = '';
			$alltracks_meta_query = new WP_Query( $alltracks_meta_args );
							
			if ( $alltracks_meta_query->have_posts() ) : 
			
				$soundcloudcount = $externalmp3count = $mediauploadmp3count = 0; 
				
				while ( $alltracks_meta_query->have_posts() ) : $alltracks_meta_query->the_post();

				$audio_posttype_option			= get_post_meta( $post->ID,'audio_posttype_option',true );
				$audio_soundcloud_title 		= get_post_meta( $post->ID,'audio_soundcloud_title',true);
				$audio_soundcloud_title_label 	= $audio_soundcloud_title ? $audio_soundcloud_title :__('Title Label','musicplay');
				$audio_soundcloud_url 			= get_post_meta( $post->ID,'audio_soundcloud_url',true);
				$audio_mp3list					= get_post_meta( $post->ID,'audio_mp3',true ) ? get_post_meta( $post->ID,'audio_mp3',true ) :'';
				$audio_mp3list_count			= count( $audio_mp3list );
					
				// Djmix
				$djmix_externalmp3title	= get_post_meta( $post->ID,'djmix_externalmp3title',true ) ? get_post_meta( $post->ID,'djmix_externalmp3title',true ) :'';
					
				// Get posttype	
				$iva_posttype_select =	$post->post_type;	
					
				// Fetching album posts having soundcloud urls and external mp3 urls
				
							
				if ( $audio_posttype_option  == 'soundcloud' ) {
					if( $audio_soundcloud_url != '' ){
						if ( preg_match("/$keyword/i", $audio_soundcloud_title )) {
						
							$externalmp3count++;
							$all_ids[] = $post->ID;
						}
					}
				}
				if ( $audio_posttype_option  == 'externalmp3' ) {
						
					// Album
					if( $iva_posttype_select == 'albums'){
						if( $audio_mp3list !='' && $audio_mp3list_count > 0) {
							foreach( $audio_mp3list as $mp3_list ) {
								if ( preg_match("/$keyword/i", $mp3_list['mp3title'])) {
									$externalmp3count++;
									$all_ids[] = $post->ID;
								}
							}
						}
					}
					
					// Djmix
					if( $iva_posttype_select == 'djmix'){
						if( $djmix_externalmp3title	!=''){
							if ( preg_match("/$keyword/i", $djmix_externalmp3title)) {
								$externalmp3count++;
								$all_ids[] = $post->ID;
							}
						}
					}
				}
				
				// echo $audio_posttype_option;
				if ( $audio_posttype_option  == 'medialibrary' ) {
				
					$audio_mediaupload = get_post_meta( $post->ID,'audio_medialibrary',false ) ? get_post_meta( $post->ID,'audio_medialibrary',false ) :'';
					$audio_mediaupload_count = count( $audio_mediaupload );
					
					if( $audio_mediaupload !='' && $audio_mediaupload_count > 0) {
						foreach( $audio_mediaupload as $mediaupload_list ) {
							$attachment = get_post( $mediaupload_list );
							if( isset( $attachment ) ){
								if ( preg_match("/$keyword/i", $attachment->post_title)) {
									$externalmp3count++;
									$all_ids[] = $post->ID;
								}
							}
						}
					}
				}

				endwhile; 

				wp_reset_query();
				else :
			endif;
			
			$all_ids = array_unique( $all_ids );
			$iva_all_postids = array_merge( $attachment_posts,$all_ids );
			$iva_all_postids = array_unique( $iva_all_postids );
			
			global $externalmp3count,$attachment_posts,$soundcloudcount;
			
			$total_postscount = count( $iva_all_postids );
			
							
			if( $iva_all_postids ) {
			
				// Query to fetch all posts that are having attachements
				$aldj_meta_args =  array(  
					'post_type' => array( 'albums','djmix'),
					'posts_per_page' => '-1',
					'post__in'  => $iva_all_postids,
				);
				 
				$aldj_query = new WP_Query( $aldj_meta_args );
				
				// var_dump($aldj_query);
				
			
				if ( $aldj_query->have_posts() ) : 
				
					$error = false;
				
					
					$pageCount = ceil( $total_postscount / $track_posts_perpage );
					
					$current_page 	= isset( $_GET['page'] ) ? intval( $_GET['page'] ) :'';
					
					if ( get_query_var('paged') ) {
						$current_page = get_query_var('paged');
					} elseif ( get_query_var('page') ) {
						$current_page = get_query_var('page');
					} else {
						$current_page = 1;  
					}
					
					if ( empty( $current_page ) || $current_page<=0 ) $current_page = 1;
						
						$max_pagenum = $current_page * $track_posts_perpage;
						$min_pagenum = ( $current_page-1 ) * $track_posts_perpage;
					
					if( $pageCount > 1 ){
						$pagenum_link 	= html_entity_decode( get_pagenum_link() );
						$page_link 		= $pagenum_link;
						
						$page_link_perma = true;
						
						if ( strpos( $page_link, '?' )!== false )
						
						$page_link_perma = false;
						
						$tracks_pagination = '<div class="clear"></div><div class="pagination">';
						for ( $j=1; $j<= $pageCount; $j++){
							if ( $j == $current_page )
								$tracks_pagination .= '<span class="current"> '.$j.' </span>';
							else
								$tracks_pagination .= '<a class="inactive" href="'.$page_link. ( ( $page_link_perma?'?':'&amp;') ). 'page='.$j.'">'.$j.'</a>';
						}
						$tracks_pagination .= '</div>';
					}// Tracks pagination 
						
					$i = $k = 0;
					
					$start = 1;
			
					while ( $aldj_query->have_posts() ) : $aldj_query->the_post();
					
					
						// Djmix and album Post meta						
						$track_post_title 				= get_the_title( $post->ID );
						$audio_soundcloud_title 		= get_post_meta( $post->ID,'audio_soundcloud_title',true);
						$audio_soundcloud_title_label 	= $audio_soundcloud_title ? $audio_soundcloud_title :__('Title Label','musicplay');
						$audio_soundcloud_url 			= get_post_meta( $post->ID,'audio_soundcloud_url',true);
						$audio_artist_name 				= get_post_meta( $post->ID,'audio_artist_name',true);
						$audio_mp3list					= get_post_meta( $post->ID,'audio_mp3',true ) ? get_post_meta( $post->ID,'audio_mp3',true ) :'';
						$audio_mp3list_count			= count( $audio_mp3list );
						$audio_posttype_option			= get_post_meta( $post->ID,'audio_posttype_option',true );
						$djmix_genre		 			= get_post_meta( $post->ID, 'djmix_genre', true );	
						$djmix_post_date 	 			= get_post_meta( $post->ID,'djmix_release_date',true);		
						$djmix_mp3_title 	 			= get_post_meta( $post->ID,'djmix_externalmp3title',true);		
						$djmix_mp3_url	 	 			= get_post_meta( $post->ID,'djmix_externalmp3',true);	
						$djmix_upload_id  	 			= get_post_meta( $post->ID, 'djmix_upload_mix', false );
						$imagesrc                		= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
						$music_image 					= aq_resize( $imagesrc[0], '80', '80', true );
						
						if( $djmix_post_date !='') { 
							if( is_numeric( $djmix_post_date ) ){
								 $djmix_post_date = date_i18n( $default_date,$djmix_post_date );
							}	
						}
						
						$attachment_url = '';
						
						// Get Post type		
						$iva_posttype_select =	$post->post_type;
								
						// Artist names
						if ( is_array( $audio_artist_name ) && count( $audio_artist_name ) > 0 ) {
							$audio_artist = array();
							foreach( $audio_artist_name as $audio_artist_id ){
								 // if( $audio_artist_id !=''){
									$permalink      = get_permalink( $audio_artist_id );
									$cs_artist_name = get_the_title( $audio_artist_id );
									$audio_artist[] = '<a href="'.$permalink.'">'.$cs_artist_name.'</a>';
								 // }
							}
							$audio_artist_name = implode( ', ', $audio_artist );
						}
						
						// Player list
						if ( $audio_posttype_option  == 'player' && in_array( $post->ID,$attachment_posts) ) {
						
							$player_unique_id = $start.'player'.rand( 1,10 );
							
							$audio_uploadlist 	= get_post_meta( $post->ID,'audio_upload',true ) ? get_post_meta( $post->ID,'audio_upload',true ) :'';
							$album_audiolist	= explode( ',',$audio_uploadlist );
							
							$imagesrc 			= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
							$music_image 		= aq_resize( $imagesrc[0], '80', '80', true );
							$post_date 			= get_post_meta($post->ID,'audio_release_date',true);
							
							if( $post_date !='') { 
								if(is_numeric($post_date)){
									 $post_date= date_i18n($default_date,$post_date);
								}	
							}
							
							global $track_ids;
								
							
							// Album 
							if( $iva_posttype_select == 'albums'){
								foreach( $track_ids as $trackid ){
									if( in_array( $trackid,$album_audiolist ) ){
										if ( $k >= $min_pagenum && $k < $max_pagenum ) {
							
											$iva_track_ids 		= $trackid;
											$attachment    		= get_post( $iva_track_ids );
											$attachment_title 	= $attachment->post_title;
											$attachment_url 	= $attachment->guid;
											$trackcount = $download_player_count = 0;
											if(  isset( $attachment->trackcount ) ){
												$trackcount = $attachment->trackcount;
											}
											$lyricsdesc = $attachment->post_content;
											$buy		= $attachment->buy;
											$download	= $attachment->download;
											$download_player_count = get_post_meta( $attachment->ID,'download_count',true);
											
											$tracks_output.= '<div class="tracklist-Row">';
											$tracks_output.= '<div class="tracklist-details">';
											$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$attachment->ID.'" data-meta="#single-player-meta'.$player_unique_id.'" href="'.$attachment_url.'"  title="'.$attachment_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a>'.$attachment_title.'<br>';
											// song count enable/disable since 3.2.5
											if( $atp_song_count_enable != 'on') {
												$tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachment->ID.'">'.$trackcount.'</span></span>';
											}
											// song like enable/disable since 3.2.5
											if( $atp_song_like_enable != 'on' ){
												$tracks_output.= iva_track_like($attachment->ID);
											}
											if($buy !='') { 
												$tracks_output .='<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
											}

											if($download != '' && $download != '#' ) { 
												$tracks_output .='<span class="mp3o download music_download" data-id="'.$attachment->ID.'" data-download="'. iva_get_download($download).'"><i class="fa fa-download fa-lg fa-fw"></i><span class="iva_dcount">'.(int)$download_player_count.'</span></span>';
											}
											if($lyricsdesc !=''){ 
												$tracks_output .='<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $attachment_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
											}
											if($lyricsdesc !=''){ ?>
												<div id="<?php echo preg_replace('/\s+/', '-', $attachment_title) ; ?>" class="lyricdesc">
													<?php 
													echo '<p><strong>'.$attachment_title.'</strong></p>';
													//echo wpautop($attachment->post_content); 
													$content = apply_filters('the_content', $attachment->post_content); 
													echo $content;
													?>
												</div>
												<?php } 
											$tracks_output.= '</div>';
											$tracks_output.= '<div class="tracklist-album tracklist-col">';
											$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
											$tracks_output.= '<strong>'.$track_post_title.'</strong><p>'.$audio_artist_name.'</p></div>';
											
											$tracks_output.= '</div>';//.tracklist-details
											$tracks_output.= '</div>';//.tracklist-Row
											
											$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$player_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a></div>'; 
															
										}
										$k++;
									}
								}
							}
								
							//Djmix
							if( $iva_posttype_select == 'djmix'){
								foreach( $track_ids as $trackid ){
									if( in_array( $trackid,$djmix_upload_id ) ){
									if ( $k >= $min_pagenum && $k < $max_pagenum ) {
						
										$iva_track_ids 		= $trackid;
										$attachment    		= get_post( $iva_track_ids );
										$attachment_title 	= $attachment->post_title;
										$attachment_url 	= $attachment->guid;
										$dj_trackcount = 0;
										if(  isset( $attachment->trackcount ) ){
											$dj_trackcount = $attachment->trackcount;
										}
										
										$tracks_output.= '<div class="tracklist-Row">';
										$tracks_output.= '<div class="tracklist-details">';
										// Djmix 
										$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$attachment->ID.'" data-meta="#single-player-meta'.$player_unique_id.'" href="'.$attachment_url.'"  title="'.$attachment_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a>'.$attachment_title.'<br>';
										$tracks_output.= '<span class="iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'.$attachment->ID.'">'.$dj_trackcount.'</span></span>';
											$tracks_output .='</div>';
										$tracks_output.= '<div class="tracklist-album tracklist-col">';
										$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
										$tracks_output.= '<strong>'.$track_post_title.'</strong></div>';
										
										$tracks_output.= '</div>';//.tracklist-details
										$tracks_output.= '</div>';//.tracklist-Row
										
										$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$player_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a></div>';
					
									}
									$k++;
									}
								}
							}		
							
							$error = false;
						}
					
						// Soundcloud list
						if ( $audio_posttype_option  == 'soundcloud' ) {
							
							if( $audio_soundcloud_url !=''){
								
								$imagesrc 		= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
								$music_image 	= aq_resize( $imagesrc[0], '80', '80', true );
								
								// Album
								$post_date 		= get_post_meta($post->ID,'audio_release_date',true);
								if( $post_date !='') { 
									if(is_numeric($post_date)){
										 $post_date= date_i18n( $default_date,$post_date );
									}	
								}
								$audio_soundcloud_id 	= $post->ID;
								$trackcount 			= get_post_meta( $post->ID,'trackcount',true );
								$soundtrackcount 		= isset( $trackcount )  ?  $trackcount :'0';
							
								if ( preg_match("/$keyword/i", $audio_soundcloud_title )) {
									
									$soundcloud_unique_id = $start.'sc'.rand( 100,200 );
									
									if ( $k >= $min_pagenum && $k < $max_pagenum ) {
									
										// Album
										if( $iva_posttype_select == 'albums'){
											$tracks_output.= '<div class="tracklist-Row">';
											$tracks_output.= '<div class="tracklist-details">';
											
											$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$audio_soundcloud_id.'" data-meta="#single-player-meta'.$soundcloud_unique_id.'"  href="'.$audio_soundcloud_url.'"  title="'.$audio_soundcloud_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a>'.$audio_soundcloud_title.'<br>';
											$tracks_output.= '<span class="iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'.$audio_soundcloud_id.'">'.$soundtrackcount.'</span></span>';
											$tracks_output .='</div>';
											$tracks_output.= '<div class="tracklist-album tracklist-col">';
											$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
											$tracks_output.= '<strong>'.$track_post_title.'</strong><p>'.$audio_artist_name.'</p></div>';											
											$tracks_output.= '</div>';//.tracklist-details
											$tracks_output.= '</div>';//.tracklist-Row
											$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$soundcloud_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a></div>'; 
										}
										
										// Djmix
										if( $iva_posttype_select == 'djmix'){
											$tracks_output.= '<div class="tracklist-Row">';
											$tracks_output.= '<div class="tracklist-details">';
											$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$audio_soundcloud_id.'" data-meta="#single-player-meta'.$soundcloud_unique_id.'"  href="'.$audio_soundcloud_url.'"  title=""  rel="'.$music_image.'"><i class="ivaplay"></i> '.$audio_soundcloud_title.'</a><span class="iva_playCount ivaplay-'.$audio_soundcloud_id.'">'.$soundtrackcount.'</span></div>';
											$tracks_output.= '<div class="tracklist-album tracklist-col">';
											$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
											$tracks_output.= '<strong>'.$track_post_title.'</strong></div>';											
											$tracks_output.= '</div>';//.tracklist-details
											$tracks_output.= '</div>';//.tracklist-Row
											
											$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$soundcloud_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a></div>'; 
										}
									 }
									$k++;
									$error = false;
								}
						
							}
						}
						// External mp3 list
						if ( $audio_posttype_option  == 'externalmp3' ) {
							
							// Albums
							if( $iva_posttype_select == 'albums'){
								if( $audio_mp3list !='' && $audio_mp3list_count > 0) {
								
									$imagesrc 		= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
									$music_image 	= aq_resize( $imagesrc[0], '80', '80', true );
									$post_date 		= get_post_meta( $post->ID,'audio_release_date',true );
									
									if( $post_date !='') { 
										if(is_numeric($post_date)){
											 $post_date= date_i18n($default_date,$post_date);
										}	
									}
									foreach( $audio_mp3list as $mp3_list ) {
										if ( preg_match("/$keyword/i", $mp3_list['mp3title'])) {
											
											$mp3_unique_id = $start.'mp3'.rand( 200,300 );
										
											if ( $k >= $min_pagenum && $k < $max_pagenum ) {
											
																								
												$mp3_title	= $mp3_list['mp3title'];
												$mp3_url	= $mp3_list['mp3url'];
												$mp3id 		= $mp3_list['mp3id'];
												$lyricsdesc	= $mp3_list['lyrics'];
												$buy		= $mp3_list['buylink'];
												$download	= $mp3_list['download'];

												$trackcount = get_post_meta( $mp3id,'trackcount',true ) ? get_post_meta( $mp3id,'trackcount',true ) : '0';	
							
												$tracks_output.= '<div class="tracklist-Row">';
												$tracks_output.= '<div class="tracklist-details">';
												$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy"  data-id="'.$mp3id.'" data-meta="#single-player-meta'.$mp3_unique_id.'" href="'.esc_attr( $mp3_url ).'"  title="'.$mp3_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i> '.$mp3_title.'<br>';
												// song count enable/disable since 3.2.5
												if( $atp_song_count_enable != 'on') {
													$tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $mp3id.'">'.$trackcount.'</span></span>';
												}
												// song like enable/disable since 3.2.5
												if( $atp_song_like_enable != 'on' ){
													$tracks_output.= iva_track_like($mp3id);
												}
												if($buy !='') { 
													$tracks_output .='<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
												}
												if($download != '' && $download != '#' ) { 
													$download_mp3_count = get_post_meta( $mp3id,'download_count',true)?get_post_meta( $mp3id,'download_count',true):'0';
													$tracks_output .='<span class="mp3o download music_download" data-id="'.$mp3id.'" data-download="'. iva_get_download($download).'"><i class="fa fa-download fa-lg fa-fw"></i><span class="iva_dcount">'.(int)$download_mp3_count.'</span></span>';
												}
												if($lyricsdesc !=''){ 
													$tracks_output .='<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $mp3_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
												}
												if($lyricsdesc !=''){ ?>
													<div id="<?php echo preg_replace('/\s+/', '-', $mp3_title) ; ?>" class="lyricdesc">
													<?php 
													echo '<p><strong>'.$mp3_title.'</strong></p>';
													$content = $lyricsdesc; 
													echo $content;
													?>
													</div>
												<?php } 
												$tracks_output.= '</div>';
												$tracks_output.= '<div class="tracklist-album tracklist-col">';
												$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
												$tracks_output.= '<strong>'.$track_post_title.'</strong><p>'.$audio_artist_name.'</p></div>';
												$tracks_output.= '</div>';//.tracklist-details
												
												$tracks_output.= '</div>';//.tracklist-Row
												
												$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$mp3_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a></div>'; 
								
											}
											$k++;
											$error = false;
										}
									}
								}
							}
							
							// Djmix
							if( $iva_posttype_select == 'djmix'){
							
								if(  $djmix_mp3_title!= ''){
								
									$imagesrc 				= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
									$music_image 			= aq_resize( $imagesrc[0], '80', '80', true );
									$mp3id 					= $post->ID;
									$trackcount 			= get_post_meta( $mp3id,'trackcount',true) ? get_post_meta( $mp3id,'trackcount',true ) : '0';	
							
								
									if ( preg_match("/$keyword/i", $djmix_mp3_title) ) {
										
										$mp3_unique_id = $start.'mp3'.rand( 200,300 );
									
										if ( $k >= $min_pagenum && $k < $max_pagenum ) {
															
											$tracks_output.= '<div class="tracklist-Row">';
											$tracks_output.= '<div class="tracklist-details">';
											$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$mp3id.'"  data-meta="#single-player-meta'.$mp3_unique_id.'" href="'.esc_attr( $djmix_mp3_url ).'"  title="'.$djmix_mp3_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a> '.$djmix_mp3_title.'<br>';
											$tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'.$mp3id.'">'.$trackcount.'</span></span></div>';
											$tracks_output.= '<div class="tracklist-album tracklist-col">';
											$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
											$tracks_output.= '<strong>'.$track_post_title.'</strong></div>';
											$tracks_output.= '</div>';//.tracklist-details
											
											$tracks_output.= '</div>';//.tracklist-Row
											
											$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$mp3_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a>
											<div>'.$djmix_genre.' | '.$djmix_post_date.'</div></div>'; 
							
										}
										$k++;
										$error = false;
									}
								}
							}
						}

					
						if ( $audio_posttype_option  == 'medialibrary') {
							
							// echo 'mediatracks';
							$player_unique_id = $start.'medialibrary'.rand( 1,10 );
							$audio_uploadlist 	= get_post_meta( $post->ID,'audio_medialibrary',false ) ? get_post_meta( $post->ID,'audio_medialibrary',false ) :'';

							$imagesrc 			= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
							$music_image 		= aq_resize( $imagesrc[0], '80', '80', true );
							$post_date 			= get_post_meta( $post->ID,'audio_release_date',true );
							$lyricsdesc			= $attachment->post_content;
							$buy				= $attachment->buy;
							$download			= $attachment->download;

							
							if( $post_date !='') { 
								if(is_numeric($post_date)){
									 $post_date= date_i18n($default_date,$post_date);
								}	
							}
				
							global $track_ids;
							foreach( $track_ids as $trackid ){
								if( in_array( $trackid,$audio_uploadlist ) ){
									if ( $k >= $min_pagenum && $k < $max_pagenum ) {
										$iva_track_ids 		= $trackid;
										$attachment    		= get_post( $iva_track_ids );
										$attachment_title 	= $attachment->post_title;
										$attachment_url 	= $attachment->guid;
										$lyricsdesc			= $attachment->post_content;
										$buy				= $attachment->buy;
										$download			= $attachment->download;
										$trackcount = 0;
										if(  isset( $attachment->trackcount ) ){
											$trackcount = $attachment->trackcount;
										}
										$tracks_output.= '<div class="tracklist-Row">';
										$tracks_output.= '<div class="tracklist-details">';
										
										$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy"  data-id="'.$attachment->ID.'"  data-meta="#single-player-meta'.$player_unique_id.'" href="'.$attachment_url.'"  title="'.$attachment_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a> '.$attachment_title.'<br>';
										// song count enable/disable since 3.2.5
										if( $atp_song_count_enable != 'on') {
											$tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachment->ID.'">'.$trackcount.'</span></span>';
										}
										// song like enable/disable since 3.2.5
										if( $atp_song_like_enable != 'on' ){
											$tracks_output.= iva_track_like($audio_soundcloud_id);
										}
										if($buy !='') { 
											$tracks_output .='<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
										}

										if($download != '' && $download != '#' ) { 
											$download_medialibrary_count = get_post_meta( $attachment->ID,'download_count',true)?get_post_meta( $mp3id,'download_count',true):'0';
											$tracks_output .='<span class="mp3o download music_download" data-id="'.$attachment->ID.'" data-download="'. iva_get_download($download).'"><i class="fa fa-download fa-lg fa-fw"></i><span class="iva_dcount">'.(int)$download_medialibrary_count.'</span></span>';
										}
										if($lyricsdesc !=''){ 
											$tracks_output .='<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $attachment_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
										}
										if($lyricsdesc !=''){ ?>
														<div id="<?php echo preg_replace('/\s+/', '-', $attachment_title) ; ?>" class="lyricdesc">
															<?php 
															echo '<p><strong>'.$attachment_title.'</strong></p>';
															//echo wpautop($attachment->post_content); 
															$content = apply_filters('the_content', $attachment->post_content); 
															echo $content;
															?>
														</div>
														<?php } 
									$tracks_output.= '</div>';

										$tracks_output.= '<div class="tracklist-album tracklist-col">';
										$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
										$tracks_output.= '<strong>'.$track_post_title.'</strong><p>'.$audio_artist_name.'</p></div>';
										$tracks_output.= '</div>';//.tracklist-details
										$tracks_output.= '</div>';//.tracklist-Row
										
										$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$player_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a></div>'; 
									}
									$k++;
								}
							}
							$error = false;
						}
						$start++;
					endwhile; 
			
					
					// Track Search Results Output
					if( $tracks_output ){
						echo '<div class="tracks-search inner">';
						echo '<h2>'.__('Tracks','musicplay').'</h2>';
						echo '<div class="tracklist-Table">';
						echo '<div class="tracklist-Header">';
						echo '<div class="tracklist-Row">';
						echo '<div class="tracklist-name tracklist-col">'.__('Tracks','musicplay').'</div>';
						echo '<div class="tracklist-album tracklist-col">'.__('Album','musicplay').'</div>';
						echo '</div>';// .tracklist-Row
						echo '</div>';//.tracklist-Header
						echo '<div class="tracklist-Body">';
						echo $tracks_output;
						
						// Tracks Pagination
						if( $search_pagination_option == 'on'){
							echo $tracks_pagination;
						}
						
						echo '</div>';//.tracklist-Body
						echo '</div>';//.tracklist-Table
						echo '</div>';//.tracklist-search
					}
					wp_reset_query();
				else :
				endif;
				echo '<div class="clear"></div>';
			}
			else{
				// $error = true;
			}
		}
		// echo $error;
		if( $iva_custom_search_select == 'albums'){
			get_template_part('musicband/album','results');
		}
		if( $iva_custom_search_select == 'djmix'){
			get_template_part('musicband/djmix','results');
		}
		if( $iva_custom_search_select == 'artists'){
			get_template_part('musicband/artists','results');
		}
		if( $error ){
			echo '<div class="inner"><div class="empty-search"><p>'.__('No Results Found','musicplay').'</p></div></div>';
		}
	}
	?>
	</div><!-- #post-<?php the_ID(); ?> -->
<?php get_footer(); ?>