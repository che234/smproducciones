<?php
	global $error,$keyword,$default_date;
	$track_posts_perpage 		= get_option('atp_search_track_limits') ? get_option('atp_search_track_limits'):'2';
	$search_pagination_option 	=  get_option('atp_search_pagination') ? get_option('atp_search_pagination') :'';	
	$al_posts_perpage 			= get_option('atp_search_album_limits') ? get_option('atp_search_album_limits') :'-1';
	$atp_song_like_enable       = get_option( 'atp_song_like_btn' ) ? get_option( 'atp_song_like_btn' ) : 'true' ;
	$atp_song_count_enable      = get_option( 'atp_song_count_btn' ) ? get_option( 'atp_song_count_btn' ) : 'true' ;

	$attachment_posts = $track_ids = $album_ids = array();
	$results = $tracks_pagination= '';
	
	// Pagination
	if ( get_query_var('paged') ) {
		$paged = get_query_var('paged');
	} elseif ( get_query_var('page') ) {
		$paged = get_query_var('page');
	} else {
		$paged = 1;  
	}
	$lyrics_option	= get_option( 'atp_lyrics_option' ) ?  get_option( 'atp_lyrics_option' ) : 'lightbox' ;
			
	if ( $lyrics_option === 'lightbox' ) {
		$lyrics_box = 'prettyPhoto[inline]';
	}else{
		$lyrics_box = 'lyrics';
	}				
	// 
	$al_sql = "SELECT * FROM $wpdb->posts WHERE post_title LIKE '%$keyword%'
			OR post_content LIKE '%$keyword%'
			AND ( post_type='attachement' OR post_mime_type='audio/mpeg') 
			AND ( post_status = 'inherit' OR post_status = 'publish')";
				
	$al_results = $wpdb->get_results( $al_sql );
					
	if( count( $al_results ) > 0  ) {
		$iva_track_ids = $attachment_posts = array();
		foreach( $al_results as $result ) {
			$track_ids[] = $result->ID;
		}
					
		// Track ids from media
		$track_ids = array_unique( $track_ids );
		
	
		// Fetching track ids from posttype 'albums'
		$track_args = array( 'post_type'=> 'albums','showposts'=>'-1');

		$iva_track_query = new WP_Query( $track_args );
		
		if ( $iva_track_query->have_posts() ) : 
			while ( $iva_track_query->have_posts() ) : $iva_track_query->the_post();

			$audio_posttype_option	= get_post_meta( $post->ID,'audio_posttype_option',true );

			if( $audio_posttype_option == 'player' ){
				$audiolist  = get_post_meta( $post->ID, 'audio_upload', true ) ? get_post_meta( $post->ID, 'audio_upload', true ) : '';
				$audio_list = explode( ',', $audiolist );
				foreach( $track_ids as $trackid ){
					if( in_array( $trackid, $audio_list ) ) {
						$iva_track_ids[] = $trackid;
					}
				}
			}
			if( $audio_posttype_option == 'medialibrary' ){
				$audiolist  = get_post_meta( $post->ID, 'audio_medialibrary', false ) ? get_post_meta( $post->ID, 'audio_medialibrary', false ) : '';
				if(is_array($audiolist)){
					foreach( $track_ids as $trackid ){
						if( in_array( $trackid, $audiolist ) ) {
							$iva_track_ids[] = $trackid;
							
						}
					}
				}
			}

			endwhile; 

			wp_reset_query();
			else :
		endif;
	
		// Track ids from posttype 'albums'
		$iva_track_ids = array_unique( $iva_track_ids );
		
		foreach( $iva_track_ids as $attachment_id ) {
			$attachment 			= get_post( $attachment_id );
			$attachment_title 		= $attachment->post_title;
			$attachment_posts[] 	= $attachment->post_parent;
		}
					
		// Fetching Post ids those having attachements
		$attachment_posts = array_unique( $attachment_posts );
	}

	// Album Meta Query
	$album_meta_args =  array(
		'post_type' => 'albums',
		'posts_per_page' => '-1',
		'meta_query'=> array(
		'relation'  => 'OR',
			array(
				'key'     => 'audio_soundcloud_title',
				'value'   => $keyword,
				'compare' => 'LIKE'
			),
			array(
				'key'     => 'audio_mp3',
				'value'   => $keyword,
				'compare' => 'LIKE'
			),
			array(
				'key'     => 'audio_medialibrary',
				'value'   => $track_ids,
				'compare' => 'IN'
			),
		),
	);

	$tracks_output = '';
	$album_meta_query = new WP_Query( $album_meta_args );
				
	if ( $album_meta_query->have_posts() ) : 
	
		$soundcloudcount = $externalmp3count = $mediauploadmp3count = 0; 
		
		while ( $album_meta_query->have_posts() ) : $album_meta_query->the_post();

		$audio_posttype_option			= get_post_meta( $post->ID,'audio_posttype_option',true );
		$audio_soundcloud_title 		= get_post_meta( $post->ID,'audio_soundcloud_title',true);
		$audio_soundcloud_title_label 	= $audio_soundcloud_title ? $audio_soundcloud_title :__('Title Label','musicplay');
		$audio_soundcloud_url 			= get_post_meta( $post->ID,'audio_soundcloud_url',true);
		$audio_mp3list					= get_post_meta( $post->ID,'audio_mp3',true ) ? get_post_meta( $post->ID,'audio_mp3',true ) :'';
		$audio_mp3list_count			= count( $audio_mp3list );
			
		// Fetching album posts having soundcloud urls and external mp3 urls
		$album_ids[] = $post->ID;
					
		if ( $audio_posttype_option  == 'soundcloud' ) {
			if( $audio_soundcloud_url != '' ){
				if ( preg_match("/$keyword/i", $audio_soundcloud_title )) {
					$externalmp3count++;
					$album_ids[] = $post->ID;
				}
			}
		}
		if ( $audio_posttype_option  == 'externalmp3' ) {
			if( $audio_mp3list !='' && $audio_mp3list_count > 0) {
				foreach( $audio_mp3list as $mp3_list ) {
					if ( preg_match("/$keyword/i", $mp3_list['mp3title'])) {
						$externalmp3count++;
						$album_ids[] = $post->ID;
					}
				}
			}
		}

		if ( $audio_posttype_option  == 'medialibrary' ) {
			$audio_mediaupload = get_post_meta( $post->ID,'audio_medialibrary',false ) ? get_post_meta( $post->ID,'audio_medialibrary',false ) :'';
			$audio_mediaupload_count = count($audio_mediaupload);
			if( $audio_mediaupload !='' && $audio_mediaupload_count > 0) {
				foreach( $audio_mediaupload as $mediaupload_list ) {
					$attachment = get_post( $mediaupload_list );
					if ( preg_match("/$keyword/i", $attachment->post_title)) {
						$externalmp3count++;
						$album_ids[] = $post->ID;
					}
				}
			}
		}
		endwhile; 
		wp_reset_query();
		else :
	endif;

	$album_ids = array_unique( $album_ids );
	$iva_albunm_postids = array_merge( $attachment_posts,$album_ids );
	$iva_albunm_postids = array_unique( $iva_albunm_postids );
	
	global $externalmp3count,$soundcloudcount;
	
	
	$total_postscount = count( $iva_albunm_postids );
					
	if( $iva_albunm_postids ) {
	
		// Query to fetch all posts that are having attachements
		$album_meta_args =  array(  
								'post_type' => 'albums',
								'posts_per_page' => '-1',
								'post__in'  => $iva_albunm_postids,
							);
			 
		$album_meta_query = new WP_Query( $album_meta_args );
		
		if ( $album_meta_query->have_posts() ) : 
		
			$error = false;
			$pageCount = ceil( $total_postscount / $track_posts_perpage );
			$current_page 	= isset( $_GET['page'] ) ? intval( $_GET['page'] ) :'';
			
			if ( get_query_var('paged') ) {
				$current_page = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
				$current_page = get_query_var('page');
			} else {
				$current_page = 1;  
			}
			
			if ( empty( $current_page ) || $current_page<=0 ) $current_page = 1;
				
				$max_pagenum = $current_page * $track_posts_perpage;
				$min_pagenum = ( $current_page-1 ) * $track_posts_perpage;
			
			if( $pageCount > 1 ){
				$pagenum_link 	= html_entity_decode( get_pagenum_link() );
				$page_link 		= $pagenum_link;
				
				$page_link_perma = true;
				
				if ( strpos( $page_link, '?' )!== false )
				
				$page_link_perma = false;
				
				$tracks_pagination = '<div class="clear"></div><div class="pagination">';
				for ( $j=1; $j<= $pageCount; $j++){
					if ( $j == $current_page )
						$tracks_pagination .= '<span class="current"> '.$j.' </span>';
					else
						$tracks_pagination .= '<a class="inactive" href="'.$page_link. ( ( $page_link_perma?'?':'&amp;') ). 'page='.$j.'">'.$j.'</a>';
				}
				$tracks_pagination .= '</div>';
			}// Tracks pagination 
				
			$k = 0;
			$start = 1;
	
			while ( $album_meta_query->have_posts() ) : $album_meta_query->the_post();
										
				$album_post_title 				= get_the_title( $post->ID );
				$audio_soundcloud_title 		= get_post_meta( $post->ID,'audio_soundcloud_title',true);
				$audio_soundcloud_title_label 	= $audio_soundcloud_title ? $audio_soundcloud_title :__('Title Label','musicplay');
				$audio_soundcloud_url 			= get_post_meta( $post->ID,'audio_soundcloud_url',true);
				$audio_artist_name 				= get_post_meta( $post->ID,'audio_artist_name',true);
				$audio_mp3list					= get_post_meta( $post->ID,'audio_mp3',true ) ? get_post_meta( $post->ID,'audio_mp3',true ) :'';
				$audio_mp3list_count			= count( $audio_mp3list );
				$imagesrc                		= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
				$music_image 					= aq_resize( $imagesrc[0], '80', '80', true );
				$audio_posttype_option			= get_post_meta( $post->ID,'audio_posttype_option',true );
						
				// Artist names
				if ( is_array( $audio_artist_name ) && count( $audio_artist_name ) > 0 ) {
					$audio_artist = array();
					foreach( $audio_artist_name as $audio_artist_id){
						$permalink      = get_permalink( $audio_artist_id );
						$cs_artist_name = get_the_title( $audio_artist_id );
						$audio_artist[] = '<a href="'.$permalink.'">'.$cs_artist_name.'</a>';
					}
					$audio_artist_name = implode( ', ', $audio_artist );
				}
				
				// Player list
				if ( $audio_posttype_option  == 'player' && in_array( $post->ID,$attachment_posts) ) {
				
					$player_unique_id = $start.'player'.rand( 1,10 );
				
					
					$audio_uploadlist 	= get_post_meta( $post->ID,'audio_upload',true ) ? get_post_meta( $post->ID,'audio_upload',true ) :'';
					$album_audiolist	= explode( ',',$audio_uploadlist );
					
					$imagesrc 			= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
					$music_image 		= aq_resize( $imagesrc[0], '80', '80', true );
					$post_date 			= get_post_meta($post->ID,'audio_release_date',true);
					
					if( $post_date !='') { 
						if(is_numeric($post_date)){
							 $post_date = date_i18n($default_date,$post_date);
						}	
					}
					foreach( $track_ids as $trackid ){
						if( in_array( $trackid,$album_audiolist ) ){
							if ( $k >= $min_pagenum && $k < $max_pagenum ) {
								$trackcount =0;
								$iva_track_ids 		=  $trackid;
								$attachment    		=  get_post( $iva_track_ids );
								$attachment_title 	=  $attachment->post_title;
								$attachment_url 	=  $attachment->guid;
								$download			=  $attachment->download;
								$lyricsdesc			=  $attachment->post_content;
								$buy				=  $attachment->buy;
								if(  isset( $attachment->trackcount ) ){
									$trackcount = $attachment->trackcount;
								
								}
								
								
								$tracks_output.= '<div class="tracklist-Row">';
								$tracks_output.= '<div class="tracklist-details">';
								$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$attachment->ID.'" data-meta="#single-player-meta'.$player_unique_id.'" href="'.$attachment_url.'"  title="'.$attachment_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a>'.$attachment_title.'</br>';
								// song count enable/disable since 3.2.5
								if( $atp_song_count_enable != 'on') {
									$tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachment->ID.'">'.$trackcount.'</span></span>';
								}
								// song like enable/disable since 3.2.5
								if( $atp_song_like_enable != 'on' ){	
									$tracks_output.= iva_track_like($trackid);
								}
								if($buy !='') { 
									$tracks_output .='<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
								}

								if($download != '' && $download != '#' ) { 
									$tracks_output .='<span class="mp3o download music_download" data-download="'. iva_get_download($download).'"><i class="fa fa-cloud-download"></i></span>';
								}
								if($lyricsdesc !=''){ 
										$tracks_output .='<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $attachment_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
								}
								if($lyricsdesc !=''){ ?>
												<div id="<?php echo preg_replace('/\s+/', '-', $attachment_title) ; ?>" class="lyricdesc">
													<?php 
													echo '<p><strong>'.$attachment_title.'</strong></p>';
													//echo wpautop($attachment->post_content); 
													$content = apply_filters('the_content', $attachment->post_content); 
													echo $content;
													?>
												</div>
												<?php } 
								$tracks_output.= '</div>';
								$tracks_output.= '<div class="tracklist-album tracklist-col">';
								$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
								$tracks_output.= '<strong>'.$album_post_title.'</strong><p>'.$audio_artist_name.'</p></div>';
								$tracks_output.= '</div>';//.tracklist-details
								$tracks_output.= '</div>';//.tracklist-Row
								
								$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$player_unique_id.'">
													<a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a>
												<div>'.get_the_term_list( $post->ID, 'label', '', ', ', '' ).' | '.$post_date.'</div></div>'; 
							}
							$k++;					
						}
					}
				
					$error = false;
				}
				
				// Soundcloud list
				if ( $audio_posttype_option  == 'soundcloud' ) {
					
					if( $audio_soundcloud_url !=''){
						
						$imagesrc 		= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
						$music_image 	= aq_resize( $imagesrc[0], '80', '80', true );
						$post_date 		= get_post_meta($post->ID,'audio_release_date',true);
						$audio_soundcloud_id = get_post_meta($post->ID,'audio_soundcloud_id',true);
						$trackcount = get_post_meta($audio_soundcloud_id,'trackcount',true);
						$soundtrackcount = isset( $trackcount )  ?  $trackcount :'0';
						if( $post_date !='') { 
							if(is_numeric($post_date)){
								 $post_date= date_i18n( $default_date,$post_date );
							}	
						}
					
						if ( preg_match("/$keyword/i", $audio_soundcloud_title )) {
							
							$soundcloud_unique_id = $start.'sc'.rand( 100,200 );
							
							if ( $k >= $min_pagenum && $k < $max_pagenum ) {
							
							
								$tracks_output.= '<div class="tracklist-Row">';
								$tracks_output.= '<div class="tracklist-details">';
								$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy"  data-id="'.$audio_soundcloud_id.'" data-meta="#single-player-meta'.$soundcloud_unique_id.'"  href="'.$audio_soundcloud_url.'"  title="'.$audio_soundcloud_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a> '.$audio_soundcloud_title.'</br>';
								// song count enable/disable since 3.2.5
								if( $atp_song_count_enable != 'on') {
									$tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $audio_soundcloud_id.'">'.$soundtrackcount.'</span></span>';
								}
								// song like enable/disable since 3.2.5
								if( $atp_song_like_enable != 'on' ){	
														
									$tracks_output.= iva_track_like($audio_soundcloud_id);
								}
								$tracks_output.= '<div>';
								$tracks_output.= '<div class="tracklist-album tracklist-col">';
								$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
								$tracks_output.= '<strong>'.$album_post_title.'</strong><p>'.$audio_artist_name.'</p></div>';											
								$tracks_output.= '</div>';//.tracklist-details
								$tracks_output.= '</div>';//.tracklist-Row
								
								$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$soundcloud_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a>
								<div>'.get_the_term_list( $post->ID, 'label', '', ', ', '' ).' | '.$post_date.'</div></div>'; 
							 }
							$k++;
							$error = false;
						}
					}
				}
				// External mp3 list
				if ( $audio_posttype_option  == 'externalmp3' ) {
					
					if( $audio_mp3list !='' && $audio_mp3list_count > 0) {
					
						$imagesrc 		= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
						$music_image 	= aq_resize( $imagesrc[0], '80', '80', true );
						$post_date 		= get_post_meta( $post->ID,'audio_release_date',true );
						
						if( $post_date !='') { 
							if(is_numeric($post_date)){
								 $post_date= date_i18n($default_date,$post_date);
							}	
						}
					
						foreach( $audio_mp3list as $mp3_list ) {
							if ( preg_match("/$keyword/i", $mp3_list['mp3title'])) {
								
								$mp3_unique_id = $start.'mp3'.rand( 200,300 );
							
								if ( $k >= $min_pagenum && $k < $max_pagenum ) {
																					
									$mp3_title	= $mp3_list['mp3title'];
									$mp3_url	= $mp3_list['mp3url'];
									$mp3id  	= $mp3_list['mp3id'];
									$lyricsdesc	=$mp3_list['lyrics'];
									$buy		=$mp3_list['buylink'];
									$download	=$mp3_list['download'];
									$trackcount = get_post_meta($mp3id,'trackcount',true) ? get_post_meta($mp3id,'trackcount',true) : '0';	
									$tracks_output.= '<div class="tracklist-Row">';
									$tracks_output.= '<div class="tracklist-details">';
									$tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$mp3id.'"  data-meta="#single-player-meta'.$mp3_unique_id.'" href="'.esc_attr( $mp3_url ).'"  title="'.$mp3_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a>'.$mp3_title.'</br>';
									// song count enable/disable since 3.2.5
									if( $atp_song_count_enable != 'on') {
										$tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $mp3id.'">'.$trackcount.'</span></span>';
									}
									// song like enable/disable since 3.2.5
									if( $atp_song_like_enable != 'on' ){	
										$tracks_output.= iva_track_like($mp3id);
									}
									if($buy !='') { 
										$tracks_output .='<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
									}

								if($download != '' && $download != '#' ) { 
									$tracks_output .='<span class="mp3o download music_download" data-download="'. iva_get_download($download).'"><i class="fa fa-cloud-download"></i></span>';
									}
								if($lyricsdesc !=''){ 
										$tracks_output .='<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $mp3_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
									
									}
								if($lyricsdesc !=''){ ?>
												<div id="<?php echo preg_replace('/\s+/', '-', $mp3_title) ; ?>" class="lyricdesc">
													<?php 
													echo '<p><strong>'.$mp3_title.'</strong></p>';
													//echo wpautop($attachment->post_content); 
													$content = $lyricsdesc; 
													echo $content;
													?>
												</div>
												<?php } 
										$tracks_output.= '</div>';


									$tracks_output.= '<div class="tracklist-album tracklist-col">';
									$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
									$tracks_output.= '<strong>'.$album_post_title.'</strong><p>'.$audio_artist_name.'</p></div>';
									$tracks_output.= '</div>';//.tracklist-details
									
									$tracks_output.= '</div>';//.tracklist-Row
									
									$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$mp3_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a>
									<div>'.get_the_term_list( $post->ID, 'label', '', ', ', '' ).' | '.$post_date.'</div></div>'; 
					
								}
								$k++;
								$error = false;
							}
						}
						
					}
					
				}

				if ( $audio_posttype_option  == 'medialibrary') {
					
					$player_unique_id = $start.'medialibrary'.rand( 1,10 );
					$audio_uploadlist 	= get_post_meta( $post->ID,'audio_medialibrary',false ) ? get_post_meta( $post->ID,'audio_medialibrary',false ) :'';
					$imagesrc 			= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
					$music_image 		= aq_resize( $imagesrc[0], '80', '80', true );
					$post_date 			= get_post_meta($post->ID,'audio_release_date',true);
					
					if( $post_date !='') { 
						if(is_numeric($post_date)){
							 $post_date= date_i18n($default_date,$post_date);
						}	
					}
					
					foreach( $track_ids as $trackid ){
						if( in_array( $trackid,$audio_uploadlist ) ){
							if ( $k >= $min_pagenum && $k < $max_pagenum ) {
								$trackcount =0;
								$iva_track_ids 		= $trackid;
								$attachment    		= get_post( $iva_track_ids );
								$attachment_title 	= $attachment->post_title;
								$attachment_url 	= $attachment->guid;
								$lyricsdesc			= $attachment->post_content;
								$buy				= $attachment->buy;
								$download			= $attachment->download;
								if(  isset( $attachment->trackcount ) ){
									$trackcount = $attachment->trackcount;
								}						
								$tracks_output.= '<div class="tracklist-Row">';
								$tracks_output.= '<div class="tracklist-details">';
								
								$tracks_output.= '<div class="tracklist-name tracklist-col"><a  data-id="'.$attachment->ID.'" class="fap-single-track no-ajaxy" data-meta="#single-player-meta'.$player_unique_id.'" href="'.$attachment_url.'"  title="'.$attachment_title.'"  rel="'.$music_image.'"><i class="ivaplay"></i></a>'.$attachment_title.'<br>';
								// song count enable/disable since 3.2.5
								if( $atp_song_count_enable != 'on') {
									$tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachment->ID.'">'.$trackcount.'</span></span>';
								}
								// song like enable/disable since 3.2.5
								if( $atp_song_like_enable != 'on' ){	
									$tracks_output.= iva_track_like($audio_soundcloud_id);
								}
								if($buy !='') { 
									$tracks_output .='<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
								}

								if($download != '' && $download != '#' ) { 
									$tracks_output .='<span class="mp3o download music_download" data-download="'. iva_get_download($download).'"><i class="fa fa-cloud-download"></i></span>';
									}
								if($lyricsdesc !=''){ 
										$tracks_output .='<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $attachment_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
									
									}
								if($lyricsdesc !=''){ ?>
												<div id="<?php echo preg_replace('/\s+/', '-', $attachment_title) ; ?>" class="lyricdesc">
													<?php 
													echo '<p><strong>'.$attachment_title.'</strong></p>';
													//echo wpautop($attachment->post_content); 
													$content = apply_filters('the_content', $attachment->post_content); 
													echo $content;
													?>
												</div>
												<?php } 
								$tracks_output.= '</div>';

								$tracks_output.= '<div class="tracklist-album tracklist-col">';
								$tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
								$tracks_output.= '<strong>'.$album_post_title.'</strong><p>'.$audio_artist_name.'</p></div>';
								$tracks_output.= '</div>';//.tracklist-details
								$tracks_output.= '</div>';//.tracklist-Row
								
								$tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$player_unique_id.'">
													<a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a>
												<div>'.get_the_term_list( $post->ID, 'label', '', ', ', '' ).' | '.$post_date.'</div></div>'; 
							}
							$k++;
						}
					}
				
					$error = false;
				}
				$start++;
			endwhile; 
			
			// Track Search Results Output
			if( $tracks_output ){
			
				echo '<div class="tracks-search inner">';
				echo '<h2>'.__('Tracks','musicplay').'</h2>';
				echo '<div class="tracklist-Table">';
				echo '<div class="tracklist-Header">';
				echo '<div class="tracklist-Row">';
				echo '<div class="tracklist-name tracklist-col">'.__('Tracks','musicplay').'</div>';
				echo '<div class="tracklist-album tracklist-col">'.__('Album','musicplay').'</div>';
				echo '</div>';// .tracklist-Row
				echo '</div>';//.tracklist-Header
				echo '<div class="tracklist-Body">';
				echo $tracks_output;
				
				// Tracks Pagination
				if( $search_pagination_option == 'on'){
					echo $tracks_pagination;
				}
				
				echo '</div>';//.tracklist-Body
				echo '</div>';//.tracklist-Table
				echo '</div>';//.tracklist-search
			}
			wp_reset_query();
		else :
		endif;
		echo '<div class="clear"></div>';
	}else{
		//$error = true;
	}
	

	$paged1 = isset( $_GET['paged1'] ) ? (int) $_GET['paged1'] : 1;
	$paged2 = isset( $_GET['paged2'] ) ? (int) $_GET['paged2'] : 1;
	
	add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );
	function title_like_posts_where( $where, &$wp_query ) {
		global $wpdb;
		if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
			$search_orderby_s = esc_sql( $wpdb->esc_like( $post_title_like ) );
			$where .= "AND $wpdb->posts.post_title LIKE '%{$search_orderby_s}%'";
		}
		return $where;
	}
	
	$album_args = array(
		'post_type' 		=> 'albums',
		'post_title_like' 	=> $keyword,
		'posts_per_page'	=> $al_posts_perpage,
		'paged' 			=> $paged1,
		'orderby'		 	=> 'date',
		'order'			 	=> 'ASC'
	);
	$iva_album_query = new WP_Query( $album_args );

	// Albums Search Results Output	
	$album_column_index = 0; $album_columns = 6;
	
	if( $album_columns == '6' ) { $album_class = 'col_sixth'; }
	if( $album_columns == '5' ) { $album_class = 'col_fifth'; }
	if( $album_columns == '4' ) { $album_class = 'col_fourth'; }
	if( $album_columns == '3' ) { $album_class = 'col_third'; }

	//Full Width Album Image Sizes
	if( $album_columns == '6' ) { $width='470'; $height = '470' ; }
	if( $album_columns == '5' ) { $width='470'; $height = '470' ; }
	if( $album_columns == '4' ) { $width='470'; $height = '470' ; }
	if( $album_columns == '3' ) { $width='470'; $height = '470' ; }

	if ( $iva_album_query->have_posts() ) : 

		$albums_title = get_option('atp_album_slug') ? get_option('atp_album_slug'):__('Albums','musicplay');
		
		echo '<div class="albums-search inner">';
		echo '<h2>'.$albums_title.'</h2>'; 
		
		while ( $iva_album_query->have_posts() ) : $iva_album_query->the_post();
		
			$audio_artist 		= array();
			$audio_artist_name  = get_post_meta( $post->ID,'audio_artist_name',true );
			
			if ( is_array( $audio_artist_name ) && count( $audio_artist_name ) > 0 ) {
				foreach( $audio_artist_name as $audio_artist_id){
					$permalink 		= get_permalink(  $audio_artist_id);
					$cs_artist_name 	= get_the_title(  $audio_artist_id);
					$audio_artist[]	= '<a href="'.$permalink.'">'.$cs_artist_name.'</a>';
				}
				$audio_artist_name = implode( ', ', $audio_artist );
			}
			
			$audio_catalog_id	= get_post_meta( $post->ID, 'audio_catalog_id', true );
			$img_alt_title 		= get_the_title();

			$album_column_index++;
			$album_last = ( $album_column_index == $album_columns && $album_columns != 1 ) ? 'end ' : '';
			echo '<div class="album-list   '.$album_class.'  '.$album_last.' ">';
			
			echo '<div class="custompost_entry">';
			if( has_post_thumbnail()){ 
				echo '<div class="custompost_thumb port_img">';
				echo '<figure>'.atp_resize( $post->ID, '', $width, $height, '', $img_alt_title ).'</figure>';
				echo '<div class="hover_type">';
				echo '<a class="hoveraudio" href="' .get_permalink(). '" title="' . get_the_title() . '"></a>';
				echo '</div>';
				echo'</div>';
			} 
			echo '<div class="album-desc">';
			echo '<h2 class="entry-title"><a href="'.get_the_permalink().'" rel="bookmark" title="'.esc_attr( get_the_title() ).'">'.get_the_title().'</a></h2>';
			//echo '<span class="label-text">'.strip_tags( get_the_term_list( $post->ID, 'label', '', ', ', '' )).'</span>';
			echo '<span class="label-text">'.$audio_artist_name.'</span>';
			echo '</div>';//.album-desc
			echo '</div>';// .custompost_entry
			echo '</div>';// .album_list
			
			if( $album_column_index == $album_columns ){
				$album_column_index = 0;
				echo '<div class="clear"></div>';
			}
		$error = false;
	endwhile; 
		
	echo '<div class="clear"></div>';

	$al_pagenum_link = html_entity_decode( get_pagenum_link() );
	$al_format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $al_pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$al_format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'paged1/%#%', 'paged1' ) : '?paged1=%#%';

	$al_page_args = array(
		'base' 		=> add_query_arg( 'paged1', '%#%' ),
		'format' 	=> $al_format,	
		'current' 	=> $paged1,
		'prev_next' => false,
		'total'   	=> $iva_album_query->max_num_pages,
		'add_args' 	=> array( 'paged2' => $paged2 )
	);

	// Album pagination
	if( $search_pagination_option == 'on'){
		echo '<div class="pagination">'.paginate_links( $al_page_args ).'</div>';
	}	
	echo '</div>';//.albums-search
	wp_reset_query();

	else :
	endif;
?>