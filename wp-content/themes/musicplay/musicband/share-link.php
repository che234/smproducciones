<?php

	// Google+
	$out = $output = '';
	$sharing_title = get_the_title();
	$sharing_post_title = (htmlspecialchars(trim(str_ireplace(' ', '%20', $sharing_title))));
	if ( get_option( 'atp_google_enable' ) == 'on' ) { 
		$out .= '<li class="google"><a target="_blank" href="http://plus.google.com/share?url='.get_permalink().'&amp;title='.$sharing_post_title.'&amp;annotation='.$sharing_post_title.'"><i class="fa fa-google-plus fa-lg"></i></a></li>';
	}
	
	// Linkdedin
	if ( get_option('atp_linkedIn_enable') == 'on' ) { 
		$out .= '<li class="linkedin"><a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.get_permalink().'&amp;title='.$sharing_post_title.'&amp;summary='.$sharing_post_title.'"><i class="fa fa-linkedin fa-lg"></i></a></li>';
	}
	
	// DiggIt
	if ( get_option('atp_digg_enable') == 'on') { 
		$out .= '<li class="digg"><a target="_blank" href="http://digg.com/submit?phase=2&amp;url='.get_permalink().'&amp;title='.$sharing_post_title.'&amp;bodytext='.$sharing_post_title.'"><i class="fa fa-digg fa-lg"></i></a></li>';
	}
	
	// StumbleUpon
	if ( get_option('atp_stumbleupon_enable') =='on'){ 
		$out .= '<li class="stumbleupon"><a target="_blank" href="http://www.stumbleupon.com/submit?url='.get_permalink().'&amp;title='.$sharing_post_title.'" rel="nofollow external"><i class="fa fa-stumbleupon fa-lg"></i></a></li>';
	}
	
	// Pinterest
	if ( get_option('atp_pinterest_enable') == 'on') { 
		$out .= '<li class="pinterest"><a target="_blank" href="http://pinterest.com/pin/create/button/?url='.get_permalink().'&amp;title='.$sharing_post_title.'"><i class="fa fa-pinterest fa-lg"></i></a></li>';
	}
	
	// Twitter
	if ( get_option('atp_twitter_enable') == 'on' ) { 
		$out .= '<li class="twitter"><a target="_blank" href="http://twitter.com/home?status='.$sharing_post_title.'-'.get_permalink().'"><i class="fa fa-twitter fa-lg"></i></a></li>';
	}
	
	// Facebook
	if ( get_option('atp_facebook_enable') == 'on' ){ 
		$out .='<li class="facebook"><a target="_blank" href="http://www.facebook.com/share.php?u='.get_permalink().'&amp;t='.$sharing_post_title.'"><i class="fa fa-facebook fa-lg"></i></a></li>';
	}
	
	if( !empty( $out ) ) {
	
		//$output .= '<div style="height: 20px;"></div>';
		$output .= '<div class="iva_sharing"><ul class="atpsocials share">';
		$output .= $out;
		$output .= '</ul><div class="clear"></div></div>';
		
		echo $output;
	}
?>