<?php
function atp_timeslider( $options ) {
	$output = '<div class="example-container">';
	$timeformat = get_option('atp_timeformat'); 
	if($timeformat!=12) {
		$output .="<script>
				jQuery(document).ready(function($){
					$('#event_starttime,#event_endtime').timepicker();
					jQuery('#ui-datepicker-div').addClass('iva-jquery-ui');
				});
				</script>";
	} else { 
		$output .="<script>
				jQuery(document).ready(function($){
					$('#event_starttime,#event_endtime').timepicker({
						hourGrid: 4,
						minuteGrid: 10,
						timeFormat: 'hh:mm tt'
					});
					jQuery('#ui-datepicker-div').addClass('iva-jquery-ui');
				});
				</script>";
	} 
	$output .='</div>';
	if( empty($options['meta'])){
		$output .= '<div class="sub_option"><label>Start Time:</label><input type="text" name="'.$options['field_id'].'[starttime]" id="event_starttime" value="" /></div>';
		$output .=	'<div class="sub_option"><label>End Time:</label><input type="text" name="'.$options['field_id'].'[endtime]"  id="event_endtime" value=""  /></div>';
	
		return $output;
	}else{
		$output .= '<div class="sub_option"><label>Start Time:</label><input type="text" name="'.$options['field_id'].'[starttime]" id="event_starttime" value="'.$options['meta']['starttime'].'" /></div>';
		$output .=	'<div class="sub_option"><label>End Time:</label><input type="text" name="'.$options['field_id'].'[endtime]"  id="event_endtime" value="'.$options['meta']['endtime'].'" /></div>';
		
	
		return $output;
	}
}
add_filter('timeslider','atp_timeslider');

function atp_map_location( $options ){
	$output ='';
	if( empty($options['meta'])){
		$output .= '<div class="sub_option"><label>Latitudes:</label><input type="text" name="'.$options['field_id'].'[latitudes]"/></div>';
		$output .= '<div class="sub_option"><label>Longitudes:</label><input type="text" name="'.$options['field_id'].'[longitutes]" /></div>';
		$output .= '<div class="sub_option"><label>zoom:</label><select  name="'.$options['field_id'].'[zoom]"  >';
		for($i=1;$i<=19;$i++){
			$zoom = isset($options['meta']['zoom']) ?  $options['meta']['zoom'] : '10';
			$selected =  $zoom == $i ? ' selected="selected"' : '';
			$output .= '<option value="'.$i.'"'.$selected.' >'.$i.'</option>';
		}
		$output .= '</select></div>';
		$output .= '<div class="clearfix" style="height: 10px;"></div>';
		$output .= '<div class="sub_option"><span>Controller:</span><input  type="checkbox" name="'.$options['field_id'].'[controller]"/></div>';
		return $output;
	}else{
		$output .= '<div class="sub_option"><label>Latitudes:</label><input type="text"  name="'.$options['field_id'].'[latitudes]"    value="'.$options['meta']['latitudes'].'" /></div>';
		$output .= '<div class="sub_option"><label>Longitudes:</label><input type="text"  name="'.$options['field_id'].'[longitutes]"   value="'.$options['meta']['longitutes'].'" /></div>';
		$output .= '<div class="sub_option"><label>zoom:</label><select name="'.$options['field_id'].'[zoom]">';
		$output .= '<div class="clearfix" style="height: 10px;"></div>';
		for($i=1;$i<=19;$i++){
			$selected =  $options['meta']['zoom'] == $i ? ' selected="selected"' : '';
			$output .= '<option value="'.$i.'"'.$selected.' >'.$i.'</option>';
		} 
		$output .= '</select></div>';
		$output .= '<div class="clearfix" style="height: 10px;"></div>';
		$checked=( isset($options['meta']['controller']) != '' ) ? 'checked' :'';
		$output .= '<div class="sub_option"><span>Controller:</span><input  type="checkbox" name="'.$options['field_id'].'[controller]"  '.$checked.'/></div>';
		return $output;
	}

}
add_filter('map_location','atp_map_location');

function atp_button( $options ){
		$output = '<div id="buttonsWrap">';
		$output .= '<div class="buttonsCount">';
		$output .= '<table id="buttonsort" class="widefat">';
		$output .= '<thead>';
		$output .= '<tr>';
		$output .= '<th>Button Text</th>';
		$output .= '<th>Link URL</th>';
		$output .= '<th>Color</th>';
		$output .= '<th>Actions</th>';
		$output .= '</tr>';
		$output .= '</thead>';
		$output .= '<tbody>';

	if( $options['meta'] == '' or empty($options['meta'])) {
		$output .= '<tr class="buttonsort-row">';
		$output .= '<td><input class="buttonname" name="'.$options['field_id'].'_buttonname[]" type="text"   value="" /></td>';
		$output .= '<td><input class="buttonurl" name="'.$options['field_id'].'_buttonurl[]" type="text"   value="" /></td>';
		$output .= '<td><input class="buttoncolor" name="'.$options['field_id'].'_buttoncolor[]" type="text"   value="" /></td>';
		$output .= '<td class="action">';
		$output .= '<a  title="Delete button" href="#" class="button button-primary" >Delete</a>';
		$output .= '</td>';
		$output .= '</tr>';
	}else{
		foreach ( $options['meta'] as $optionvals ){
			$output .= '<tr class="buttonsort-row">';
			$output .= '<td><input name="'.$options['field_id'].'_buttonname[]" class="buttonname" type="text"   value="'.$optionvals['buttonname'].'" /></td>';
			$output .= '<td><input name="'.$options['field_id'].'_buttonurl[]" class="buttonurl" type="text"   value="'.$optionvals['buttonurl'].'"/></td>';
			$output .= '<td><input name="'.$options['field_id'].'_buttoncolor[]" class="buttoncolor" type="text"   value="'.$optionvals['buttoncolor'].'" /></td>';
			$output .= '<td class="action">';
			$output .= '<a  title="Delete button" href="#" class="button button-primary" >Delete</a>';
			$output .= '</td>';
			$output .= '</tr>';
		}
	}

	$output .= '</tbody>';
	$output .= '</table>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<div class="clearfix" style="height: 10px;"></div>';
	$output .= '<a href="#" id="add-buttons" class="add button button-primary button-large">Add Button</a>';
	return $output;
}
add_filter('add_buttons','atp_button');
add_filter('add_externalmp3','atp_mp3external');
function atp_mp3external( $options ){
		$output = '<div class="mp3externalwrap">';
		$output .= '<div class="mp3urlCount">';
		$output .= '<table id="buttonsort" class="widefat">';
		$output .= '<tbody>';
	if( $options['meta'] == '' or empty($options['meta'])) {
		$output .= '<tr class="buttonsort-row">';
		$output .= '<td><div class="atp_externalmp3"><div class="ext_mp3url"><label>Mp3 URI</label><input class="buttonmp3" size="50" name="'.$options['field_id'].'_mp3url[]" type="text"   value="" /></div>';
		$output .= '<div class="ext_title"><label>Title</label><input class="buttontitle" size="50" name="'.$options['field_id'].'_mp3title[]" type="text"   value="" /></div>';
		$output .= '<div class="ext_dlink"><label>Download Link</label><input size="50" class="buttondownload" name="'.$options['field_id'].'_download[]" type="text"   value="" /></div>';
		$output .= '<div class="ext_buylink"><label>Buy Link</label><input size="50" class="buttonbuylink" name="'.$options['field_id'].'_buylink[]" type="text"   value="" /></div>';
		$output .= '<div class="ext_lyrics"><label>Lyrics</label><textarea class="buttonlyrics" name="'.$options['field_id'].'_lyrics[]" cols="47" row="10"/></textarea></div>';
		$output .= '<div class="ext_id"><label>Attachment ID</label><input size="50" name="'.$options['field_id'].'_mp3id[]" class="buttonid" type="text"   value=""/></div>';
		$output .= '<div class="ext_alldlink"><label>Add To Download Package</label><input size="50" class="allbuttondownload" name="'.$options['field_id'].'_alldownload[]" type="checkbox" value="" /></div>';
		$output .= '<div class="ext_delbtn"><label>&nbsp;</label><a title="Delete button" href="#" class="button button-primary red-button">Delete</a></div>';
		$output .= '</div></td>';
		$output .= '</tr>';
	}else{
		$i=0;
		foreach ( $options['meta'] as $optionvals ){

			$checked = '';					
			if(!empty($optionvals['alldownload'])) {				
				if($optionvals['alldownload'] == 'on') {
					$checked = 'checked="checked"';
				}else{
					$checked = '';
				}
			}else {
				$checked = '';
			}
			
			//var_dump($optionvals['alldownload']);
			$mp3id = esc_attr(isset($optionvals['mp3id'])) ? esc_attr($optionvals['mp3id']) : '' ;
			$output .= '<tr class="buttonsort-row">';
			$output .= '<td><div class="atp_externalmp3"><div class="ext_mp3url"><label>Mp3 URI</label><input size="50" name="'.$options['field_id'].'_mp3url[]" class="buttonmp3" type="text"   value="'.esc_attr($optionvals['mp3url']).'" /></div>';
			$output .= '<div class="ext_title"><label>Title</label><input size="50" name="'.$options['field_id'].'_mp3title[]" class="buttontitle" type="text"   value="'.esc_attr($optionvals['mp3title']).'"/></div>';
			$output .= '<div class="ext_dlink"><label>Download Link</label><input size="50" name="'.$options['field_id'].'_download[]" class="buttondownload" type="text"   value="'.esc_attr($optionvals['download']).'" /></div>';
			$output .= '<div class="ext_buylink"><label>Buy Link</label><input size="50" name="'.$options['field_id'].'_buylink[]" class="buttonbuylink" type="text"   value="'.esc_attr($optionvals['buylink']).'" /></div>';
			$output .= '<div class="ext_lyrics"><label>Lyrics</label><textarea name="'.$options['field_id'].'_lyrics[]" class="buttonlyrics"  cols="47" row="10" />'.$optionvals['lyrics'].'</textarea></div>';
			$output .= '<div class="ext_id"><label>Attachment ID</label><input size="50" name="'.$options['field_id'].'_mp3id[]" class="buttonid" type="text"   value="'.$mp3id.'"/></div>';
			$output .= '<div class="ext_alldlink"><label>Add To Download Package</label><input size="50" class="allbuttondownload" name="'.$options['field_id'].'_alldownload['.$i.']" type="checkbox" '. $checked .'/></div>';
			$output .= '<div class="ext_delbtn"><label>&nbsp;</label><a title="Delete button" href="#" class="button button-primary red-button">Delete</a></div>';
			$output .= '</div></td>';
			$output .= '</tr>';
			$i++;
		}
	}

	$output .= '</tbody>';
	$output .= '</table>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<div class="clearfix" style="height: 10px;"></div>';
	$output .= '<a href="#" id="add-mp3url" class="add button button-primary green-button button-hero">Add</a>';
	return $output;
}
add_filter('add_custom_meta','iva_add_custom_meta');
function iva_add_custom_meta( $options ) {

	$output  = '';
	$fieldid = $options['field_id'];
	$c = 0;
	?>
	<?php
	$meta 	 = $options['meta'] ?  $options['meta'] :'';	
	$output = '<div class="metawrap">';
	if( !empty( $meta ) ) {
		foreach ( $meta as $optionvalues ){
			$output .= '<div class="iva_meta_row clearfix">';
			$output .= '<div class="iva_meta_label"><label>Label</label><input  name="'.$fieldid.'['.$c.'][label]" class="meta_label" type="text" value="'.esc_attr( $optionvalues['label'] ).'" /></div>';
			$output .= '<div class="iva_meta_value"><label>Value</label><input  name="'.$fieldid.'['.$c.'][value]" class="meta_value" type="text" value="'.esc_attr( $optionvalues['value'] ).'"/></div>';
			$output .= '<span><a title="Delete metainfo" href="javascript:void(0);" class="delete-meta-info red-button button-primary red">Delete</a></span>';
			$output .= '</div>';
				$c = $c + 1;
		}
	}			
	$output .= '</div>';//.metawrap
	?>
	<script>
	$ = jQuery.noConflict();
	jQuery(document).ready(function($) {
		$(".delete-meta-info").live('click', function() {
			$(this).parent().parent().remove();
		});
		var count = <?php echo $c; ?>;
		$("#add-metainfo").click(function() {
			count = count + 1;
			$('.metawrap').append('<div class="iva_meta_row clearfix"><div class="iva_meta_label"><label>Label</label><input type="text" name="<?php echo $fieldid; ?>['+count+'][label]" value=""></div><div class="iva_meta_value"><label>Value</label><input type="text" name="<?php echo $fieldid; ?>['+count+'][value]"></div><span><a title="Delete Meta Info" href="javascript:void(0);" class="delete-meta-info red-button button-primary">Delete</a></span></div>');	
			
			return false;
		});
	});
	</script>
	<?php
	$output .='<button type="button" id="add-metainfo" class="add_meta_button green-button button-primary">'.__('Add Meta Info','iva_theme_admin').'</button>';
	echo $output;
}
?>