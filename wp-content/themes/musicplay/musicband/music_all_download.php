<?php
$path 		= __FILE__;
$pathwp 	= explode( 'wp-content', $path );
$wp_url		= $pathwp[0];
require_once( $wp_url.'/wp-load.php' );

function pp_album_download(){

	error_reporting(0);
	
	if( $_GET['albumid'] ){
		//do;
		$albumid = $_GET['albumid'];
	
		$post = get_post($albumid);
		$audio_posttype_option= get_post_meta( $albumid, 'audio_posttype_option', true);		
		$tmpdir = ABSPATH . 'wp-content/tmp-album/'.$albumid.'-'.mt_rand(100,999).'/';			
		$download_file = ABSPATH . 'wp-content/tmp-album/album-' . $post->post_name .'.zip';
	
		@mkdir($tmpdir, 0777, true);
		if( $audio_posttype_option=='medialibrary' ){  
		 $tracks = get_post_meta( $albumid, 'audio_medialibrary', false); 
			foreach( $tracks as $trackid ){
				$attachment = get_post( $trackid );	
			global $blog_id;
			 $track = get_post_meta( trim($trackid), '_wp_attached_file', true);
			$all_download = get_post_meta( trim($trackid), 'all_download', true) ;
			$download_link = get_post_meta( trim($trackid), 'download', true) ;
			 $to = $tmpdir . basename($track);
			// Look for Multisite Path
			if (preg_match("/files/", $attachment->guid)) {
				if($all_download == '1' && $download_link !=''){
					$path = explode('/',$attachment->guid);
					foreach($path as $k=>$v){
					  if( $k == '5'){
						$from = ABSPATH . 'wp-content/blogs.dir/'.$blog_id.'/files/'.$v;
						 if( file_exists($from) ){
							copy($from, $to );
						 }
					  }					
					}
				}
			}else{
				//$trackid = get_attachment_id_from_src( $attachment->guid );	
				if($all_download == '1' && $download_link !=''){
					$from = ABSPATH . 'wp-content/uploads/' . trim($track, '/');
					$to = $tmpdir . basename($track);
					if( file_exists($from) ){
						copy($from, $to );
					}
				}
			
			}

							
			}
		}
		if($audio_posttype_option=='player'){
			$tracks = get_post_meta( $albumid, 'audio_upload', true);			
			$audio_list=array();
			$audio_list=explode(',',$tracks);
			foreach( $audio_list as $trackid ){
				$attachment = get_post( $trackid );				
				$trackid= get_attachment_id_from_src( $attachment->guid );
				global $blog_id;	
				$track = get_post_meta( trim($trackid), '_wp_attached_file', true);
				$all_download = get_post_meta( trim($trackid), 'all_download', true);
				$download_link=get_post_meta( trim($trackid), 'download', true);
				$to = $tmpdir . basename($track);
				if (preg_match("/files/", $attachment->guid)) {
					if($all_download == '1' && $download_link !=''){
						$path = explode('/',$attachment->guid);
						foreach($path as $k=>$v){
						  if( $k == '5'){
							$from = ABSPATH . 'wp-content/blogs.dir/'.$blog_id.'/files/'.$v;
							 if( file_exists($from) ){
								copy($from, $to );
							 }
						  }					
						}
					}
				} else {
					if($all_download == '1' && $download_link !=''){
						$from = ABSPATH . 'wp-content/uploads/' . trim($track, '/');
						$to = $tmpdir . basename($track);
						if( file_exists($from) ){
							copy($from, $to );
						}	
					}
				}
							
			}
		} 
		
		if($audio_posttype_option=='externalmp3'){ 
			$tracks =get_post_meta( $albumid, 'audio_mp3', true); 			
			foreach( $tracks as $trackid ){	
				$track = get_post_meta( trim($trackid), '_wp_attached_file', true);
				$all_download = $trackid['alldownload'];
                $download = $trackid['download'];
				
				if($all_download == 'on' && $download !=''){
				$to = $tmpdir . basename($trackid['mp3url']);
					if( file_get_contents($trackid['mp3url']) ){
						copy($trackid['mp3url'], $to );
					}
				}				
			}
		}

		// Get real path for our folder
		$rootPath = realpath($tmpdir);
		
		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open( $download_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
		
		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator($rootPath),
			RecursiveIteratorIterator::LEAVES_ONLY
		);
		
		foreach ($files as $name => $file)
		{
			// Skip directories (they would be added automatically)
			if (!$file->isDir())
			{
				// Get real and relative path for current file
				$filePath = $file->getRealPath();
				$relativePath = substr($filePath, strlen($rootPath) + 1);
		
				// Add current file to archive
				$zip->addFile($filePath, $relativePath);
			}
		}
		
		// Zip archive will be created only after closing object
		$zip->close();
		
	pp_clear_dir($tmpdir);
		//////////////////////////////////////////////////////
		
		if (headers_sent()) {
		    //echo 'HTTP header already sent';
		} else {
		    if (!is_file($download_file)) {
		        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
		        //echo 'File not found';
		    } else if (!is_readable($download_file)) {
		        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
		        //echo 'File not readable';
		    } else {
		        header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
		        header("Content-Type: application/zip");
		        header("Content-Transfer-Encoding: Binary");
		        header("Content-Length: ".filesize($download_file));
		        header("Content-Disposition: attachment; filename=\"".basename($download_file)."\"");
		        readfile($download_file);
		        unlink($download_file);
		        exit;
		    }
		}
		//////////////////////////////////////////////////////
		
	}
	else{
		return false;
	}
}

pp_album_download();

function pp_clear_dir($dir, $type = 'all'){
	$odir = opendir($dir);
	while(($file = readdir($odir))!==false){
		if(!in_array($file,array(".",".."))){
			unlink($dir."/".$file);
		}
	}
	rmdir($dir);
}
?>