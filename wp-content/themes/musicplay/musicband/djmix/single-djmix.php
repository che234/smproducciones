<?php


/** 
 * The Header for our theme.
 * Includes the header.php template file. 
 */

get_header(); ?>

	
	<div id="primary" class="pagemid">
	<div class="inner">

		<div class="content-area">
		<?php
		$atp_djmix_like_enable       = get_option( 'atp_song_like_btn' ) ? get_option( 'atp_djmix_like_btn' ) : 'true' ;
		$atp_djmix_count_enable      = get_option( 'atp_song_count_btn' ) ? get_option( 'atp_djmix_count_btn' ) : 'true' ;
		?>
			<?php if (have_posts()): while (have_posts()): the_post(); ?>	
			
			<div <?php post_class('custompost-single'); ?> id="post-<?php the_ID(); ?>">
			<?php
				$add_to_playlist_txt    = get_option('atp_add_to_playlist_txt') ? get_option('atp_add_to_playlist_txt') :  __('Add songs to playlist', 'musicplay');
				$djmix_genre		= get_post_meta( $post->ID, 'djmix_genre', true );
				$djmix_buy_mix		= get_post_meta( $post->ID, 'djmix_buy_mix', true );
				$djmix_buy_url		= get_post_meta( $post->ID, 'djmix_buy_url', true );
				$djmix_download_url	= get_post_meta( $post->ID, 'djmix_download_url', true );
				$djmix_download_text 	= get_post_meta( $post->ID, 'djmix_download_text', true ) ? get_post_meta( $post->ID, 'djmix_download_text', true ) : 'Download';
				$audio_posttype_option  = get_post_meta( $post->ID, 'audio_posttype_option', true )? get_post_meta( $post->ID, 'audio_posttype_option', true ) :'player';
				$djmix_upload_mix	= get_post_meta( $post->ID, 'djmix_upload_mix', true );
				$post_date	        = get_post_meta(get_the_id(),'djmix_release_date',true);
				
				if($post_date !='') { 
					if(is_numeric($post_date)){
						$post_date = date_i18n($default_date,$post_date);
					}	
				}
				

				$audio_auto_play	        = get_option('atp_audio_autoplay') ? get_option('atp_audio_autoplay') : 'false';
				$imagesrc			= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
				$image				= aq_resize($imagesrc[0], '80', '80', true );
				$djtitle			= get_the_title($post->ID);
				$img_alt_title 		        = get_the_title();	
				?>
				<?php 
				 $djmix_list = get_post_meta($post->ID,'djmix_upload_mix',true) ? get_post_meta($post->ID,'djmix_upload_mix',true) :'';
					if($djmix_list) {
						global $wpdb;
						$djmix_list = $wpdb->get_col("
							SELECT ID FROM {$wpdb->posts}
							WHERE post_type = 'attachment'
							AND ID in ({$djmix_list})
							ORDER BY menu_order ASC");
					}  
			
					//  Next and previous links
					echo '<div class="iva-np-headwrap">';				
					echo '<div class="iva-np-navs">';
					echo '<div class="iva-np-pagination">';
					echo previous_post_link( $link = '%link','<i class="fa fa-angle-left"></i>');
					echo next_post_link( $link = '%link', '<i class="fa fa-angle-right"></i>');
					echo '</div>';
					echo '</div>';
					echo '</div>';
				?>
					<div class="mp3list-head">
						<?php

						// Add tracks to playlist
						if(get_option('atp_playlist_enable') == 'on' ) {  ?>
						<span class="add-to-playlist-btn"><i class="fa fa-bars fa-fw"></i>&nbsp;<a href="#" class="fap-add-playlist alldata-playlist" data-enqueue="yes" data-playlist="any-playlist"><?php echo $add_to_playlist_txt; ?></a></span>
						<?php }
						
						// Album Likes
						$post_likes_option = get_option('atp_like_btn');
						if ( $post_likes_option != 'on' ){
							if ( function_exists( 'iva_get_post_like' ) ) {
								echo iva_get_post_like( $post->ID );
							}
						}
						?>
					</div>


				<div class="djmix-list" data-playlist="any-playlist">

				  <div class="djmix_thumb">
						<?php if ( has_post_thumbnail() ) { echo atp_resize( $post->ID, '', '60','60', '', $djtitle ); } ?>
					</div>

					<div class="djmix-content">
							<?php 
							$start ='';	
							if($audio_posttype_option == 'player'){
								if(!empty($djmix_list)){
									foreach($djmix_list as $attachment_id){
										$trackcount ='0';
										$attachment = get_post( $attachment_id );
										$title = $attachment->post_title;
										if (  isset( $attachment->trackcount ) ) {
												
											$trackcount= $attachment->trackcount;
												
										}
										$attachmentid  = $post->ID;
										$trackCountlist = isset($trackcount) ?  $trackcount :'0';
										echo '<span class="single-player-meta" id="single-player-meta'.$start.'">'.$djtitle.'</span>';
										echo '<h2 class="entry-title djmix-title"><a data-meta="#single-player-meta'.$start.'" class="fap-single-track no-ajaxy" data-id="'.$attachmentid.'" href="'.$attachment->guid.'" title="'.$title.'"  rel="'.$image.'"><i class="ivaplay"></i></a>';
										echo '<a href="'. get_permalink().'">'.$djtitle.'</a>';
										echo '</h2>';
									}
								} 
								 //echo $playlisttitles; 
							}
							elseif($audio_posttype_option == 'externalmp3'){
								echo '<span class="single-player-meta" id="single-player-meta'.$start.'">'.$djtitle.'</span>';
								$attachmentid  = '0'.$post->ID;
								$trackCountlist = get_post_meta($attachmentid,'trackcount',true) ? get_post_meta($attachmentid,'trackcount',true) : '0';
								$djmix_externalmp3url = get_post_meta($post->ID,'djmix_externalmp3',true); 
								$djmix_externalmp3urltitle = get_post_meta($post->ID,'djmix_externalmp3title',true);
								
								echo '<h2 class="entry-title djmix-title"><a  data-meta="#single-player-meta'.$start.'" data-id="'.$attachmentid.'"	rel="'.$image.'" title="'.$djmix_externalmp3urltitle.'" href="'.$djmix_externalmp3url.'" class="fap-single-track no-ajaxy"><i class="ivaplay"></i></a>';
								echo '<a href="'. get_permalink().'">'.$djtitle.'</a>';
								echo '</h2>';
							}elseif($audio_posttype_option == 'medialibrary'){
								$djmixmediaid= get_post_meta($post->ID,'djmix_medialibrary',false) ? get_post_meta($post->ID,'djmix_medialibrary',false) :'';
								if( !empty( $djmixmediaid ) ) {
								
									foreach( $djmixmediaid as $mediaupload_list) {
										$attachment = get_post( $mediaupload_list );
										$trackCountlist ='0';
										$attachmentid  = $attachment->ID;
										if (  isset( $attachment->trackcount ) ) {
													
												$trackCountlist= $attachment->trackcount;
													
										}
									}
								}
								echo '<h2 class="entry-title djmix-title"><a  data-meta="#single-player-meta'.$start.'" data-id="'.$attachmentid.'"	rel="'.$image.'" title="" href="'.$attachment->guid.'" class="fap-single-track no-ajaxy"><i class="ivaplay"></i></a>';
								echo '<a href="'. get_permalink().'">'.$djtitle.'</a>';
								echo '</h2>';
							}
							else {
								$attachmentid  = $post->ID;
								$trackCountlist = get_post_meta($attachmentid,'trackcount',true) ? get_post_meta($attachmentid,'trackcount',true) : '0';
								$audio_soundcloud_url = get_post_meta($post->ID,'audio_soundcloud_url',true);
								echo '<h2 class="entry-title djmix-title"><a href="'.$audio_soundcloud_url.'" data-id="'.$attachmentid.'" class="fap-single-track no-ajaxy"><i class="ivaplay"></i></a>';
								echo '<a href="'. get_permalink().'">'.$djtitle.'</a>';
								echo '</h2>';
							
							} ?>
							
							<div class="djmix-postmeta">
								<!-- Title and Meta -->
								<?php if (  $djmix_genre != '' ) { ?><span><?php echo $djmix_genre; ?></span><?php } ?>
								<?php if (  $post_date != '' ) { ?><span><?php echo $post_date; ?></span><?php } ?>
							</div>
								<div class="djmix-buttons">
								<?php	
								// djmix somg count enable/disable since 3.2.5
								if( $atp_djmix_count_enable != 'on' ){	
									echo '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachmentid.'">'.$trackCountlist.'</span></span>';
								}
								// djmix  song like  enable/disale since 3.2.5
								if( $atp_djmix_like_enable != 'on' ){
									echo iva_track_like($attachmentid);
								}
								?>
								<?php
								// BuyMix Button
								if ( $djmix_buy_mix != '' ) { ?>
									<div class="djbtn"><a href="<?php echo $djmix_buy_url; ?>" target="_blank"><button type="button" class="btn mini abestos"><span><?php echo $djmix_buy_mix; ?></span></button></a></div>
								<?php } ?>
								<?php
								// DjMix Download Button
								if ( $djmix_download_url != '' ) { 
									$download_count = get_post_meta( $attachmentid,'download_count',true);
									?>
									<div class="djbtn">
									<button type="button" class="btn mini abestos music_download" data-id ="<?php echo $attachmentid;?>" data-download="<?php echo iva_get_download( $djmix_download_url ); ?>">
									<span><?php echo $djmix_download_text; ?>&nbsp;&nbsp;&nbsp;<span class="iva_dcount"><?php echo(int)$download_count; ?></span></span></button></span>
									</div>
								<?php } ?>
								
								</div><!-- .djmix-buttons -->
				</div><!-- .djmix-content -->
				<?php the_content(); ?>

				</div><!-- .djmix-list -->
			
			</div><!-- #post-<?php the_ID();?> -->
			<?php $start++; endwhile; ?>
			<?php else: ?>
			<?php '<p>' . __('Sorry, no projects matched your criteria.', 'musicplay') . '</p>';?>
			<?php endif; ?>

			<?php edit_post_link(__('Edit', 'musicplay'), '<span class="edit-link">', '</span>'); ?>

			<?php 
			$comments = get_option('atp_artist_comments');
			if ( $comments == 'enable' ) {
				comments_template( '', true ); 
			}?>
			</div><!-- .content-area -->
			<?php if ( atp_generator( 'sidebaroption', $post->ID ) != "fullwidth" ){ get_sidebar(); } ?>

	</div><!-- inner -->

	</div><!-- #primary.pagemid -->	
	<?php get_footer(); ?>