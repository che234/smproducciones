<?php
/*
Template Name: Dj Mix
*/

get_header(); ?>
<?php
	global $default_date;
 ?>

<div id="primary" class="pagemid">
	<div class="inner">

		<div class="content-area">

			<?php while (have_posts()): the_post(); ?>
			<?php the_content(); ?> 
			<?php endwhile; ?>

			<?php
			if ( get_query_var('paged') ) {
				$paged = get_query_var('paged');
			}
			elseif ( get_query_var('page') ) {
				$paged = get_query_var('page');
			} else {
				$paged = 1;  
			}
			
			$start = 1;
			$orderby = get_option('atp_djmix_orderby') ? get_option('atp_djmix_orderby') : 'date';
			$order   = get_option('atp_djmix_order') ? get_option('atp_djmix_order') : 'ASC';
			$atp_djmix_like_enable       = get_option( 'atp_djmix_like_btn' ) ? get_option( 'atp_djmix_like_btn' ) : 'true' ;
			$atp_djmix_count_enable      = get_option( 'atp_djmix_count_btn' ) ? get_option( 'atp_djmix_count_btn' ) : 'true' ;	
			$pagination = get_option( 'atp_djmix_pagination' );

			if ( $pagination == 'on' ){
				$djmix_limit = get_option( 'atp_djmix_limits' );
			}else{
				$djmix_limit = '-1' ;
			}
			if($orderby == 'djmix_release_date'){
				$meta_key ='djmix_release_date';
			}else{
				$meta_key = '';
			}

			// EO pagination limit options

			$args = array(
				'post_type' 	=> 'djmix',
				'posts_per_page'=> $djmix_limit, 
				'paged' 		=> $paged,
				'meta_key'  	=> $meta_key,
				'orderby'		=>	$orderby,
				'order'			=>	$order
			);

			$wp_query = new WP_Query( $args );
					
			if ( $wp_query->have_posts()) : while (  $wp_query->have_posts()) :  $wp_query->the_post(); 

				$djmix_genre           = get_post_meta( $post->ID, 'djmix_genre', true );
				$djmix_buy_mix         = get_post_meta( $post->ID, 'djmix_buy_mix', true );
				$djmix_buy_url         = get_post_meta( $post->ID, 'djmix_buy_url', true );
				$djmix_download_url    = get_post_meta( $post->ID, 'djmix_download_url', true );
				$djmix_download_text   = get_post_meta( $post->ID, 'djmix_download_text', true ) ? get_post_meta( $post->ID, 'djmix_download_text', true ) : 'Download';
				$audio_posttype_option = get_post_meta( $post->ID, 'audio_posttype_option', true )? get_post_meta( $post->ID, 'audio_posttype_option', true ) :'';
				$djmix_upload_mix      = get_post_meta( $post->ID, 'djmix_upload_mix', true );
				$post_date             = get_post_meta(get_the_id(),'djmix_release_date',true);
				$audio_soundcloud_url = get_post_meta($post->ID,'audio_soundcloud_url',true);
				$attachmentid = '';

				if ( $post_date != '' ) { 
					if ( is_numeric( $post_date ) ) {
						$post_date = date_i18n( $default_date, $post_date );
					}
				}

				$imagesrc         = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail', false, '' );
				$image            = aq_resize( $imagesrc[0], '80', '80', true );
				$djtitle          = get_the_title( $post->ID );
				$djmix_list       = get_post_meta( $post->ID, 'djmix_upload_mix', true ) ? get_post_meta( $post->ID, 'djmix_upload_mix', true ) : '';

				if ( $djmix_list ) {
					global $wpdb;
					$djmix_lists = $wpdb->get_col("
						SELECT ID FROM {$wpdb->posts}
						WHERE post_type = 'attachment'
						AND ID in ({$djmix_list})
						ORDER BY menu_order ASC");
				} ?>

				<div id="post-<?php the_ID(); ?>" class="djmix-list clearfix">

					<div class="djmix_thumb">
						<?php if ( has_post_thumbnail() ) { echo atp_resize( $post->ID, '', '60','60', '', $djtitle ); } ?>
					</div>

					<div class="djmix-content">
						<?php 
						/**
						 * Music Player - Audio Type
						 */		
						if ( $audio_posttype_option == 'player' ) {

							$trackcount = '0';
							if( !empty($djmix_lists) ) {
								foreach ( $djmix_lists as $attachment_id ) {
									$attachment = get_post( $attachment_id );
									$title = $attachment->post_title;
									$attachmentid = $attachment->ID;
									if ( isset( $attachment->trackcount ) ) {
										$trackcount = $attachment->trackcount;
									}

									$trackCountlist = isset($trackcount) ?  $trackcount : '0';
									echo '<span class="single-player-meta" id="single-player-meta'.$start.'"><a href="'. get_permalink().'">'.$djtitle.'</a></span>';

									echo '<h2 class="entry-title djmix-title"><a data-meta="#single-player-meta'. $start.'" data-id="'. $attachmentid.'" class="fap-single-track no-ajaxy" href="'. $attachment->guid.'" title="'. $title.'" rel="'. $image.'"><i class="ivaplay"></i></a>';
									echo '<a href="'. get_permalink().'">'.$djtitle.'</a>';
									echo '</h2>';
								}
							} 
							//echo $playlisttitles;
						}

						/**
						 * External MP3 - Audio Type
						 */
						elseif($audio_posttype_option == 'externalmp3'){
							echo '<span class="single-player-meta" id="single-player-meta'.$start.'"><a href="'. get_permalink().'">'.$djtitle.'</a></span>';
							$djmix_externalmp3url = get_post_meta($post->ID,'djmix_externalmp3',true); 
							$djmix_externalmp3urltitle = get_post_meta($post->ID,'djmix_externalmp3title',true);
							$attachmentid  = '0'.$post->ID;
							$trackCountlist = get_post_meta($attachmentid,'trackcount',true) ? get_post_meta($attachmentid,'trackcount',true) : '0';
							echo '<h2 class="entry-title djmix-title"><a  data-meta="#single-player-meta'.$start.'" data-id="'.$attachmentid.'"	rel="'.$image.'" title="'.$djmix_externalmp3urltitle.'" href="'.$djmix_externalmp3url.'" class="fap-single-track no-ajaxy"><i class="ivaplay"></i></a>';
							echo '<a href="'. get_permalink().'">'.$djtitle.'</a>';
							echo '</h2>';
						}
						
						/**
						 * Soundcloud - Audio Type
						 */
						elseif($audio_posttype_option == 'soundcloud'){
							$attachmentid  = $post->ID;
							$trackCountlist = get_post_meta($attachmentid,'trackcount',true) ? get_post_meta($attachmentid,'trackcount',true) : '0';	
						 echo '<h2 class="entry-title djmix-title"><a href="'.$audio_soundcloud_url.'" data-id="'.$attachmentid.'"  class="fap-single-track no-ajaxy"><i class="ivaplay"></i></a>';
						 echo '<a href="'. get_permalink().'">'.$djtitle.'</a>';
						 echo '</h2>';
						 }
						/**
						 * Media Library - Audio Type
						 */
						elseif($audio_posttype_option == 'medialibrary'){ 
						$djmixmediaid= get_post_meta($post->ID,'djmix_medialibrary',false) ? get_post_meta($post->ID,'djmix_medialibrary',false) :'';
						if( !empty( $djmixmediaid ) ) {
						
							foreach( $djmixmediaid as $mediaupload_list) {
								$attachment = get_post( $mediaupload_list );
								$trackCountlist ='0';
								$attachmentid  = $attachment->ID;
								if (  isset( $attachment->trackcount ) ) {
											
										$trackCountlist= $attachment->trackcount;
											
								}
							}
						}
					
						echo '<h2 class="entry-title djmix-title"><a data-meta="#single-player-meta'.$start.'" href='.$attachment->guid.'" rel="'.$image.'"  data-id="'.$attachmentid.'" class="fap-single-track no-ajaxy"><i class="ivaplay"></i></a>';
						echo '<a href="'. get_permalink().'">'.$djtitle.'</a>';
						echo '</h2>';
						 } ?>
					

							<div class="djmix-postmeta">
							<?php if (  $djmix_genre != '' ) { ?><span><?php echo $djmix_genre; ?></span><?php } ?>
							<?php if (  $post_date != '' ) { ?><span><?php echo $post_date; ?></span><?php } ?>
							</div>
							<div class="djmix-buttons">
							<!--  Track Count And Likes  -->
							<?php
							// djmix count enable/disable since 3.2.5
							if( $atp_djmix_count_enable != 'on') {	
								echo '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachmentid.'">'.$trackCountlist.'</span></span>';
							}
							// djmix  song like  enable/disale since 3.2.5
							if( $atp_djmix_like_enable != 'on' ){
								echo iva_track_like( $attachmentid );
							}
							?>
							<?php
							// BuyMix Button
							if ( $djmix_buy_mix != '' ) { ?>
								<div class="djbtn"><a href="<?php echo $djmix_buy_url; ?>" target="_blank"><button type="button" class="btn mini abestos"><span><?php echo $djmix_buy_mix; ?></span></button></a></div>
							<?php } ?>
							<?php
							// DjMix Download Button
							if ( $djmix_download_url != '' ) { ?>
								<div class="djbtn"><button type="button" class="btn mini abestos music_download" data-id ="<?php echo $attachmentid;?>" data-download="<?php echo iva_get_download( $djmix_download_url ); ?>"><span><?php echo $djmix_download_text; ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="iva_dcount"><?php echo(int)$download_count; ?></span></span></button></span></div>
							<?php } ?>
							
							</div><!-- .djmix-buttons -->
				</div><!-- .djmix-content -->

					<?php the_content(); ?>

				</div><!-- .djmix-list -->
			<?php $start++; ?>
			
			<?php endwhile; ?>
			
			<?php if ( $pagination == 'on' ) { echo atp_pagination(); } ?>
			
			<?php wp_reset_query(); ?>
				
			<?php else : ?>
	
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'musicplay' ); ?></p>
	
			<?php get_search_form(); ?>
	
			<?php endif;?>
					
			<div class="clear"></div>
	
			<?php edit_post_link( __( 'Edit', 'musicplay' ), '<span class="edit-link">', '</span>' ); ?>
	
		</div><!-- .content-area -->

		<?php if ( atp_generator( 'sidebaroption', $post->ID) != "fullwidth" ){ get_sidebar(); } ?>

	</div><!-- inner -->
</div><!-- #primary.pagemid -->

<?php get_footer(); ?>