<?php
$path 		= __FILE__;
$pathwp 	= explode( 'wp-content', $path );
$wp_url		= $pathwp[0];
require_once( $wp_url.'/wp-load.php' );

$filename 		= $_GET['type']; 
$attachment_id 	= $_GET['iva_id']; 
$dcount 		= $_GET['dcount']; 

function iva_download_file( $file, $attachment_id, $dcount ){
   
    $filename = basename($file);
	$file_extension = strtolower(substr(strrchr($filename,"."),1));
	
    //This will set the Content-Type to the appropriate setting for the file
    switch( $file_extension ) {
		  case "mp3": $ctype="audio/mpeg"; break;
		  //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
		  case "wav":
		  case "mpeg":
		  case "mpg":
		  case "mpe":
		  case "mov":
		  case "avi": 
		  case "png": 
		  case "jpeg":
		  case "jpg": 
		  case "txt": die("<b>Cannot be used for ". $file_extension ." files!</b>"); break;
		  default: $ctype="application/force-download";
    }

	// Update download couts
	update_post_meta( $attachment_id ,"download_count",$dcount);
	
    //Begin writing headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
   
    //Use the switch-generated Content-Type
    header("Content-Type: $ctype");

    //Force the download
    $header="Content-Disposition: attachment; filename=".$filename.";";
    header($header );
    header("Content-Transfer-Encoding: binary");
   // header("Content-Length: ".$len);
    @readfile($file);
    exit;
}
$code 		= new Encryption;
$dcode		= $code->decode( $filename );
iva_download_file( $dcode ,$attachment_id,$dcount );
?>