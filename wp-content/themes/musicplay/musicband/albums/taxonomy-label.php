<?php

/** 
 * The Header for our theme.
 * Includes the header.php template file. 
 */

get_header(); ?>

	
	<div id="primary" class="pagemid">
	<div class="inner">

		<div class="content-area">

			<?php
			//Columns for Album Thumbs
			$column_index = 0; $columns = 4;
			if( $columns == '4' ) { $class = 'col_fourth'; }
			if( $columns == '3' ) { $class = 'col_third'; }

			//Full Width Album Image Sizes
			if( $columns == '4' ) { $width='470'; $height = '470' ; }
			if( $columns == '3' ) { $width='470'; $height = '470' ; }

			if ( get_query_var('paged') ) {
				$paged = get_query_var('paged');
			}
			elseif ( get_query_var('page') ) {
				$paged = get_query_var('page');
			} else {
				$paged = 1;  
			}
			
			$albums_orderby = get_option('atp_album_orderby');
			$albums_order = get_option('atp_album_order');
			
			$pagination	= get_option('atp_audio_pagination');

			if( $albums_orderby == 'meta_value_num'){
				$meta_key = 'audio_release_date';
			}else{
				$meta_key = "";
			}
		
			$taxonomy_label_obj = $wp_query->get_queried_object();
			if( $taxonomy_label_obj ){
				$args = array(
					'post_type'	=> 'albums', 
					'tax_query' => array(
							array(
								'taxonomy' => 'label', 
								'field' => 'id',
								'terms' => $taxonomy_label_obj->term_id
							)
					),
					'meta_key'  => $meta_key, // date order by
					'paged'		=> $paged,
					'orderby'	=> $albums_orderby,
					'order'		=> $albums_order
				);
			}
			
			$iva_label_query = new WP_Query( $args );
	
			if ( $iva_label_query->have_posts()) : while (  $iva_label_query->have_posts()) :  $iva_label_query->the_post(); 
			
				$audio_artist_name	= get_post_meta( $post->ID, 'audio_artist_name', true );
				$audio_catalog_id	= get_post_meta( $post->ID, 'audio_catalog_id', true );
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'full', true );

				$column_index++;
				$last = ( $column_index == $columns && $columns != 1 ) ? 'end ' : '';
				?>

				<div class="album-list  <?php echo $class. ' ' .$last; ?>" >
					<div class="custompost_entry">

						<?php if( has_post_thumbnail()){ ?>
						<div class="custompost_thumb port_img">
							<?php
							if ( get_option('atp_hovericon') ) {
								echo '<figure><a href="' .get_permalink(). '" title="' . get_the_title() . '">';
								echo atp_resize( $post->ID, '', $width, $height, '', get_the_title() );
								echo '</a></figure>';
							} else {
								echo '<figure>'.atp_resize( $post->ID, '', $width, $height, '', get_the_title() ).'</figure>';
								echo '<div class="hover_type">';
								echo '<a class="hoveraudio"  href="' .get_permalink(). '" title="' . get_the_title() . '"></a>';
								echo '</div>';
								echo '<span class="imgoverlay"></span>';
							}
							?>
						</div><!-- .custompost_thumb -->
						<?php } ?>

						<div class="album-desc">
							<h2 class="entry-title">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( __( "Permanent Link to %s", 'musicplay' ), esc_attr( get_the_title() ) ); ?>"><?php the_title(); ?></a>
							</h2>
							<span><?php echo strip_tags(get_the_term_list( $post->ID, 'label', '', ', ', '' ));?></span>
						</div><!-- .album-desc-->

					</div><!-- .custompost_entry -->
				</div><!-- .album_list -->

				<?php 
				if( $column_index == $columns ){
					$column_index = 0;
					echo '<div class="clear"></div>';
				}
				?>
	
			<?php endwhile; ?>
			
			<?php wp_reset_query(); ?>
			
			<div class="clear"></div>

			<?php 
			if( $pagination == 'on' ) {
				if(function_exists('atp_pagination')) { 
					echo atp_pagination(); 
				}
			}?>
			
			<?php else : ?>

			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'musicplay' ); ?></p>
	
			<?php get_search_form(); ?>
			
			<?php endif;?>

			</div><!-- .content-area -->

	</div><!-- inner -->
	</div><!-- #primary.pagemid -->

<?php get_footer(); ?>