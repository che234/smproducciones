<?php
/** 
 * The Header for our theme.
 * Includes the header.php template file. 
 */

get_header(); ?>
<?php global $default_date,$atp_downloadalltxt; ?>
	<div id="primary" class="pagemid">
	<div class="inner">
		<div class="content-area">
			<?php

			$lyrics_option	= get_option( 'atp_lyrics_option' ) ?  get_option( 'atp_lyrics_option' ) : 'lightbox' ;
			$audio_auto_play            = get_option( 'atp_audio_autoplay' ) ? get_option( 'atp_audio_autoplay' ) : 'true' ;
			$add_to_playlist_txt        = get_option('atp_add_to_playlist_txt') ? get_option('atp_add_to_playlist_txt') :  __('Add songs to playlist', 'musicplay');
			$tracks_count_txt           = get_option('atp_tracks_count_txt') ? get_option('atp_tracks_count_txt') :  __('Tracks', 'musicplay');
			$atp_song_like_enable       = get_option( 'atp_song_like_btn' ) ? get_option( 'atp_song_like_btn' ) : 'off' ;
			$atp_song_count_enable      = get_option( 'atp_song_count_btn' ) ? get_option( 'atp_song_count_btn' ) : 'off' ;
			$atp_download_all_enable      = get_option( 'atp_download_all_btn' ) ? get_option( 'atp_download_all_btn' ) : 'off' ;
			$atp_more_albums_genres      = get_option( 'atp_more_albums_genres' ) ? get_option( 'atp_more_albums_genres' ) : 'off' ;
			$atp_more_albums_label      = get_option( 'atp_more_albums_label' ) ? get_option( 'atp_more_albums_label' ) : 'off' ;

			
			if ( $lyrics_option === 'lightbox' ) {
				$lyrics_box = 'prettyPhoto';
			}else{
				$lyrics_box = 'lyrics';
			}
			
			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div <?php post_class( 'custompost-single' );?> id="post-<?php the_ID(); ?>">
				<?php
				
				$audio_upload               = get_post_meta( $post->ID, 'audio_upload', true );
				$audio_release_date         = get_post_meta( $post->ID, 'audio_release_date', true );
				$audio_label                = get_the_terms( $post->ID, 'label', true );
				$audio_genre_music          = get_the_terms( $post->ID, 'genres', true );
				$audio_video                = get_post_meta( $post->ID, 'audio_video', true );
				$audio_catalog_id           = get_post_meta( $post->ID, 'audio_catalog_id', true );
				$audio_soundcloud_code      = get_post_meta( $post->ID, 'audio_soundcloud_code', true );
				$audio_posttype_option      = get_post_meta( $post->ID, 'audio_posttype_option', true ) ? get_post_meta( $post->ID, 'audio_posttype_option', true ) : 'player' ;
				$audio_button_disable       = get_post_meta( $post->ID, 'audio_button_disable', true );
				$audio_buttons              = get_post_meta( $post->ID, 'audio_buttons', true );
				$audio_artist_name          = get_post_meta( $post->ID, 'audio_artist_name', true);
				$imagesrc                   = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );
				$music_image                = aq_resize( $imagesrc[0], '80', '80', true );
				$permalink                  = get_permalink( get_the_id() );
				$post_date                  = get_post_meta( get_the_id(), 'audio_release_date', true );
				$albumupload                = explode( ', ', $audio_upload );
				$img_alt_title              = get_the_title();
				$audio_artist               = array();
				$playlisttitle              = get_the_title( get_the_id() );
				$iva_album_custom_meta  	= get_post_meta( $post->ID, 'album_meta', true );

				// Fetch Artist(s) Linked to Album and its permalink
				if ( is_array( $audio_artist_name ) && count( $audio_artist_name ) > 0 ) {
					foreach( $audio_artist_name as $audio_artist_id){
						$artist_permalink     = get_permalink(  $audio_artist_id);
						$audio_artist[]= '<a href="'. $artist_permalink. '">'.get_the_title(  $audio_artist_id).'</a>';
					}
					$audio_artist_name = implode( ', ', $audio_artist );
				}
				
				// Fires the WordPress default date for post.
				if( $post_date != '' ) {
					if ( is_numeric( $post_date) ) {
						 $post_date = date_i18n( $default_date, $post_date );
					}
				}
				?>
			
				<div class="custompost_entry">
					<div class="custompost_details">
						<div class="col_fourth album_bio">
							<?php if( has_post_thumbnail() ) { ?>
							<div class="custompost_thumb port_img">
								<?php
								echo '<figure>'. atp_resize($post->ID,'','470','470','', $img_alt_title  ). '</figure>';
								// Light Box enable/disable
								$album_lightbox = get_option('atp_album_lightbox');
								if ( $album_lightbox == 'enable' ) {
									echo '<div class="hover_type">';
									echo '<a data-rel="prettyPhoto" class="hoverimage" href="'.$imagesrc[0].'" title="'. get_the_title().'"></a>';
									echo '</div>';
									echo '<span class="imgoverlay"></span>';
								}
								?>
							</div>
							<?php } ?>
						
							<div class="album-details">
								<?php

								if ( $audio_release_date != '' ) {
									echo '<div class="album-meta">';
									echo atp_localize($atp_album_releasedate_txt,'<span class="album-label">','</span>');
									echo '<div class="album-mdata">'. $post_date .'</div>';
									echo '</div>';
								}
								
								if ( $audio_label != '' ) {
									echo '<div class="album-meta">';
									echo atp_localize($atp_album_label_txt,'<span class="album-label">','</span>');
									echo '<div class="album-mdata">'. get_the_term_list( $post->ID, 'label', '','' ) .'</div>';
									echo '</div>';
								}
			
								if ( $audio_catalog_id != '' ) {
									echo '<div class="album-meta">';
									echo atp_localize($atp_album_catid_txt,'<span class="album-label">','</span>');
									echo '<div class="album-mdata">'. $audio_catalog_id .'</div>';
									echo '</div>';
								}
								
								if ( $audio_genre_music != '' ) {
									echo '<div class="album-meta">';
									echo atp_localize($atp_album_genre_txt,'<span class="album-label">','</span>');
									echo '<div class="album-mdata">'. get_the_term_list( $post->ID, 'genres', '','' ) .'</div>';
									echo '</div>';
								}
								if( $iva_album_custom_meta !='' ) {
									foreach ( $iva_album_custom_meta as $albumcustom_meta ) {	
										echo '<div class="album-meta"><span class="album-label">'. $albumcustom_meta['label'] .'</span>';
										echo '<div class="album-mdata">'. $albumcustom_meta['value'].'</div>';
										echo '</div>';
						  			}
						  		}
								?>

							</div><!-- .album-details -->

							<?php
							// Get Share - Sociables
							get_template_part('musicband/share','link');
							?>

						</div><!-- .col_fourth -->
					
						<div class="col_three_fourth album_contents end">
							<?php
								echo '<div class="iva-np-headwrap">';				
								echo '<div class="iva-np-title"><h2 class="album-title">'.get_the_title() .'</h2></div>';
								echo '<div class="iva-np-navs">';
								echo '<div class="iva-np-pagination">';
								echo previous_post_link( $link = '%link','<i class="fa fa-angle-left"></i>');
								echo next_post_link( $link = '%link', '<i class="fa fa-angle-right"></i>');
								echo '</div>';
								echo '</div>';
								echo '</div>';
							?>
							<div class="arts_name"><?php echo $audio_artist_name; ?></div>
							<?php
							if ( $audio_button_disable != 'on' ) {
								if ( $audio_buttons != '' ){
									echo '<div class="buttons-wrap">';
									foreach ($audio_buttons as $buttons){ ?>
										<a href="<?php echo $buttons['buttonurl'] ?>" target="_blank" <?php if($buttons['buttoncolor']!=''){?> style="background-color:<?php echo $buttons['buttoncolor'] ?>"<?php } ?> class="btn small silver flat no-ajaxy"><span><?php echo  $buttons['buttonname'];?></span></a>
						    				<?php	
									}
								 
									echo '</div>';
								}
							} 
							?>
							<?php 
							if($audio_posttype_option == 'player'){
								$audiolist=get_post_meta($post->ID,'audio_upload',true) ? get_post_meta($post->ID,'audio_upload',true) :'';

								$audio_list=array();
								$audio_list=explode(',',$audiolist);$count=count($audio_list);
								$audio_id=array();
								$player_download = false;
								foreach ($audio_list as $attachment_id) {
									if($attachment->all_download == '1' && $attachment->download != '') { $player_download = true;}
									$attachment = get_post( $attachment_id );
									if(count($attachment) >0 ){
										$audio_id[]=$attachment->ID;
									}
									}
									
									if(!empty($audio_id)){
									$count=count($audio_id);
									}
								/*** 
								 * Album Details for MusicPlayer
								 * permalink, title, release date, audio playlist
								 */
								
								?>
								<div class="mp3list-head">
									<?php

									// Total tracks
									if($audiolist != '') { 
										if($count > 0) { 
											echo '<span class="tracks-count"><i class="fa fa-music fa-fw"></i>&nbsp;'.$count.'&nbsp;'. $tracks_count_txt .'</span>';
										}
									}

									// Add tracks to playlist
									if(get_option('atp_playlist_enable') == 'on' ) {  ?>
									<span class="add-to-playlist-btn"><i class="fa fa-bars fa-fw"></i>&nbsp;<a href="#" class="fap-add-playlist alldata-playlist" data-enqueue="no" data-playlist="any-playlist"><?php echo $add_to_playlist_txt; ?></a></span>
									<?php }
									
									// Album Likes
									$post_likes_option = get_option('atp_like_btn');
									if ( $post_likes_option != 'on' ){
										if ( function_exists( 'iva_get_post_like' ) ) {
											echo iva_get_post_like( $post->ID );
										}
									}
									if( $player_download == true && $atp_download_all_enable != 'on' ){ ?>	
								     <span class="add-to-playlist-btn">
										<i class="fa fa-download fa-fw"></i>&nbsp;
										<a href="#" class="fap-download-album alldata-download-album" data-albumid='<?php echo $post->ID; ?>' ><?php echo $atp_downloadalltxt; ?></a>
	                                 </span>
									<?php } ?>
								</div>

								<ul data-playlist="any-playlist" class="album-playlist">
								<?php
								if($audiolist !='') {
								if($count > 0) { 
									$i = 1;
									foreach($audio_list as $attachment_id) {
										$attachment = get_post( $attachment_id );
										$trackcount ='0';
										if(count($attachment) > 0) {
											$attachment_title=$attachment->post_title;
											$attachment_caption=$attachment->post_excerpt;

											$lyricsdesc=$attachment->post_content;
											$buy=$attachment->buy;
											$download= $attachment->download;
											if(  isset( $attachment->trackcount ) ){
												$trackcount = $attachment->trackcount;
											
											}

											$trackCountlist = isset($trackcount) ?  $trackcount :'0';
											$download_player_count = get_post_meta( $attachment->ID,'download_count',true)?get_post_meta( $attachment->ID,'download_count',true):'0';
											 ?>
											<li><a class="fap-single-track no-ajaxy" data-id="<?php  echo $attachment->ID;?>" data-enqueue="no" data-meta="#single-player-meta" href="<?php  echo $attachment->guid;?>"  title="<?php echo $attachment_title; ?>"  rel="<?php echo $imagesrc[0]; ?>">
											<i class="ivaplay"></i></a>
											<?php echo $attachment_title; ?>
											<div class="mp3options">
											<?php
											// song count enable/disable since 3.2.5
											if( $atp_song_count_enable != 'on') {
												echo '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachment->ID.'">'.$trackCountlist.'</span></span>';
											}
											// song like enable/disable since 3.2.5
											if( $atp_song_like_enable != 'on' ){										
												echo iva_track_like($attachment->ID);
											}
											if( $buy !='') { 
												echo '<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
											}
											if( $download != '' && $download != '#' ) { 
												echo '<span class="mp3o download iva_music_download" data-id="'.$attachment->ID.'" data-download="'. iva_get_download($download).'"><i class="fa fa-download fa-lg fa-fw"></i><span class="iva_dcount">'.(int)$download_player_count.'</span></span>';
											}
											if( $lyricsdesc !=''){ 
												echo '<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $attachment_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
											}
											?>
											</div>
											<?php if( $lyricsdesc !=''){ ?>
												<div id="<?php echo preg_replace('/\s+/', '-', $attachment_title) ; ?>" class="lyricdesc">
													<?php 
													echo '<p><strong>'.$attachment_title.'</strong></p>';
													//echo wpautop($attachment->post_content); 
													$content = apply_filters('the_content', $attachment->post_content); 
													echo $content;
													?>
												</div>
												<?php } ?>
											<?php }
										echo'</li>';
										}
									}
								}
								?>
								</ul>
								
								<?php 
								echo '<div id="single-player-meta">'.$audio_artist_name.' &mdash; <a href="'.$permalink.'">'.$playlisttitle.'</a></div>'; 
								
								// Fetch Soundcloud if assign in meta
							}elseif($audio_posttype_option == 'externalmp3'){
							$audiolist=get_post_meta($post->ID,'audio_mp3',true) ? get_post_meta($post->ID,'audio_mp3',true) :'';
							$count=count($audiolist);
							$mp3_download = false;
							foreach ($audiolist as $mp3_all_list) {
								$alldownload = $mp3_all_list['alldownload'];
								$download = $mp3_all_list['download'];								
								if($alldownload == 'on' && $download != '' ) { $mp3_download = true; }								
							}
							?>
							<div class="mp3list-head">
							<?php
							// Total tracks
									if($audiolist != '') { 
										if($count > 0) { 
											echo '<span class="tracks-count"><i class="fa fa-music fa-fw"></i>&nbsp;'.$count.'&nbsp;'. $tracks_count_txt .'</span>';
										}
									}

									// Add tracks to playlist
									if(get_option('atp_playlist_enable') == 'on' ) {  ?>
									<span class="add-to-playlist-btn"><i class="fa fa-bars fa-fw"></i>&nbsp;<a href="#" class="fap-add-playlist alldata-playlist" data-enqueue="no" data-playlist="any-playlist"><?php echo $add_to_playlist_txt; ?></a></span>
									<?php }
									
									// Album Likes
									$post_likes_option = get_option('atp_like_btn');
									if ( $post_likes_option != 'on' ){
										if ( function_exists( 'iva_get_post_like' ) ) {
											echo iva_get_post_like( $post->ID );
										}
									}

									if( $mp3_download == true && $atp_download_all_enable != 'on' ) {	?>
										<span class="add-to-playlist-btn">
											<i class="fa fa-download fa-fw"></i>&nbsp;
											<a href="#" class="fap-download-album alldata-download-album" data-albumid='<?php echo $post->ID; ?>' ><?php echo $atp_downloadalltxt; ?></a>
		                                </span>
									<?php } ?>
							</div>
							<ul data-playlist="any-playlist" class="album-playlist">
								<?php
								if($audiolist !='') {
								
								if($count > 0) {
									$i = 1;
									foreach($audiolist as $mp3_list) {
										//$attachment = get_post( $attachment_id );
										$mp3_title=$mp3_list['mp3title'];
										$lyricsdesc=$mp3_list['lyrics'];
										$buy=$mp3_list['buylink'];
										$download=$mp3_list['download'];
										$mp3id  =  $mp3_list['mp3id'];
										$trackcount = get_post_meta($mp3id,'trackcount',true) ? get_post_meta($mp3id,'trackcount',true) : '0';
										$download_mp3_count = get_post_meta( $mp3id,'download_count',true)?get_post_meta( $mp3id,'download_count',true):'0';
										 ?>
										<li><a class="fap-single-track no-ajaxy" data-autoenqueue="no" data-id="<?php  echo $mp3id; ?>"  data-meta="#single-player-meta" href="<?php  echo esc_attr($mp3_list['mp3url']);?>"  title="<?php echo $mp3_title; ?>"  rel="<?php echo $imagesrc[0]; ?>">
										<i class="ivaplay"></i></a><?php echo $mp3_title; ?>
										<div class="mp3options">
										<?php
										// song count enable/disable since 3.2.5
										if( $atp_song_count_enable != 'on') {
											echo '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $mp3id.'">'.$trackcount.'</span></span>';
										}
										// song like enable/disable since 3.2.5
										if( $atp_song_like_enable != 'on' ){	
											echo iva_track_like($mp3id);
										}
										if($buy !='') { 
											echo '<span class="mp3o buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
										}
										if($download !='') { 
											echo '<span class="mp3o download music_download" data-id="'.$mp3id.'" data-download="'. iva_get_download( $download ).'"><i class="fa fa-download fa-lg"></i><span class="iva_dcount">'.(int)$download_mp3_count.'</span></span>';
										}
										if($lyricsdesc !=''){ 
											echo '<span class="mp3o lyrics"><a href="#'. preg_replace('/\s+/', '-', $mp3_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
										}
										?>
										</div>
										<?php if($lyricsdesc !=''){ ?>
											<div id="<?php echo preg_replace('/\s+/', '-', $mp3_title) ; ?>" class="lyricdesc">
												<?php 
												echo '<p><strong>'.$mp3_title.'</strong></p>';
												$content = apply_filters('the_content', $mp3_list['lyrics']); 
												echo $content;
												?>
											</div>
											<?php } ?>
										<?php }
									echo'</li>';
									}
								}
								?>
								</ul>
								
							<?php
							// External MP3
							echo '<div id="single-player-meta">'.$audio_artist_name.' &mdash; <a href="'.$permalink.'">'.$playlisttitle.'</a></div>'; 
							}elseif($audio_posttype_option == 'soundcloud') {
								$audio_soundcloud_title = get_post_meta($post->ID,'audio_soundcloud_title',true);
								$audio_soundcloud_title_label = $audio_soundcloud_title ? $audio_soundcloud_title :'Title Label';
								$audio_soundcloud_id = get_post_meta($post->ID,'audio_soundcloud_id',true);
								$trackcount = get_post_meta($audio_soundcloud_id,'trackcount',true);
								$audio_soundcloud_url = get_post_meta($post->ID,'audio_soundcloud_url',true); ?>
								<div class="mp3list-head">
								<?php if($audio_soundcloud_url !='') {  echo '<span class="tracks-count"><i class="fa fa-music fa-fw"></i>&nbsp; 1 &nbsp;'. $tracks_count_txt .'</span>';  } 
								// Add tracks to playlist
									if(get_option('atp_playlist_enable') == 'on' ) {  ?>
									<span class="add-to-playlist-btn"><i class="fa fa-bars fa-fw"></i>&nbsp;<a href="#" class="fap-add-playlist alldata-playlist" data-enqueue="no" data-playlist="any-playlist"><?php echo $add_to_playlist_txt; ?></a></span>
								<?php }
									$post_likes_option = get_option('atp_like_btn');
									if ( $post_likes_option != 'on' ){
										if ( function_exists( 'iva_get_post_like' ) ) {
											echo iva_get_post_like( $post->ID );
										}
									}
								
								?>
								</div>
								<ul class="album-playlist"   data-playlist="any-playlist">
									<li><a href="<?php echo $audio_soundcloud_url; ?>"  data-id="<?php  echo $audio_soundcloud_id;?>" data-autoenqueue="no" class="fap-single-track no-ajaxy"><i class="ivaplay"></i></a><?php echo $audio_soundcloud_title; ?>
									<div class="mp3options">
											<?php
											// song count enable/disable since 3.2.5
											if( $atp_song_count_enable != 'on') {
												echo '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $audio_soundcloud_id.'">'.$trackcount.'</span></span>';
											}
											// song like enable/disable since 3.2.5
											if( $atp_song_like_enable != 'on' ){
												echo iva_track_like($audio_soundcloud_id);
											}
								?>
								</div>
								</li>
								</ul>
								<?php
									
							}elseif($audio_posttype_option == 'medialibrary'){
								$audiolist= get_post_meta($post->ID,'audio_medialibrary',false) ? get_post_meta($post->ID,'audio_medialibrary',false) :'';
								$count = count($audiolist);
								$library_download = false;
								foreach ($audiolist as $attachment_id) {
									$attachment = get_post( $attachment_id );									
									if($attachment->all_download == '1' && $attachment->download != '') { $library_download = true;}
									if(count($attachment) >0 ){
										$audio_id[]=$attachment->ID;
									}
								}

								?>
								<div class="mp3list-head">
								<?php
								// Total tracks
										if($audiolist != '') { 
											if($count > 0) { 
												echo '<span class="tracks-count"><i class="fa fa-music fa-fw"></i>&nbsp;'.$count.'&nbsp;'. $tracks_count_txt .'</span>';
											}
										}
	
										// Add tracks to playlist
										if(get_option('atp_playlist_enable') == 'on' ) {  ?>
										<span class="add-to-playlist-btn"><i class="fa fa-bars fa-fw"></i>&nbsp;<a href="#" class="fap-add-playlist alldata-playlist" data-enqueue="no" data-playlist="any-playlist"><?php echo $add_to_playlist_txt; ?></a></span>
										<?php }
										
										// Album Likes
										$post_likes_option = get_option('atp_like_btn');
										if ( $post_likes_option != 'on' ){
											if ( function_exists( 'iva_get_post_like' ) ) {
												echo iva_get_post_like( $post->ID );
											}
										}
								if($library_download == true && $atp_download_all_enable != 'on' ){ ?>
									<span class="add-to-playlist-btn">
										<i class="fa fa-download fa-fw"></i>&nbsp;
										<a href="#" class="fap-download-album alldata-download-album" data-albumid='<?php echo $post->ID; ?>' ><?php echo $atp_downloadalltxt; ?></a>
	                                 </span>
								<?php }	?>
								</div>
								<?php
								echo '<ul data-playlist="any-playlist" class="album-playlist">';
									if($audiolist !='') {
										if($count > 0) {
											foreach($audiolist as $mediaupload_list) {
												$trackcount ='0';
												$attachment = get_post( $mediaupload_list );
												$attachment_caption = $attachment->post_excerpt;
												$lyricsdesc = $attachment->post_content;
												$buy = $attachment->buy;
												$download = $attachment->download;
												$media_upload_title = $attachment->post_title;
												if (  isset( $attachment->trackcount ) ) {
													$trackcount= $attachment->trackcount;
												}
												$download_medialibrary_count = get_post_meta( $attachment->ID,'download_count',true)?get_post_meta( $mp3id,'download_count',true):'0';
												if($media_upload_title) {
												 	?>
												 	<li><a class="fap-single-track no-ajaxy" data-id="<?php  echo $attachment->ID;?>" data-autoenqueue="no"  data-meta="#single-player-meta" href="<?php  echo $attachment->guid;?>"  title="<?php echo $media_upload_title; ?>"  rel="<?php echo $imagesrc[0]; ?>">
													<i class="ivaplay"></i></a><?php echo $media_upload_title; ?>
													<div class="mp3options">
													<?php
													// song count enable/disable since 3.2.5
													if( $atp_song_count_enable != 'on') {
														echo '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'. $attachment->ID.'">'.$trackcount.'</span></span>';
													}
													// song like enable/disable since 3.2.5
													if( $atp_song_like_enable != 'on' ){
	
														echo iva_track_like($attachment->ID);

													}
													if($buy !='') { 
														echo '<span class="buy"><a href="'. esc_url($buy) .'" target="_blank"><i class="fa fa-shopping-cart"></i></a></span>';
													}
													if($download !='') { 
														echo '<span class="download music_download" data-id="'.$attachment->ID.'" data-download="'.  iva_get_download( $download ).'"><i class="fa fa-download fa-lg"></i><span class="iva_dcount">'.(int)$download_medialibrary_count.'</span></span>';
													}
													if($lyricsdesc !=''){ 
														echo '<span class="lyrics"><a href="#'. preg_replace('/\s+/', '-', $media_upload_title) .'" data-rel="'. $lyrics_box .'"><i class="fa fa-file-text"></i></a></span>';
													}
													?>
													</div>
													<?php if($lyricsdesc !=''){ ?>
														<div id="<?php echo preg_replace('/\s+/', '-', $media_upload_title) ; ?>" class="lyricdesc">
															<?php 
															echo '<p><strong>'.$media_upload_title.'</strong></p>';
															//echo wpautop($attachment->post_content); 
															$content = apply_filters('the_content', $attachment->post_content); 
															echo $content;
															?>
														</div>
													<?php } ?>
													</li><?php 
												}
											}	 

										}
									}
									echo '</ul>';	
									// Media Upload
									echo '<div id="single-player-meta">'.$audio_artist_name.' &mdash; <a href="'.$permalink.'">'.$playlisttitle.'</a></div>'; 
							} ?>


							<?php the_content(); ?>	
							<?php if (  $audio_video != '' ) {  echo $audio_video;  } ?>	
						</div><!-- .col_three_fourth -->
						
					</div><!-- .custompost_details -->
				</div><!-- .custompost_entry -->
				
			</div><!-- #post-<?php the_ID();?> -->
			<?php endwhile; ?>

			<?php else: ?>
			<?php '<p>'.__('Sorry, no projects matched your criteria.', 'musicplay').'</p>';?>
			<?php endif; ?>
			
			<?php edit_post_link(__('Edit', 'musicplay'), '<span class="edit-link">', '</span>'); ?>

			<?php 
			$comments = get_option('atp_album_comments');
			if ( $comments == 'enable' ) {
				comments_template( '', true ); 
			}?>
			
			</div><!-- .content-area -->
			<?php if ( atp_generator( 'sidebaroption', $post->ID ) != "fullwidth" ){ get_sidebar(); } ?>
	</div><!-- inner -->
	</div><!-- #primary.pagemid -->

	<?php 
	if($audio_label != '' &&  $atp_more_albums_label != 'on') {
		get_template_part('musicband/albums/audio','label');
	}

	if($audio_genre_music !='' &&  $atp_more_albums_genres != 'on') {
		get_template_part('musicband/albums/audio','genres');
	}
	?>


<?php get_footer(); ?>