<?php
// Artists results
	global $error,$keyword;
	
	$search_pagination_option 	=  get_option('atp_search_pagination') ? get_option('atp_search_pagination') :''; 
	$ar_posts_perpage  			=  get_option('atp_search_artist_limits') ? get_option('atp_search_artist_limits') :'2';
	
	// Pagination
	$paged3 = isset( $_GET['paged3'] ) ? (int) $_GET['paged3'] : 1;
	$paged4 = isset( $_GET['paged4'] ) ? (int) $_GET['paged4'] : 1;

	// Djmix title search query
	add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );
	function title_like_posts_where( $where, &$wp_query ) {
		global $wpdb;
		if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
			$search_orderby_s = esc_sql( $wpdb->esc_like( $post_title_like ) );
			$where .= "AND $wpdb->posts.post_title LIKE '%{$search_orderby_s}%'";
		}
		return $where;
	}
	// Artists Search Results Output
	$artist_args = array(
		'post_type' 		=> 'artists',
		'post_title_like' 	=> $keyword,
		'posts_per_page'	=> $ar_posts_perpage,
		'paged' 			=> $paged3,
		'orderby'		 	=>	'date',
		'order'			 	=>	'ASC'
	);
	$iva_artist_query = new WP_Query( $artist_args );

	$column_index = 0; $columns = 6;

	if( $columns == '6' ) { $class = 'col_sixth'; }
	if( $columns == '5' ) { $class = 'col_fifth'; }
	if( $columns == '4' ) { $class = 'col_fourth'; }
	if( $columns == '3' ) { $class = 'col_third'; }

	//Full Width Artist Image Sizes
	if( $columns == '6' ) { $width='470'; $height = '470' ; }
	if( $columns == '5' ) { $width='470'; $height = '470' ; }
	if( $columns == '4' ) { $width='470'; $height = '470' ; }
	if( $columns == '3' ) { $width='470'; $height = '470' ; }

	if ( $iva_artist_query->have_posts() ) : 

		$artists_title = get_option('atp_artist_slug') ? get_option('atp_artist_slug'):__('Artists','musicplay');

		echo '<div class="artists-search inner">';
		echo '<h2>'.$artists_title.'</h2>'; 
		
		while ( $iva_artist_query->have_posts() ) : $iva_artist_query->the_post();
		
			$artist_genres = get_post_meta( $post->ID, 'artist_genres', true );
			$img_alt_title = get_the_title();
			
			$column_index++;
			$last = ( $column_index == $columns && $columns != 1 ) ? 'end ' : '';

		echo '<div class="artist-list  '.$class. ' '.$last.'">';
		echo '<div class="custompost_entry">';
		
		if( has_post_thumbnail() ){
			echo '<div class="custompost_thumb port_img">';
			echo '<figure>'. atp_resize( $post->ID, '', $width, $height, '', $img_alt_title ) .'</figure>'; 
			echo '<div class="hover_type">';
			echo '<a class="hoverartist"  href="' .get_permalink(). '" title="' . get_the_title() . '"></a>';
			echo '</div>';
			echo '</div>';
		} 

		echo '<div class="artist-desc">';
		echo '<h2 class="entry-title"><a href="'.get_the_permalink().'" rel="bookmark" title="'.esc_attr( get_the_title() ).'">'.get_the_title().'</a></h2>';
		if( $artist_genres !='' ) { echo '<span>'. $artist_genres.'</span>'; }
		echo'</div>';

		echo '</div>';// .custompost_entry
		echo '</div>';// .artist-post
		
		
	if( $column_index == $columns ){
		$column_index = 0;
		echo '<div class="clear"></div>';
	}
	$error = false;
	endwhile; 

	echo '<div class="clear"></div>';

	// Artist pagination
	$ar_pagenum_link = html_entity_decode( get_pagenum_link() );
	$ar_format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $ar_pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$ar_format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'paged3/%#%', 'paged3' ) : '?paged3=%#%';

	 $ar_page_args = array(
					'base' 		=> add_query_arg( 'paged3', '%#%' ),
					'format'  	=> $ar_format,
					'current' 	=> $paged3,
					'prev_next' => false,
					'total'   	=> $iva_artist_query->max_num_pages,
					'add_args'  => array( 'paged4' => $paged4 )
				);
	if( $search_pagination_option == 'on'){
		echo '<div class="pagination">'.paginate_links( $ar_page_args ).'</div>';
	}
	echo '</div>';// .artists-search
	$error = false;
	wp_reset_query();
	else :
	endif;
	// Artist Search Ends
?>