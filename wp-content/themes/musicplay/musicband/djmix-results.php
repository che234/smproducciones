<?php
	global $error,$keyword,$default_date;
	$track_posts_perpage 		= get_option('atp_search_track_limits') ? get_option('atp_search_track_limits'):'2';
	$search_pagination_option 	= get_option('atp_search_pagination') ? get_option('atp_search_pagination') :'';	
	$dj_posts_perpage 			= get_option('atp_search_djmix_limits') ? get_option('atp_search_djmix_limits') :'-1';

	$dj_attachment_posts = $djmix_ids = $dj_att_track_ids = array();	
	$results = $dj_tracks_pagination ='';	
			
	// Pagination
	if ( get_query_var('paged') ) {
		$paged = get_query_var('paged');
	} elseif ( get_query_var('page') ) {
		$paged = get_query_var('page');
	} else {
		$paged = 1;  
	}
	
	$dj_attachements_sql = "SELECT * FROM $wpdb->posts WHERE post_title LIKE '%$keyword%'
		OR post_content LIKE '%$keyword%'
		AND ( post_type='attachement' OR post_mime_type='audio/mpeg') 
		AND ( post_status = 'inherit' OR post_status = 'publish')";
			
	$dj_attachements_results = $wpdb->get_results( $dj_attachements_sql );
	
	if( count( $dj_attachements_results ) > 0 &&  !empty( $dj_attachements_results ) ) {

		$iva_dj_track_ids = array();
		foreach( $dj_attachements_results as $result ) {
			$dj_att_track_ids[] = $result->ID;
		}
					
		// Track ids from media
		$dj_att_track_ids = array_unique( $dj_att_track_ids );
		
		// Fetching track ids from posttype 'djmix'
		$dj_track_args = array( 'post_type'=> 'djmix','showposts'=>'-1');

		$iva_dj_track_query = new WP_Query( $dj_track_args );
		
		if ( $iva_dj_track_query->have_posts() ) : 
		
			while ( $iva_dj_track_query->have_posts() ) : $iva_dj_track_query->the_post();

			$dj_audio_posttype_option	= get_post_meta( $post->ID,'audio_posttype_option',true );

			if( $dj_audio_posttype_option == 'player' ){
			
				$dj_upload_id  = get_post_meta( $post->ID, 'djmix_upload_mix', false );
				
				// Djmix
				if( $dj_upload_id!=''){
					foreach( $dj_att_track_ids as $trackid ){
						if( in_array( $trackid, $dj_upload_id ) ) {
							$iva_dj_track_ids[] = $trackid;
						}
					}
				}
			}
			endwhile; 
			wp_reset_query();
			else :
		endif;
		
		// Track ids from posttype 'djmix'
		$iva_dj_track_ids = array_unique( $iva_dj_track_ids );
		
		// var_dump( $iva_dj_track_ids );
			
		foreach( $iva_dj_track_ids as $dj_attachment_id ) {
			$dj_attachment 			= get_post( $dj_attachment_id );
			$dj_attachment_title 	= $dj_attachment->post_title;
			$dj_attachment_posts[] 	= $dj_attachment->post_parent;
		}
					
		// Fetching Post ids those having attachements
		$dj_attachment_posts = array_unique( $dj_attachment_posts );

	}
		
	// djmix Meta Query
	$djmix_meta_args =  array(
		'post_type' => 'djmix',
		'posts_per_page' => '-1',
		'meta_query'=> array(
		'relation'  => 'OR',
			array(
				'key'     => 'audio_soundcloud_title',
				'value'   => $keyword,
				'compare' => 'LIKE'
			),
			array(
				'key'     => 'djmix_externalmp3title',
				'value'   => $keyword,
				'compare' => 'LIKE'
			),
		),
	);

	$dj_tracks_output = '';
	
	$djmix_meta_query = new WP_Query( $djmix_meta_args );
					
	if ( $djmix_meta_query->have_posts() ) : 
	
		$dj_soundcloudcount = $dj_externalmp3count = $dj_externalmp3count = 0; 
		
		while ( $djmix_meta_query->have_posts() ) : $djmix_meta_query->the_post();

		$dj_audio_posttype_option	= get_post_meta( $post->ID,'audio_posttype_option',true );
		$dj_soundcloud_title 		= get_post_meta( $post->ID,'audio_soundcloud_title',true);
		$dj_soundcloud_title_label 	= $dj_soundcloud_title ? $dj_soundcloud_title :__('Title Label','musicplay');
		$dj_soundcloud_url 			= get_post_meta( $post->ID,'audio_soundcloud_url',true);
		$dj_externalmp3title		= get_post_meta( $post->ID,'djmix_externalmp3title',true ) ? get_post_meta( $post->ID,'djmix_externalmp3title',true ) :'';
			
		// Fetching album posts having soundcloud urls and external mp3 urls
		$djmix_ids[] = $post->ID;
					
		// Soundcloud
		if ( $dj_audio_posttype_option  == 'soundcloud' ) {
			if( $dj_soundcloud_url != '' ){
				if ( preg_match("/$keyword/i", $dj_soundcloud_title )) {
					$dj_externalmp3count++;
					$djmix_ids[] = $post->ID;
				}
			}
		}
		
		// externalmp3
		if ( $dj_audio_posttype_option  == 'externalmp3' ) {
			if( $dj_externalmp3title	!=''){
				if ( preg_match("/$keyword/i", $dj_externalmp3title)) {
					$dj_externalmp3count++;
					$djmix_ids[] = $post->ID;
				}
			}
		}
		endwhile; 
		wp_reset_query();
		else :
	endif;
					
	
	$djmix_ids = array_unique( $djmix_ids );
	$iva_djmix_postids = array_merge( $dj_attachment_posts,$djmix_ids );
	$iva_djmix_postids = array_unique( $iva_djmix_postids );
	
	// var_dump($dj_attachment_posts);
	
	global $dj_externalmp3count,$dj_soundcloudcount;
	
	$dj_total_postscount = count( $iva_djmix_postids );

	if( $iva_djmix_postids ) {
		// Query to fetch all posts that are having attachements
		$djmix_meta_args =  array(  
			'post_type' => 'djmix',
			'posts_per_page' => '-1',
			'post__in'  => $iva_djmix_postids,
		);
			 
		$djmix_meta_query = new WP_Query( $djmix_meta_args );
	
		if ( $djmix_meta_query->have_posts() ) : 
	
			$error = false;
		
			$dj_pageCount = ceil( $dj_total_postscount / $track_posts_perpage );
			$dj_current_page = isset( $_GET['page'] ) ? intval( $_GET['page'] ) :'';
			
			if ( get_query_var('paged') ) {
				$dj_current_page = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
				$dj_current_page = get_query_var('page');
			} else {
				$dj_current_page = 1;  
			}
			
		
			if ( empty( $dj_current_page ) || $dj_current_page<=0 ) $dj_current_page = 1;
				
				$dj_max_pagenum = $dj_current_page * $track_posts_perpage;
				$dj_min_pagenum = ( $dj_current_page-1 ) * $track_posts_perpage;
		
			if( $dj_pageCount > 1 ){
				$dj_pagenum_link 	= html_entity_decode( get_pagenum_link() );
				$dj_page_link 		= $dj_pagenum_link;
				$dj_page_link_perma = true;
				
				if ( strpos( $dj_page_link, '?' )!== false )
				
				$dj_page_link_perma = false;
				
				$dj_tracks_pagination = '<div class="clear"></div><div class="pagination">';
				
				for ( $p =1; $p<= $dj_pageCount; $p++){
					if ( $p == $dj_current_page )
						$dj_tracks_pagination .= '<span class="current"> '.$p.' </span>';
					else
						$dj_tracks_pagination .= '<a class="inactive" href="'.$dj_page_link. ( ( $dj_page_link_perma?'?':'&amp;') ). 'page='.$p.'">'.$p.'</a>';
				}
				
				$dj_tracks_pagination .= '</div>';
			}// Tracks pagination 
			
		$i = 0;
	
		$djmix_start = 1;

		while ( $djmix_meta_query->have_posts() ) : $djmix_meta_query->the_post();
		
			// Djmix and album Post meta						
			$dj_post_title 				= get_the_title( $post->ID );
			$dj_soundcloud_title 		= get_post_meta( $post->ID,'audio_soundcloud_title',true);
			$dj_soundcloud_title_label 	= $dj_soundcloud_title ? $dj_soundcloud_title :__('Title Label','musicplay');
			$dj_soundcloud_url 			= get_post_meta( $post->ID,'audio_soundcloud_url',true);
			$dj_audio_posttype_option	= get_post_meta( $post->ID,'audio_posttype_option',true );
			$dj_genre		 			= get_post_meta( $post->ID,'djmix_genre', true );	
			$dj_post_date 	 			= get_post_meta( $post->ID,'djmix_release_date',true);		
			$dj_mp3_title 	 			= get_post_meta( $post->ID,'djmix_externalmp3title',true);		
			$dj_mp3_url	 	 			= get_post_meta( $post->ID,'djmix_externalmp3',true);	
			$dj_upload_id  	 			= get_post_meta( $post->ID,'djmix_upload_mix', false );
			$dj_imagesrc                = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false, '' );
			$dj_music_image 			= aq_resize( $dj_imagesrc[0], '80', '80', true );
			
			if( $dj_post_date !='') { 
				if( is_numeric( $dj_post_date ) ){
					 $dj_post_date = date_i18n( $default_date,$dj_post_date );
				}	
			}
			
			$dj_attachment_url = '';
		
			// var_dump( $dj_attachment_posts );
			
			// Player list
			if ( $dj_audio_posttype_option  == 'player' && in_array( $post->ID,$dj_attachment_posts) ) {
		
				$player_unique_id = $djmix_start.'player'.rand( 1,10 );
				
				//Djmix
				foreach( $dj_att_track_ids as $trackid ){
					
					if( in_array( $trackid,$dj_upload_id ) ){
					
						if ( $i >= $dj_min_pagenum && $i < $dj_max_pagenum ) {
					
						$iva_dj_track_ids 		= $trackid;
						$dj_attachment    		= get_post( $iva_dj_track_ids );
						$dj_attachment_title 	= $dj_attachment->post_title;
						$dj_attachment_url 		= $dj_attachment->guid;
						$dj_trackcount=get_post_meta($dj_attachment->ID,'trackcount',true) ? get_post_meta($dj_attachment->ID,'trackcount',true) :'0';
						
						// Djmix 
						$dj_tracks_output.= '<div class="tracklist-Row">';
						$dj_tracks_output.= '<div class="tracklist-details">';
						$dj_tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$dj_attachment->ID.'" data-meta="#single-player-meta'.$player_unique_id.'" href="'.$dj_attachment_url.'"  title="'.$dj_attachment_title.'"  rel="'.$dj_music_image.'"><i class="ivaplay"></i> </a>'.$dj_attachment_title.'<br>';
						$dj_tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'.$dj_attachment->ID.'">'.$dj_trackcount.'</span></span>';
						$dj_tracks_output.= '</div>';
						$dj_tracks_output.= '<div class="tracklist-album tracklist-col">';
						$dj_tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
						$dj_tracks_output.= '<strong>'.$dj_post_title.'</strong></div>';
						$dj_tracks_output.= '</div>';//.tracklist-details
						$dj_tracks_output.= '</div>';//.tracklist-Row
						$dj_tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$player_unique_id.'">
											<a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a>
										<div>'.$dj_genre.' | '.$dj_post_date.'</div></div>'; 
						}
						$i++;	
					}
				}
				
				$error = false;
			}
			
			// Soundcloud list
			if ( $dj_audio_posttype_option  == 'soundcloud' ) {
				
				if( $dj_soundcloud_url !=''){
					
					// Djmix
					if ( preg_match("/$keyword/i", $dj_soundcloud_title )) {
						
						$dj_soundcloud_unique_id = $djmix_start.'sc'.rand( 100,200 );
						$audio_soundcloud_id 	= $post->ID;
						$trackcount 			= get_post_meta( $post->ID,'trackcount',true );
						$soundtrackcount 		= isset( $trackcount )  ?  $trackcount :'0';
						
						if ( $i >= $dj_min_pagenum && $i < $dj_max_pagenum ) {
							// Djmix
							$dj_tracks_output.= '<div class="tracklist-Row">';
							$dj_tracks_output.= '<div class="tracklist-details">';
							$dj_tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy" data-id="'.$audio_soundcloud_id.'"  data-meta="#single-player-meta'.$dj_soundcloud_unique_id.'"  href="'.$dj_soundcloud_url.'"  title=""  rel="'.$dj_music_image.'"><i class="ivaplay"></i> </a>'.$dj_soundcloud_title.'<br>';
							$dj_tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><sapn class="ivaplay-'.$audio_soundcloud_id.'">'.$soundtrackcount.'</span></span>';
							$dj_tracks_output.= '</div>';
							$dj_tracks_output.= '<div class="tracklist-album tracklist-col">';
							$dj_tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
							$dj_tracks_output.= '<strong>'.$dj_post_title.'</strong><p>'.$dj_audio_artist_name.'</p></div>';											
							$dj_tracks_output.= '</div>';//.tracklist-details
							$dj_tracks_output.= '</div>';//.tracklist-Row
							
							$dj_tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$dj_soundcloud_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a>
							<div>'.$dj_genre.' | '.$dj_post_date.'</div></div>'; 
						 }
						$i++;
						$error = false;
					}
				}
			}
			
			// External mp3 list
			if ( $dj_audio_posttype_option  == 'externalmp3' ) {
				if(  $dj_mp3_title!= ''){
					if ( preg_match("/$keyword/i", $dj_mp3_title) ) {
						
						$dj_mp3_unique_id = $djmix_start.'mp3'.rand( 200,300 );
						$mp3id 			= $post->ID;
						$trackcount 	= get_post_meta( $mp3id,'trackcount',true) ? get_post_meta( $mp3id,'trackcount',true ) : '0';		
					
						if ( $i >= $dj_min_pagenum && $i < $dj_max_pagenum ) {
							$dj_tracks_output.= '<div class="tracklist-Row">';
							$dj_tracks_output.= '<div class="tracklist-details">';
							$dj_tracks_output.= '<div class="tracklist-name tracklist-col"><a class="fap-single-track no-ajaxy"  data-id="'.$mp3id.'"   data-meta="#single-player-meta'.$dj_mp3_unique_id.'" href="'.esc_attr( $dj_mp3_url ).'"  title="'.$dj_mp3_title.'"  rel="'.$dj_music_image.'"><i class="ivaplay"></i></a>'.$dj_mp3_title.'<br>';
							$dj_tracks_output.= '<span class="mp3o iva_playCount"><i class="fa fa-play fa-fw"></i><span class="ivaplay-'.$mp3id.'">'.$trackcount.'</span></span>';
							$dj_tracks_output.= '</div>';
							$dj_tracks_output.= '<div class="tracklist-album tracklist-col">';
							$dj_tracks_output.= '<div class="tracklist-thumb"><figure>'. atp_resize( $post->ID ,'',80, 80,'','').'</figure></div>';
							$dj_tracks_output.= '<strong>'.$dj_post_title.'</strong></div>';
							$dj_tracks_output.= '</div>';//.tracklist-details
							$dj_tracks_output.= '</div>';//.tracklist-Row
							$dj_tracks_output.= '<div style="display:none;" class="track-playlist" id="single-player-meta'.$dj_mp3_unique_id.'"><a href="'.get_permalink( $post->ID ).'">'.get_the_title( $post->ID ).'</a>
							<div>'.$dj_genre.' | '.$dj_post_date.'</div></div>'; 
						}
						$i++;
						$error = false;
					}
				}
			}
		$djmix_start++;
		endwhile; 
		
		// Track Search Results Output
		if( $dj_tracks_output ){
			echo '<div class="tracks-search inner">';
			echo '<h2>'.__('Tracks','musicplay').'</h2>';
			echo '<div class="tracklist-Table">';
			echo '<div class="tracklist-Header">';
			echo '<div class="tracklist-Row">';
			echo '<div class="tracklist-name tracklist-col">'.__('Tracks','musicplay').'</div>';
			echo '<div class="tracklist-album tracklist-col">'.__('Album','musicplay').'</div>';
			echo '</div>';// .tracklist-Row
			echo '</div>';//.tracklist-Header
			echo '<div class="tracklist-Body">';
			
			echo $dj_tracks_output;
			
			// Tracks Pagination
			if( $search_pagination_option == 'on'){
				echo $dj_tracks_pagination;
			}
			
			echo '</div>';//.tracklist-Body
			echo '</div>';//.tracklist-Table
			echo '</div>';//.tracklist-search
		}
		wp_reset_query();
		else :
		endif;
		echo '<div class="clear"></div>';
	}else{
		//$error = true;
	}
			
	
	// Djmix pagination
	$paged5 = isset( $_GET['paged5'] ) ? (int) $_GET['paged5'] : 1;
	$paged6 = isset( $_GET['paged6'] ) ? (int) $_GET['paged6'] : 1;

	// Djmix title search query
	add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );
	function title_like_posts_where( $where, &$wp_query ) {
		global $wpdb;
		if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
			$search_orderby_s = esc_sql( $wpdb->esc_like( $post_title_like ) );
			$where .= "AND $wpdb->posts.post_title LIKE '%{$search_orderby_s}%'";
		}
		return $where;
	}
	
	$dj_posts_perpage  =  get_option('atp_search_djmix_limits') ? get_option('atp_search_djmix_limits') :'1';
	
	// Djmix Searchstarts here
	$djmix_args = array(
		'post_type' 		=> 'djmix',
		'post_title_like' 	=> $keyword,
		'posts_per_page'	=> $dj_posts_perpage,
		'paged' 			=> $paged5,
		'orderby'		 	=>	'date',
		'order'			 	=>	'ASC'
	);
	$iva_djmix_query = new WP_Query( $djmix_args );

	$dj_column_index = 0; $dj_columns = 6;

	if( $dj_columns == '6' ) { $dj_class = 'col_sixth'; }
	if( $dj_columns == '5' ) { $dj_class = 'col_fifth'; }
	if( $dj_columns == '4' ) { $dj_class = 'col_fourth'; }
	if( $dj_columns == '3' ) { $dj_class = 'col_third'; }

	//Full Width Artist Image Sizes
	if( $dj_columns == '6' ) { $dj_width='470'; $dj_height = '470' ; }
	if( $dj_columns == '5' ) { $dj_width='470'; $dj_height = '470' ; }
	if( $dj_columns == '4' ) { $dj_width='470'; $dj_height = '470' ; }
	if( $dj_columns == '3' ) { $dj_width='470'; $dj_height = '470' ; }

	if ( $iva_djmix_query->have_posts() ) : 

		$djmix_title = get_option('atp_djmix_slug') ? get_option('atp_djmix_slug'):__('Djimix','musicplay');

		echo '<div class="artists-search inner">';
		echo '<h2>'.$djmix_title.'</h2>'; 
		
		while ( $iva_djmix_query->have_posts() ) : $iva_djmix_query->the_post();
		
			// $djmix_categories = get_post_meta( $post->ID, 'artist_genres', true );
			$dj_img_alt_title = get_the_title();
			
			$dj_column_index++;
			$dj_last = ( $dj_column_index == $dj_columns && $dj_columns != 1 ) ? 'end ' : '';

		echo '<div class="artist-list  '.$dj_class. ' '.$dj_last.'">';
		echo '<div class="custompost_entry">';
		
		if( has_post_thumbnail() ){
			echo '<div class="custompost_thumb port_img">';
			echo '<figure>'. atp_resize( $post->ID, '', $dj_width, $dj_height, '', $dj_img_alt_title ) .'</figure>'; 
			echo '<div class="hover_type">';
			echo '<a class="hoverartist"  href="' .get_permalink(). '" title="' . get_the_title() . '"></a>';
			echo '</div>';
			echo '</div>';
		} 

		echo '<div class="artist-desc">';
		echo '<h2 class="entry-title"><a href="'.get_the_permalink().'" rel="bookmark" title="'.esc_attr( get_the_title() ).'">'.get_the_title().'</a></h2>';
		echo '<span>'.strip_tags( get_the_term_list( $post->ID, 'djmix_cat', '', ', ', '' )).'</span>';
		echo '</div>';

		echo '</div>';// .custompost_entry
		echo '</div>';// .artist-post
		
		
	if( $dj_column_index == $dj_columns ){
		$dj_column_index = 0;
		echo '<div class="clear"></div>';
	}
	$error = false;
	endwhile; 

	echo '<div class="clear"></div>';

	// Djmix pagination
	$dj_pagenum_link = html_entity_decode( get_pagenum_link() );
	$dj_format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $dj_pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$dj_format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'paged5/%#%', 'paged5' ) : '?paged3=%#%';
	
	$dj_page_args = array(
		'base' 		=> add_query_arg( 'paged5', '%#%' ),
		'format'  	=> $dj_format,
		'current' 	=> $paged5,
		'prev_next' => false,
		'total'   	=> $iva_djmix_query->max_num_pages,
		'add_args'  => array( 'paged6' => $paged6 )
	);
	if( $search_pagination_option == 'on'){
		echo '<div class="pagination">'.paginate_links( $dj_page_args ).'</div>';
	}
	echo '</div>';// .djmix-search
	$error = false;
	// wp_reset_query();
	else :
	endif;
	// Djmix results ends here
?>