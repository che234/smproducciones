<?php
	/***
	 * Image Post Format
	 */
	global $post;

	if( has_post_thumbnail()){
		
		if( atp_generator( 'sidebaroption', get_the_id() ) != "fullwidth" ) {
			$width = '790';
		} else {
			$width = '1140';
		}
	?>
	<div class="postimg">
		<figure>
		<?php echo atp_resize($post->ID,'',$width,'350','','');?>
		</figure>
	</div>
<?php } ?>