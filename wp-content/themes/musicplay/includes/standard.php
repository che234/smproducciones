<?php 

	if( ( function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) {
		
	global $post_id;

	$img_alt_title = get_the_title();

	if( atp_generator( 'sidebaroption', get_the_id() ) != "fullwidth" ) {
		$width = '790';
	} else {
		$width = '1140';
	}

?>
	<div class="postimg">
        <figure>
		<a title="<?php printf(__('Permanent Link to %s', 'musicplay'), get_the_title()); ?>" href="<?php the_permalink(); ?>">
			<?php echo atp_resize($post_id,'',$width,'350','imgborder', $img_alt_title );?>
		</a>
		</figure>
	</div>
<?php } 
