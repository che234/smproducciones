<?php 
// Theme Class
if ( ! class_exists('ATP_theme_musicband') ) {
	class ATP_theme_musicband extends ATP_Theme {
	
		function _construct() {
			add_action( 'after_setup_theme', array($this,'atp_theme_setup' ));
		}
		
		function atp_variable($type) {
		
			$iva_options = parent::atp_variable($type);
			
			switch($type) {
			
				case 'artists': // Get Artists Name/Slug
								$args = array(
									'posts_per_page'   => -1,
									'offset'           => 0,
									'category'         => '',
									'orderby'          => 'post_date',
									'order'            => 'DESC',
									'include'          => '',
									'exclude'          => '',
									'meta_key'         => '',
									'meta_value'       => '',
									'post_type'        => 'artists',
									'post_mime_type'   => '',
									'post_parent'      => '',
									'post_status'      => 'publish',
									'suppress_filters' => true 
								); 
							
								$atp_entries = get_posts($args);
								foreach ($atp_entries as $key => $entry) {
									$iva_options[$entry->ID] = $entry->post_title;
								}
								break;
					case 'album': // Get album Slug and Name
								$atp_entries = get_terms( 'genres', 'orderby=name&hide_empty=0' );
								foreach ( $atp_entries as $atpalbum ) {
									$iva_options[$atpalbum->slug] = $atpalbum->name;
									$album_ids[] = $atpalbum->slug;
								}
								break;
					case 'album_label': // Get album Slug and Name
								$atp_entries = get_terms( 'label', 'orderby=name&hide_empty=0' );
								foreach ( $atp_entries as $atpalbum ) {
									$iva_options[$atpalbum->slug] = $atpalbum->name;
									$album_ids[] = $atpalbum->slug;
								}
								break;
					case 'album_genres': // Get album Slug and Name
								$atp_entries = get_terms( 'genres', 'orderby=name&hide_empty=0' );
								foreach ( $atp_entries as $atpalbum ) {
									$iva_options[$atpalbum->slug] = $atpalbum->name;
									$album_ids[] = $atpalbum->slug;
								}
								break;	
					case 'gallery': // Get Gallery Slug and Name
							$atp_entries = get_terms( 'gallery_type', 'orderby=name&hide_empty=0' );
							foreach ( $atp_entries as $atpalbum ) {
								$iva_options[$atpalbum->slug] = $atpalbum->name;
								$album_ids[] = $atpalbum->slug;
							}
							break;
					case 'video': // Get Gallery Slug and Name
								$atp_entries = get_terms( 'video_type', 'orderby=name&hide_empty=0' );
								foreach ( $atp_entries as $atpvideo ) {
									$iva_options[$atpvideo->slug] = $atpvideo->name;
									$video_ids[] = $atpvideo->slug;
								}
								break;
					case 'artist_cat': // Get Events Slug and Name
								$atp_entries = get_terms( 'artist_cat','orderby=name&hide_empty=0' );
								if(is_array($atp_entries)){
									foreach ( $atp_entries as $atpArtist ) {
										$iva_options[$atpArtist->slug] = $atpArtist->name;
										$artist[] = $atpArtist->slug;
									}
							}
							break;
					case 'djmix': // Get Slider Slug and Name
								$atp_entries = get_terms( 'djmix_cat', 'orderby=name&hide_empty=0' );
								foreach ( $atp_entries as $atpDjmix ) {
									$iva_options[$atpDjmix->slug] = $atpDjmix->name;
									$djmix_ids[] = $atpDjmix->slug;
								}
								break;
				}
				return $iva_options;
			}
			
			function atp_post_types() {
					parent::atp_post_types();
					$this->child_require_once( MUSIC_DIR . 'events/events.php');
					$this->child_require_once( MUSIC_DIR . 'artists/artist.php');
					$this->child_require_once( MUSIC_DIR . 'albums/album.php');
					$this->child_require_once( MUSIC_DIR . 'video/video.php');
					$this->child_require_once( MUSIC_DIR . 'gallery/gallery.php');
					$this->child_require_once( MUSIC_DIR . 'djmix/djmix.php');
			}
			
			function atp_custom_meta() {
			
					parent::atp_custom_meta();
					
					$this->child_require_once( MUSIC_DIR . 'events/events-meta.php');
					$this->child_require_once( MUSIC_DIR . 'artists/artist-meta.php');
					$this->child_require_once( MUSIC_DIR . 'albums/allbum-meta.php');
					$this->child_require_once( MUSIC_DIR . 'video/video-meta.php');
					$this->child_require_once( MUSIC_DIR . 'gallery/gallery-meta.php');
					$this->child_require_once( MUSIC_DIR . 'djmix/djmix-meta.php');
			
			}
			function child_require_once($file){
				global $atp_shortcodes;
				$child_file = str_replace(get_template_directory(),get_stylesheet_directory(),$file);
				if(file_exists($child_file)){
					require_once($child_file);
				}else{
					require_once($file);
				}
			}
			
	}
	$atp_theme = new ATP_theme_musicband();
}	
$url =  FRAMEWORK_URI . 'admin/images/';


global $iva_options, $atp_options, $url, $shortname,$atp_theme ;

//POST TYPE LANG OPTIONS
//---------------------------------------------------------------------------
require_once( child_require_file(MUSIC_DIR . 'posttype_lang_options.php') );

//Localization Text
//---------------------------------------------------------------------------
if( !function_exists('atp_localize')){
	function atp_localize($text="",$before="",$after=""){
		$output = $before.$text.$after;
		return $output;
	}
}

//S I N G L E  P O S T T Y P E S
//---------------------------------------------------------------------------
if( !function_exists('atp_single_posttypes')){
	function atp_single_posttypes() {
		global $wp_query, $post;  
		$customtype = $post->post_type;
		if(file_exists( child_require_file(MUSIC_DIR .$customtype.'/'. 'single-'.$customtype.'.php'))){
			return child_require_file(MUSIC_DIR . $customtype.'/'. 'single-'.$customtype.'.php');
		}
		elseif(file_exists(child_require_file( THEME_DIR . '/single-'.$customtype.'.php'))){
			return child_require_file(THEME_DIR . '/single-'.$customtype.'.php');
		}else{
			return child_require_file(THEME_DIR .'/single.php');
		}
	}
	add_filter('single_template', 'atp_single_posttypes');
}
//Retrieve path of taxonomy template in current or parent template. 
if( !function_exists('atp_taxonomy_posttypes')){
	function atp_taxonomy_posttypes() 
	{
		global $wp_query, $post;  
		$customtype = $post->post_type;
		$name = get_queried_object()->taxonomy;
		if(file_exists( child_require_file(MUSIC_DIR .$customtype.'/'. 'taxonomy-'.$name.'.php'))){
			return child_require_file(MUSIC_DIR . $customtype.'/'. 'taxonomy-'.$name.'.php');
		}
		elseif(file_exists(child_require_file( THEME_DIR . '/taxonomy-'.$name.'.php'))){
			return child_require_file(THEME_DIR . '/taxonomy-'.$name.'.php');
		}else{
			return child_require_file(THEME_DIR .'/archive.php');
		} 
	}
	add_filter('taxonomy_template', 'atp_taxonomy_posttypes');
}


// A D M I N  E N Q U E U E   S T Y L E S  A N D  S C R I P T S
//---------------------------------------------------------------------------

//define( 'MUSIC_URI',child_scripts(get_template_directory_uri() . '/musicband/'));
define( 'MUSIC_URI',get_template_directory_uri() . '/musicband/');
define( 'CH_MUSIC',get_template_directory() . '/musicband/');

if( !function_exists('admin_enqueue_custompostscripts')){
	function admin_enqueue_custompostscripts(){
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-slider');
		wp_enqueue_script('jquery-ui-draggable');
		wp_enqueue_script('jquery-ui-droppable');
		wp_enqueue_script('atp-button', child_enqueue_scripts(CH_MUSIC . 'js/buttons.js',MUSIC_URI . 'js/buttons.js'), false,false,'all' );
		wp_enqueue_script('admin-timepickeraddon', child_enqueue_scripts(CH_MUSIC . 'events/js/jquery-ui-timepicker-addon.js',MUSIC_URI . 'events/js/jquery-ui-timepicker-addon.js'),false,false,'all');
		wp_enqueue_script('atp-custompost', child_enqueue_scripts(CH_MUSIC . 'js/custompost.js',MUSIC_URI . 'js/custompost.js'), false,false,'all' ); 
		wp_enqueue_style('atp-jquery-timepicker-addon', child_enqueue_scripts(CH_MUSIC . 'events/css/jquery-ui-timepicker-addon.css',MUSIC_URI . 'events/css/jquery-ui-timepicker-addon.css'), false,false,'all' ); 
	}
	add_action( 'admin_enqueue_scripts', 'admin_enqueue_custompostscripts' );	
}

// Custom Musicplay Options
require_once( child_require_file(MUSIC_DIR . 'musicplay-options.php') );

//Custom Post types
//-------------------------------------------------

//Post Type Label Options
require_once( child_require_file(MUSIC_DIR . 'posttype_label_options.php') );

//Postype Meta Cases
require_once( child_require_file(MUSIC_DIR . 'meta_cases.php') );


/**
 * Follow Social Network for Artists
 * @since - 2.7.1
 */
function follow_social_networks( $sociables ){
	$sociable_icon = array(
		'fa fa-facebook fa-fw fa-lg'	=> 'facebook',
		'fa fa-twitter fa-fw fa-lg'		=> 'twitter' ,
		'fa fa-google-plus fa-fw fa-lg'	=> 'plus.google',
		'fa fa-pinterest fa-fw fa-lg'	=> 'pinterest' ,
		'fa fa-youtube fa-fw fa-lg'		=> 'youtube' ,
		'fa fa-vimeo fa-fw fa-lg'		=> 'vimeo',
		'fa fa-flickr fa-fw fa-lg'		=> 'flickr' ,
		'fa fa-instagram fa-fw fa-lg'	=> 'instagram',
		'fa fa-soundcloud fa-fw fa-lg'	=> 'soundcloud',
		'fa fa-foursquare fa-fw fa-lg'	=> 'foursquare'
	);
	$output ='';
	$sociable_path = explode("\n",$sociables);
	$output .= '<div class="iva_follow">';
	$output .= '<ul class="follow">';

	foreach ( $sociable_path as $sociable_url ){
		$sociableurl 	= preg_replace("/http:\/\/www.||https:\/\/www.||https:\/\/||http:\/\/||www./", "" ,$sociable_url);
		$sociable_host 	= preg_split("/[\s.]+/",$sociableurl);
		$sociable_host_names =  $sociable_host[0];
		
		foreach ($sociable_icon as $key => $sociableicon){
			if( trim($sociableicon) === trim(strtolower($sociable_host_names))) {
				$sociable_url = trim($sociableurl);
				$output .= '<li class="'.$sociableicon.'"><a href="'.esc_url($sociable_url).'" target="_blank"><i class="'.$key.'"></i></a></li>';
			}
		}
	}
	$output .= '</ul>';
	$output .= '</div>';

	return $output;
}

function iva_get_download($file) {
	$code = new Encryption;
	$ecode = $code->encode($file);	
	return $ecode;
}

//Encrypt/Decrypt url data
class Encryption {
    var $skey     = "iva_encrypt_2015"; // you can change it
    
    public  function safe_b64encode($string) {
    
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
 
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
    
    public  function encode($value){
        
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }
    
    public function decode($value){
        
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
}

