<?php
	// REGISTER AND ENQUEUE SCRIPTS
	//--------------------------------------------------------
	if(!function_exists('enqueue_scripts')){
		function enqueue_scripts() {
			wp_register_script('atp-sf-menu', child_enqueue_scripts(CH_THEME_JS.'/superfish.js',THEME_JS .'/superfish.js'),array( 'jquery' ),'','in_footer');
			wp_register_script('atp-flexslider', child_enqueue_scripts(CH_THEME_JS .'/jquery.flexslider.js',THEME_JS .'/jquery.flexslider.js'), array( 'jquery' ),'','in_footer');	
			wp_register_script('atp-progresscircle', child_enqueue_scripts(CH_THEME_JS .'/jquery.easy-pie-chart.js',THEME_JS .'/jquery.easy-pie-chart.js'), array( 'jquery' ),'','in_footer');
			wp_register_script('atp-excanvas', child_enqueue_scripts(CH_THEME_JS .'/excanvas.js',THEME_JS .'/excanvas.js'), array( 'jquery' ),'','in_footer');
			wp_register_script('atp-custom', child_enqueue_scripts(CH_THEME_JS . '/sys_custom.js',THEME_JS . '/sys_custom.js'), array( 'jquery' ),'1.0','in_footer');
			wp_register_script('atp-waypoints', child_enqueue_scripts(CH_THEME_JS .'/waypoints.js',THEME_JS .'/waypoints.js'), array( 'jquery' ),'','in_footer');
			wp_register_script('atp-countdown', child_enqueue_scripts(CH_THEME_JS . '/countdown.js',THEME_JS . '/countdown.js'), array( 'jquery' ),'1.0','in_footer');	
	
			// Enqueue Scripts
			//--------------------------------------------------------
	
			wp_enqueue_script('jquery');
			wp_enqueue_script('atp-easing', THEME_JS .'/jquery.easing.1.3.js',array( 'jquery' ),'','in_footer');
			wp_enqueue_script('atp-countdown');
			wp_enqueue_script('atp-sf-hover');
			wp_enqueue_script('atp-sf-menu');
			wp_enqueue_script('atp-preloader', THEME_JS .'/jquery.preloadify.min.js',array( 'jquery' ),'','in_footer');
			wp_enqueue_script('atp-prettyPhoto', THEME_JS .'/jquery.prettyPhoto.js',array( 'jquery' ),'','in_footer');
			wp_enqueue_script('atp-fitvids', THEME_JS .'/jquery.fitvids.js',array( 'jquery' ),'','in_footer');
			wp_enqueue_script('atp-isotope', THEME_JS .'/isotope.js',array( 'jquery' ),'','in_footer');
			wp_enqueue_script('atp-custom');
			wp_enqueue_script('atp-flexslider');
			wp_enqueue_script('atp-waypoints');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-widget');
			wp_enqueue_script('jquery-ui-mouse');
			wp_enqueue_script('jquery-ui-draggable');
			wp_enqueue_script('jquery-ui-sortable');
			wp_enqueue_script('jplayer.min', THEME_JS.'/jquery.jplayer.min.js',false,false,'all');
			if(get_option('atp_ajaxify') =='enable'){
				wp_enqueue_script('atp-history', child_enqueue_scripts(CH_THEME_JS.'/history.js',THEME_JS.'/history.js'),array( 'jquery' ),'','in_footer');
				wp_enqueue_script('atp-ajaxify', child_enqueue_scripts(CH_THEME_JS.'/ajaxify.js',THEME_JS.'/ajaxify.js'),array( 'jquery' ),'','in_footer');
						
				$aws_data = array(
						'rootUrl' 		=> site_url() . '/',
						'ThemeDir'     => get_template_directory_uri() . '/',
						'choose_player'     => get_option('atp_audio_player'),
						'preloader_image'	=>  get_option ('atp_preloader_image')

					);
					
				wp_localize_script('atp-ajaxify', 'aws_data', $aws_data);
			}
	
		//AJAX URL
		$data["ajaxurl"] = admin_url("admin-ajax.php");	
	
		//HOME URL
		$data["home_url"] = get_home_url();
		
		//pass data to javascript
		$params = array(
			'l10n_print_after' => 'iva_panel = ' . json_encode($data) . ';'
		);
	
		wp_localize_script('jquery', 'iva_panel', $params);	
	
			if(is_singular( 'albums' ))
			{
				wp_enqueue_script('atp-jqurydownload', child_enqueue_scripts(CH_THEME_JS.'/jquerydownload.js',THEME_JS.'/jquerydownload.js'),array( 'jquery' ),'','in_footer');
				
			}
			wp_enqueue_script('jplayer.playlist', child_enqueue_scripts(CH_THEME_JS.'/jplayer.playlist.min.js',THEME_JS.'/jplayer.playlist.min.js'),false,false,'all');
			if ( get_option( 'atp_audio_enable' ) == 'on' ) {
				wp_enqueue_script('atp-soundmanager2', child_enqueue_scripts(CH_THEME_JS.'/soundmanager2-nodebug-jsmin.js',THEME_JS.'/soundmanager2-nodebug-jsmin.js'),array( 'jquery' ),'','in_footer');
				wp_enqueue_script('atp-soundcloud', 'https://connect.soundcloud.com/sdk.js','jquery','','in_footer');
				wp_enqueue_script('atp-amplify', child_enqueue_scripts(CH_THEME_JS .'/amplify.min.js',THEME_JS .'/amplify.min.js'),'jquery','','in_footer');
				wp_enqueue_script('atp-fullwidthAudioPlayer', child_enqueue_scripts(CH_THEME_JS .'/jquery.fullwidthAudioPlayer.min.js',THEME_JS .'/jquery.fullwidthAudioPlayer.min.js'),array( 'jquery' ),'','in_footer');
				wp_enqueue_style('fullwidthAudioPlayer', child_enqueue_scripts(CH_THEME_CSS.'/jquery.fullwidthAudioPlayer.css',THEME_CSS.'/jquery.fullwidthAudioPlayer.css'), false, false, 'all');

			}
			wp_localize_script( 'jquery', 'atp_panel', array(
				'SiteUrl' =>get_template_directory_uri()
			));
			
			wp_register_style('iva-responsive',  child_enqueue_scripts(get_template_directory().'/css/responsive.css',get_template_directory_uri().'/css/responsive.css'), array(), '1', 'all' ); 

			// E N Q U E U E   S T Y L E S 
			//--------------------------------------------------------
			wp_enqueue_style( 'iva-shortcodes', child_enqueue_scripts(CH_THEME_CSS . '/shortcodes.css',THEME_CSS . '/shortcodes.css'), array(), '1', 'all' ); 
			wp_enqueue_style( 'iva-fontawesome', child_enqueue_scripts(CH_THEME_CSS . '/fontawesome/css/font-awesome.css',THEME_CSS . '/fontawesome/css/font-awesome.css'), array(), '1', 'all' ); 
			wp_enqueue_style( 'musicplay-style', get_stylesheet_uri() );
			wp_enqueue_style( 'iva-animate', child_enqueue_scripts(CH_THEME_CSS.'/animate.css',THEME_CSS.'/animate.css'), false, false, 'all');
			wp_enqueue_style( 'iva-prettyphoto', child_enqueue_scripts(CH_THEME_CSS.'/prettyPhoto.css',THEME_CSS.'/prettyPhoto.css'), false, false, 'all');
	
			wp_enqueue_style('iva-jplayer', child_enqueue_scripts(CH_THEME_CSS.'/jplayer/premium-pixels.css',THEME_CSS.'/jplayer/premium-pixels.css'),false,false,'all');
		
			if( get_option('atp_responsive') != 'on') {
				wp_enqueue_style( 'iva-responsive' );
			}
			wp_enqueue_style( 'iva-datepicker', child_enqueue_scripts(CH_FRAMEWORK.'admin/css/datepicker.css',FRAMEWORK_URI.'admin/css/datepicker.css'), false, false, 'all');
			wp_enqueue_style( 'iva-flexslider', child_enqueue_scripts(CH_THEME_CSS . '/flexslider.css',THEME_CSS . '/flexslider.css'), array(), '1', 'all' ); 
			
			if ( is_singular() ){
				wp_enqueue_script( 'comment-reply' );
			}
			if(get_option('atp_style') != '0'){
				$atp_style=get_option('atp_style');
				wp_enqueue_style('atp-style', child_enqueue_scripts(CH_THEME.'/colors/'.$atp_style,THEME_URI.'/colors/'.$atp_style), false, false, 'all');
			}
		}  
		add_action( 'wp_enqueue_scripts','enqueue_scripts');
	}
	// E N Q U E U E   S C R I P T S   I N   A D M I N 
	//--------------------------------------------------------
	if(!function_exists('admin_enqueue_scripts')){
		function admin_enqueue_scripts(){
			wp_enqueue_script('theme-script',child_enqueue_scripts(CH_FRAMEWORK . 'admin/js/script.js',FRAMEWORK_URI . 'admin/js/script.js'));
			wp_enqueue_script( 'wp-color-picker');
			wp_enqueue_script('theme-shortcode',child_enqueue_scripts(CH_FRAMEWORK.'admin/js/shortcode.js',FRAMEWORK_URI . 'admin/js/shortcode.js'));
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-datepicker');
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
	
			wp_localize_script( 'theme-script', 'atp_panel', array(
				'SiteUrl' =>get_template_directory_uri()
			));
			wp_enqueue_style('thickbox');
			wp_enqueue_style( 'wp-color-picker');
			wp_enqueue_style('atpadmin', child_enqueue_scripts(CH_FRAMEWORK . 'admin/css/atpadminnew.css',FRAMEWORK_URI . 'admin/css/atpadminnew.css'));
			wp_enqueue_style('appointment-style', child_enqueue_scripts(CH_FRAMEWORK.'admin/css/datepicker.css',FRAMEWORK_URI.'admin/css/datepicker.css'), false, false, 'all');
			wp_enqueue_style( 'atp-chosen', child_enqueue_scripts(CH_FRAMEWORK . 'admin/css/chosen.css',FRAMEWORK_URI . 'admin/css/chosen.css'), false, false, 'all' ); 
		}
		add_action( 'admin_enqueue_scripts', 'admin_enqueue_scripts' );
	}

	// Owl Carousel script and style
	//--------------------------------------------------------
	if(!function_exists('owl_carousel_scripts')){
		function owl_carousel_scripts() {
			wp_enqueue_style( 'atp-owl', child_enqueue_scripts(CH_THEME_CSS.'/owl.carousel.css',THEME_CSS.'/owl.carousel.css'), false, '1.24', 'all');
			wp_enqueue_script( 'atp-owl', child_enqueue_scripts(CH_THEME_JS .'/owl.carousel.min.js',THEME_JS . '/owl.carousel.min.js'), array(), '1.0.0', true );
		}
	}
?>