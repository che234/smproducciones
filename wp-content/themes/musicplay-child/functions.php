<?php
/**
 * Require the files in child theme
 */
function child_require_file($file){
	global $atp_shortcodes;
	$child_file = str_replace(get_template_directory(),get_stylesheet_directory(),$file);
	if(file_exists($child_file)){
		return($child_file);
	}else{
		return($file);
	}
}
/**
 * Enqueue the css and js files in child theme
 *
 */

function child_enqueue_scripts( $file_dir,$file_url ){
	$child_style = str_replace( get_template_directory(),get_stylesheet_directory(),$file_dir);
	if(file_exists($child_style)){
		return str_replace( get_template_directory_uri(),get_stylesheet_directory_uri(),$file_url);
	}else{
		return $file_url;
	}
}
 
/**
 * ATP_Theme class.
 */
if (!class_exists('ATP_Theme')) {
    
    class ATP_Theme
    {
        public $theme_name;
   		public $meta_box;
		
        public function __construct()
        {
            $this->atp_constant();
            $this->atp_themesupport();
            $this->atp_head();
            $this->atp_themepanel();
            $this->atp_widgets();
            $this->atp_post_types();
            $this->atp_custom_meta();
			$this->atp_shortcodes();
			$this->atp_meta_generators();
            $this->atp_common();
        }
        
        function atp_constant(){
			/**
			 * framework general variables and directory paths.
			 */
         	$theme_data   = wp_get_theme();
			$themeversion = $theme_data->Version;
			$theme_name   = $theme_data->Name;
            
            /**
             * Set the file path based on whether the Options 
             * Framework is in a parent theme or child theme
             * Directory Structure
             */
			/**
			 * theme framework.
			 */
            define('FRAMEWORK', '2.0'); 
            define('THEMENAME', $theme_name);
            define('THEMEVERSION', $themeversion);
            
            define('THEME_URI', get_template_directory_uri());
            define('THEME_DIR', get_template_directory());
            define('THEME_JS', THEME_URI . '/js');
            define('THEME_CSS', THEME_URI . '/css');
            define('FRAMEWORK_DIR', THEME_DIR . '/framework/');
            define('FRAMEWORK_URI', THEME_URI . '/framework/');
			define('ADMIN_URI', FRAMEWORK_URI . '/admin/');
			define('ADMIN_DIR', FRAMEWORK_DIR . '/admin/');
            
            define('CUSTOM_META', FRAMEWORK_DIR . '/custom-meta/');
            define('CUSTOM_PLUGINS', FRAMEWORK_DIR . '/custom-plugins/');
            define('CUSTOM_POST', FRAMEWORK_DIR . '/custom-post/');
			define('THEME_SHORTCODES', FRAMEWORK_DIR . 'shortcode/' );
            define('THEME_WIDGETS', FRAMEWORK_DIR . 'widgets/');
            define('THEME_PLUGINS', FRAMEWORK_DIR . 'plugins/');
            define('THEME_POSTTYPE', FRAMEWORK_DIR . 'custom-post/');
            define('THEME_CUSTOMMETA', FRAMEWORK_DIR . 'custom-meta/');
            
            define('THEME_PATTDIR', THEME_URI . '/images/patterns/');

			define('CH_THEME', get_template_directory());
			define('CH_THEME_JS', CH_THEME. '/js');
            define('CH_THEME_CSS', CH_THEME. '/css');
			define('CH_FRAMEWORK', CH_THEME . '/framework/');
        }
        
        /** 
         * allows a theme to register its support of a certain features
         */
		function atp_themesupport() {
			add_theme_support( 'post-formats', array( 'aside', 'audio', 'link', 'image', 'gallery', 'quote', 'status', 'video' ) );
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'editor-style' );
			add_theme_support( 'menus' );
			add_theme_support( 'title-tag' );
			
			/* Register Menu */
			register_nav_menus( array(
				'primary-menu' => __( 'Primary Menu','ATP_ADMIN_SITE' )
			));

			/* Define Content Width */
			if ( ! isset( $content_width ) ) $content_width = 900;
		}
        
       
		/** 
		 * scripts and styles enqueue .
		 */
        function atp_head(){
             $this->child_require_once(FRAMEWORK_DIR . 'common/head.php');
        }
        
		
		
		/** 
		 * admin interface .
		 */
        function atp_themepanel(){
            $this->child_require_once(FRAMEWORK_DIR . 'common/atp_googlefont.php');
            $this->child_require_once(FRAMEWORK_DIR . 'admin/admin-interface.php');
            $this->child_require_once(FRAMEWORK_DIR . 'admin/theme-options.php');
		}
      
		/** 
		 * widgets .
		 */
        function atp_widgets(){
			$atp_widgts = array(
					'register_widget', 'contactinfo',
					'flickr', 'twitter', 'sociable',
					'popularpost','recentpost', 
					'testimonial', 'testimonials_submit',
					'music_album','music_artist',
					'music_gallery_photos',
					'music_video','music_gallery_post','events'
				);	
			foreach( $atp_widgts as $widget ) {
				$this->child_require_once( THEME_WIDGETS .$widget.'.php' );
			}	
			
        }
        
        /**
         * load custom post types templates
         * @files slider, testimonials
         */
        function atp_post_types(){
             $this->child_require_once(THEME_POSTTYPE . '/slider.php');
             $this->child_require_once(THEME_POSTTYPE . '/testimonial.php');
		}
        
        /** load meta generator templates
         * @files slider, Menus, testimonial, page, posts, shortcodes generator
         */
        function atp_custom_meta(){
            $this->child_require_once(THEME_CUSTOMMETA . '/page-meta.php');
            $this->child_require_once(THEME_CUSTOMMETA . '/post-meta.php');
            $this->child_require_once(THEME_CUSTOMMETA . '/slider-meta.php');
            $this->child_require_once(THEME_CUSTOMMETA . '/testimonial-meta.php');
		}
		
		function atp_meta_generators(){
			$this->child_require_once( THEME_CUSTOMMETA . '/meta-generator.php' );
			$this->child_require_once( THEME_CUSTOMMETA . '/shortcode-meta.php' );
			$this->child_require_once( THEME_CUSTOMMETA . '/shortcode-generator.php' );
		}
		
		/* Shortcodes */
		function atp_shortcodes() {
			$atp_short = array(
					'accordion', 'boxes','buttons', 'contactinfo', 'flickr', 'general','image', 
					'layout', 'lightbox', 'messageboxes', 'flexslider', 'tabs_toggles', 
					'twitter', 'gmap', 'testimonial', 'testimonials','sociable', 'videos', 'staff', 
					'progressbar', 'services', 'carousel_blog', 'progresscircle','music_album','music_artist','music_gallery',
					'music_video','music_event','music_djmix','popularposts','recentposts','blog','iva_carousel','music_popular_songs'
			);
			
			foreach( $atp_short as $short ) {
				$this->child_require_once( THEME_SHORTCODES .$short.'.php' );
				
			}
		}
		
        
        /** 
         * theme functions
         * @uses skin generator
         * @uses twitter class
         * @uses pagination
         * @uses sociables
         * @uses Aqua imageresize // Credits : http://aquagraphite.com/
         * @uses plugin activation class
         */
        function atp_common(){
            $this->child_require_once(THEME_DIR . '/css/skin.php');
            $this->child_require_once(FRAMEWORK_DIR . 'common/class_twitter.php');
            $this->child_require_once(FRAMEWORK_DIR . 'common/atp_generator.php');
			$this->child_require_once(FRAMEWORK_DIR . 'common/pagination.php' );
            $this->child_require_once(FRAMEWORK_DIR . 'common/sociables.php');
            $this->child_require_once(FRAMEWORK_DIR . 'common/postlike.php');
            $this->child_require_once(FRAMEWORK_DIR . 'includes/image_resize.php');
            $this->child_require_once(FRAMEWORK_DIR . 'includes/class-activation.php');
        }
        
		function child_require_once($file){
			global $atp_shortcodes;
			$child_file = str_replace(get_template_directory(),get_stylesheet_directory(),$file);
			if(file_exists($child_file)){
				require_once($child_file);
			}else{
				require_once($file);
			}
		}
		
        /** 
         * custom switch case for fetching
         * posts, post-types, custom-taxonomies, tags
         */
        
		function atp_variable( $type ) {

			$iva_terms = array();

			switch( $type ){
				case 'pages': // Get Page Titles
							$atp_entries = get_pages( 'sort_column=post_parent,menu_order' );
							foreach ( $atp_entries as $atpPage ) {
								$iva_terms[$atpPage->ID] = $atpPage->post_title;
							}
							break;
				case 'slider': // Get Slider Slug and Name
							$atp_entries = get_terms( 'slider_cat', 'orderby=name&hide_empty=0' );
							foreach ( $atp_entries as $atpSlider ) {
								$iva_terms[$atpSlider->slug] = $atpSlider->name;
								$slider_ids[] = $atpSlider->slug;
							}
							break;
				case 'posts': // Get Posts Slug and Name
							$atp_entries = get_categories( 'hide_empty=0' );
							foreach ( $atp_entries as $atpPosts ) {
								$iva_terms[$atpPosts->slug] = $atpPosts->name;
								$atp_posts_ids[] = $atpPosts->slug;
							}
							break;
				case 'categories':
							$atp_entries = get_categories('hide_empty=true');
							foreach ($atp_entries as $atp_posts) {
								$iva_terms[$atp_posts->term_id] = $atp_posts->name;
								$atp_posts_ids[] = $atp_posts->term_id;
							}
							break;
				case 'postlink': // Get Posts Slug and Name
							$atp_entries = get_posts( 'hide_empty=0');
							foreach ( $atp_entries as $atpPosts ) {
								$iva_terms[$atpPosts->ID] = $atpPosts->post_title;
								$atp_posts_ids[] = $atpPosts->slug;
							}
							break;
				case 'events': // Get Events Slug and Name
							$atp_entries = get_terms( 'events_cat','orderby=name&hide_empty=1' );
							if(is_array($atp_entries)){
								foreach ( $atp_entries as $atpEvents ) {
									$iva_terms[$atpEvents->slug] = $atpEvents->name;
									$eventsvalue_id[] = $atpEvents->slug;
								}
							}
							break;
				case 'testimonial': // Get Testimonial Slug and Name
							$atp_entries = get_terms( 'testimonial_cat', 'orderby=name&hide_empty=0' );
							foreach ( $atp_entries as $atpTestimonial ) {
								$iva_terms[$atpTestimonial->slug] = $atpTestimonial->name;
								$testimonialvalue_id[] = $atpTestimonial->slug;
							}
							break;
				case 'tags': // Get Taxonomy Tags
							$atp_entries = get_tags( array( 'taxonomy' => 'post_tag' ) );
							foreach ( $atp_entries as $atpTags ) {
								$iva_terms[$atpTags->slug] = $atpTags->name;
							}
							break;
				case 'slider_type': // Slider Arrays for Theme Options
							$iva_terms = array(
								''				=> 'Select Slider',
								'flexslider'	=> 'Flex Slider',
								'videoslider'	=> 'Single Video',
								'static_image'	=> 'Static Image',
								'customslider'	=> 'Custom Slider'
							);
							break;
			}
			
			return $iva_terms;
		}
    }
}


/** section to decide whther use child theme or parent theme **/


if ( !defined('MUS_DIR') ){
	define( 'MUS_DIR', child_require_file(get_template_directory() . '/musicband/'));
}
/*** Remove Query String from Static Resources ***/
if(!function_exists('iva_remove_cssjs_ver')){
	function iva_remove_cssjs_ver( $src ) {
		if( strpos( $src, '?ver=' ) )
		$src = remove_query_arg( 'ver', $src );
		return $src;
	}
	add_filter( 'style_loader_src', 'iva_remove_cssjs_ver', 10, 2 );
	add_filter( 'script_loader_src', 'iva_remove_cssjs_ver', 10, 2 );
}
if ( wp_is_mobile() ){
	add_filter( 'body_class', 'iva_mobile_class' );
}

if( !function_exists('iva_mobile_class') ){
	function iva_mobile_class( $classes ) {
		// add 'class-name' to the $classes array
		$classes[] = 'iva_mobile';
		// return the $classes array
		return $classes;
	}
}
 