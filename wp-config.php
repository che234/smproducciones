<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'bd_sm');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '-@>da]|:-UGV9F*(cyTdAlM6,R;>Fv}7MSP/W>?RIl[e*,Ek@`gJI :-)*<!g@lt');
define('SECURE_AUTH_KEY', '1iGW(^0+Hh@+T%1V+[18j0}?b$,/:D^QcdE#Z:Nr){<0rp<UOkf!m hD0]W%m/L]');
define('LOGGED_IN_KEY', '1ond -MW5zIF#>vLlkWq-NyG!E$vc*nJ=jNBAM6(yHPP+=jMXXf$c|DlzqS.=JJk');
define('NONCE_KEY', 'O+<Sn%pW3eZm|tdS7*(Kt<;P8-eX<@z-UI0} ^}W)uH<|^aN6VB+4^Bvhx^6Iz([');
define('AUTH_SALT', '^2ZAyimH}f_;0&S|#$/42i^]|fcQ!+hCYTaK k2ZP?73}(AX|&3#]-PfJPUxX{fg');
define('SECURE_AUTH_SALT', 'D;O/{~TWc5sk 0LCB5it{__;6RS:>RVc{p/3+M:#vmijWo_T7GkApP+~;3_9{zti');
define('LOGGED_IN_SALT', '?Ibtp|l1/zd(v}QtmDic[uz#*1:T?Peqa,K}y12K%y|QWv?Kh~.i#t9@Qy_c!>&=');
define('NONCE_SALT', '$2hj9E<!XW>wJ[lGtE?C_W:_NGM]G%vl#QegmSsu[KhG0V(-B/4oci&N^WTDJJ[i');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'smp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD','direct');